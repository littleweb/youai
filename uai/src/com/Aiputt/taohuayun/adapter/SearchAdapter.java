//package com.Aiputt.taohuayun.adapter;
//
//import java.util.List;
//
//import android.content.Context;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.Aiputt.taohuayun.R;
//import com.Aiputt.taohuayun.domain.User;
//import com.Aiputt.taohuayun.utils.IntentUtils;
//import com.nostra13.universalimageloader.core.ImageLoader;
//
///**
// * Created by zhonglq on 2015/7/21.
// */
//public class SearchAdapter extends SuperAdapter<User> {
//
//	public SearchAdapter(Context context) {
//		super(context);
//	}
//
//	public SearchAdapter(List<User> list) {
//		super(list);
//	}
//
//	public SearchAdapter(Context context, List<User> list) {
//		super(context, list);
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		final ViewHolder holder;
//		if (convertView == null) {
//			convertView = View
//					.inflate(mContext, R.layout.seach_list_item, null);
//			holder = new ViewHolder(convertView);
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolder) convertView.getTag();
//		}
//		final User user = mList.get(position);
//		// holder.headPhoto.setImageResource(R.drawable.round_image_head_girl);
//		ImageLoader.getInstance().displayImage(user.getUser_face(),
//				holder.headPhoto);
//		holder.nickName.setText(user.getUser_nick());
//		holder.personAge.setText(user.getUser_age() + "岁");
//		holder.personChracter.setText(user.getUser_feature());
//		holder.personHeight.setText(user.getUser_height() + "cm");
//		holder.personIncome.setText(user.getUser_income() + "元");
//		if (user.getIsSayHello() == 1) {
//			holder.sayHello
//					.setBackgroundResource(R.drawable.list_item_user_btn_bg_press);
//		} else {
//			holder.sayHello
//					.setBackgroundResource(R.drawable.list_item_user_btn_bg_default);
//		}
//		holder.sayHello.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (user.getIsSayHello() == 0) {
//					IntentUtils.enterHuanxin(mContext,
//							String.valueOf(user.getUser_no()),
//							user.getUser_nick());
//					// holder.sayHello
//					// .setBackgroundResource(R.drawable.list_item_user_btn_bg_press);
//				}
//			}
//		});
//		return convertView;
//	}
//
//	public class ViewHolder {
//		public final ImageView headPhoto;
//		public final TextView nickName;
//		public final LinearLayout llnearbypersonitemdis;
//		public final TextView personAge;
//		public final TextView personHeight;
//		public final TextView personIncome;
//		public final TextView personHobby;
//		public final TextView personChracter;
//		public final LinearLayout llnearbypersonitemcharacter;
//		public final ImageView sayHello;
//		public final View root;
//
//		public ViewHolder(View root) {
//			headPhoto = (ImageView) root.findViewById(R.id.headPhoto);
//			nickName = (TextView) root.findViewById(R.id.nickName);
//			llnearbypersonitemdis = (LinearLayout) root
//					.findViewById(R.id.ll_nearby_person_item_dis);
//			personAge = (TextView) root.findViewById(R.id.personAge);
//			personHeight = (TextView) root.findViewById(R.id.personHeight);
//			personIncome = (TextView) root.findViewById(R.id.personIncome);
//			personHobby = (TextView) root.findViewById(R.id.personHobby);
//			personChracter = (TextView) root.findViewById(R.id.personChracter);
//			llnearbypersonitemcharacter = (LinearLayout) root
//					.findViewById(R.id.ll_nearby_person_item_character);
//			sayHello = (ImageView) root.findViewById(R.id.sayHello);
//			this.root = root;
//		}
//	}
//}
