package com.Aiputt.taohuayun.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by zhonglq on 2015/7/21.
 */
public class PrivateMailAdapter extends SuperAdapter<User> {

	public PrivateMailAdapter(Context context) {
		super(context);
	}

	public PrivateMailAdapter(List<User> list) {
		super(list);
	}

	public PrivateMailAdapter(Context context, List<User> list) {
		super(context, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = View
					.inflate(mContext, R.layout.private_mail_list_item, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final User user = mList.get(position);
        if(!user.getUser_face().equals(holder.headPhoto.getTag())) {
            ImageLoader.getInstance().displayImage(user.getUser_face(),
                    holder.headPhoto, ImageLoadUtils.getNormalOptions());
        }
        holder.headPhoto.setTag(user.getUser_face());
		holder.nickName.setText(user.getUser_nick());
		holder.personAge.setText(user.getUser_age() + "岁");
		holder.personHeight.setText(user.getUser_height() + "cm");
		holder.personLocation.setText(user.getUser_location());
		if (user.getUnread_cnt()>0) {
			holder.sayHello.setVisibility(View.VISIBLE);
			holder.ivJiaobiao.setVisibility(View.VISIBLE);
		}else {
			holder.sayHello.setVisibility(View.INVISIBLE);
			holder.ivJiaobiao.setVisibility(View.INVISIBLE);
		}
		return convertView;
	}

	public class ViewHolder {
		public final RoundedImageView headPhoto;
		public final ImageView ivJiaobiao;
		public final TextView nickName;
		public final LinearLayout llnearbypersonitemdis;
		public final TextView personAge;
		public final TextView personHeight;
		public final TextView personLocation;
		public final TextView sayHello;
		
		public final LinearLayout llnearbypersonitemcharacter;
		public final View root;

		public ViewHolder(View root) {
			headPhoto = (RoundedImageView) root.findViewById(R.id.headPhoto);
			ivJiaobiao = (ImageView) root.findViewById(R.id.ivJiaobiao);
			nickName = (TextView) root.findViewById(R.id.nickName);
			llnearbypersonitemdis = (LinearLayout) root
					.findViewById(R.id.ll_nearby_person_item_dis);
			personAge = (TextView) root.findViewById(R.id.personAge);
			personHeight = (TextView) root.findViewById(R.id.personHeight);
			personLocation = (TextView) root.findViewById(R.id.personLocation);
			sayHello = (TextView) root.findViewById(R.id.sayHello);
			llnearbypersonitemcharacter = (LinearLayout) root
					.findViewById(R.id.ll_nearby_person_item_character);
			this.root = root;
		}
	}
}
