package com.Aiputt.taohuayun.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.AgeInfo;
import com.Aiputt.taohuayun.domain.NotifyMessage;
import com.Aiputt.taohuayun.domain.RengongMessage;
import com.nostra13.universalimageloader.core.ImageLoader;

public class RengongMessageAdapter extends SuperAdapter<RengongMessage> {



	public RengongMessageAdapter(Context context, List<RengongMessage> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}
	public RengongMessageAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = View
					.inflate(mContext, R.layout.item_rengong_message, null);
			holder = new ViewHolder();
			holder.tvLabel = (TextView) convertView.findViewById(R.id.label);
			holder.ivImge = (ImageView) convertView.findViewById(R.id.ivImge);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		RengongMessage disposition = mList.get(position);
		holder.tvLabel.setText(disposition.content);
		if (disposition.user_info!=null) {
			ImageLoader.getInstance().displayImage(disposition.user_info.getUser_face(), holder.ivImge);
		}
		return convertView;
	}

	static class ViewHolder {
		public ImageView ivImge;
		public TextView tvLabel;
	}
}
