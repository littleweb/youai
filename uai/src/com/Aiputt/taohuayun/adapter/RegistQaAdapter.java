package com.Aiputt.taohuayun.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.QustionOption;

public class RegistQaAdapter extends SuperAdapter<QustionOption>{


	

	public RegistQaAdapter(Context context, List<QustionOption> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = View
					.inflate(mContext, R.layout.item_qa, null);
			holder = new ViewHolder();
			holder.tvLabel = (TextView) convertView.findViewById(R.id.tvContent);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		QustionOption registerQa = mList.get(position);
		holder.tvLabel.setText(registerQa.content);
		return convertView;
	}

	static class ViewHolder {
		public TextView tvLabel;
	}



}
