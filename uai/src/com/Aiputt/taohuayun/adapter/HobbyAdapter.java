package com.Aiputt.taohuayun.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.Hobby;

import java.util.List;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class HobbyAdapter extends BaseAdapter {

    private List<Hobby> hobbyList;
    private Context mContext;

    public HobbyAdapter(Context mContext, List<Hobby> hobbyList) {
        this.mContext = mContext;
        this.hobbyList = hobbyList;
    }

    @Override
    public int getCount() {
        return hobbyList.size();
    }

    @Override
    public Object getItem(int position) {
        return hobbyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.dialog_multiply_choice_item, null);
            holder = new ViewHolder();
            holder.tvLabel = (TextView) convertView.findViewById(R.id.label);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Hobby hobby = hobbyList.get(position);
        if(hobby.getType()==1){
            holder.tvLabel.setBackgroundResource(R.drawable.bg_md_item_pre);
            holder.tvLabel.setTextColor(mContext.getResources().getColor(R.color.white));
        }else{
            holder.tvLabel.setBackgroundResource(R.drawable.bg_md_item_nor);
            holder.tvLabel.setTextColor(mContext.getResources().getColor(R.color.gray));
        }
        holder.tvLabel.setText(hobby.getDescription());
        return convertView;
    }
    static class ViewHolder {
        public TextView tvLabel;
    }

}
