package com.Aiputt.taohuayun.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.AgeInfo;
import com.Aiputt.taohuayun.domain.NotifyMessage;

public class NotifyMessageAdapter extends SuperAdapter<NotifyMessage> {



	public NotifyMessageAdapter(Context context, List<NotifyMessage> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}
	public NotifyMessageAdapter(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = View
					.inflate(mContext, R.layout.item_notify_message, null);
			holder = new ViewHolder();
			holder.tvLabel = (TextView) convertView.findViewById(R.id.label);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		NotifyMessage disposition = mList.get(position);
		holder.tvLabel.setText(disposition.message);
		return convertView;
	}

	static class ViewHolder {
		public TextView tvLabel;
	}
}
