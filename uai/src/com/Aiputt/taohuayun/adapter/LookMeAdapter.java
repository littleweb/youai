package com.Aiputt.taohuayun.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhonglq on 2015/8/8.
 */
public class LookMeAdapter extends RecyclerView.Adapter<LookMeAdapter.ViewHolder> {

    private Context context;
    private List<User> userList = new ArrayList<User>();

    private OnRecentVisitItemClickListener onItemClickListener;

    public void setOnRecentVisitItemClickListener(OnRecentVisitItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public LookMeAdapter(Context context) {
        this.context = context;
    }

    public void replace(List<User> users) {
        if (userList == null) {
            userList = new ArrayList<User>();
        }
        userList.clear();
        userList.addAll(users);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = View.inflate(context, R.layout.user_head_photo_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        User user = userList.get(i);
        String tag = (String) viewHolder.photoItem.getTag();
        if(TextUtils.isEmpty(tag)){
            ImageLoader.getInstance().displayImage(user.getUser_face(), viewHolder.photoItem, ImageLoadUtils.getHeadPhotoOptions());
            viewHolder.photoItem.setTag(user.getUser_face());
        }else if(!tag.equals(user.getUser_face())){
            ImageLoader.getInstance().displayImage(user.getUser_face(), viewHolder.photoItem, ImageLoadUtils.getHeadPhotoOptions());
            viewHolder.photoItem.setTag(user.getUser_face());
        }
        if (onItemClickListener != null) {
            viewHolder.photoItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onRecentVisitItemClick(i);
                }
            });
        }
    }

    public User getCurrentItem(int i) {
        return userList.get(i);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }


    public interface OnRecentVisitItemClickListener {
        void onRecentVisitItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView photoItem;
        public TextView reviewing;

        public ViewHolder(View itemView) {
            super(itemView);
            photoItem = (RoundedImageView) itemView.findViewById(R.id.ivHeadPhotoItem);
            reviewing = (TextView) itemView.findViewById(R.id.reviewing);
        }
    }
}
