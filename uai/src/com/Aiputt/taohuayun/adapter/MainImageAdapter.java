//package com.Aiputt.taohuayun.adapter;
//
//import java.util.List;
//
//import android.content.Context;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.Aiputt.taohuayun.R;
//import com.Aiputt.taohuayun.domain.User;
//import com.Aiputt.taohuayun.domain.UserList;
//import com.nostra13.universalimageloader.core.ImageLoader;
//
//public class MainImageAdapter extends SuperAdapter<UserList> {
//
//	public MainImageAdapter(Context context, List<UserList> list) {
//		super(context, list);
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		ViewHolder holder;
//		if (convertView == null) {
//			convertView = View.inflate(mContext, R.layout.item_main, null);
//			holder = new ViewHolder();
//			holder.ivDazhaohu1 = (ImageView) convertView.findViewById(R.id.ivDazhaohu1);
//			holder.ivDazhaohu2 = (ImageView) convertView.findViewById(R.id.ivDazhaohu2);
//			holder.ivDazhaohu3 = (ImageView) convertView.findViewById(R.id.ivDazhaohu3);
//			holder.ivDazhaohu4 = (ImageView) convertView.findViewById(R.id.ivDazhaohu4);
//			holder.ivDazhaohu5 = (ImageView) convertView.findViewById(R.id.ivDazhaohu5);
//			holder.ivDazhaohu6 = (ImageView) convertView.findViewById(R.id.ivDazhaohu6);
//			holder.ivDazhaohu7 = (ImageView) convertView.findViewById(R.id.ivDazhaohu7);
//			holder.ivDazhaohu8 = (ImageView) convertView.findViewById(R.id.ivDazhaohu8);
//			holder.ivDazhaohu9 = (ImageView) convertView.findViewById(R.id.ivDazhaohu9);
//			
//			
//			holder.iv1 = (ImageView) convertView.findViewById(R.id.iv1);
//			holder.iv2 = (ImageView) convertView.findViewById(R.id.iv2);
//			holder.iv3 = (ImageView) convertView.findViewById(R.id.iv3);
//			holder.iv4 = (ImageView) convertView.findViewById(R.id.iv4);
//			holder.iv5 = (ImageView) convertView.findViewById(R.id.iv5);
//			holder.iv6 = (ImageView) convertView.findViewById(R.id.iv6);
//			holder.iv7 = (ImageView) convertView.findViewById(R.id.iv7);
//			holder.iv8 = (ImageView) convertView.findViewById(R.id.iv8);
//			holder.iv9 = (ImageView) convertView.findViewById(R.id.iv9);
//			holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
//			holder.tvAge = (TextView) convertView.findViewById(R.id.tvAge);
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolder) convertView.getTag();
//		}
//		UserList userList = mList.get(position);
//		if (userList != null && userList.users != null) {
//			if (userList.users.size() >= 1) {
//				User user = userList.users.get(0);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv1);
//				holder.tvName.setText(user.getUser_nick());
//				holder.tvAge.setText(user.getUser_age() + "岁");
//			}
//			if (userList.users.size() >= 2) {
//				User user = userList.users.get(1);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv2);
//			}
//			if (userList.users.size() >= 3) {
//				User user = userList.users.get(2);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv3);
//			}
//			if (userList.users.size() >= 4) {
//				User user = userList.users.get(3);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv4);
//			}
//			if (userList.users.size() >= 5) {
//				User user = userList.users.get(4);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv5);
//			}
//			if (userList.users.size() >= 6) {
//				User user = userList.users.get(5);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv6);
//			}
//			if (userList.users.size() >= 7) {
//				User user = userList.users.get(6);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv7);
//			}
//			if (userList.users.size() >= 8) {
//				User user = userList.users.get(7);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv8);
//			}
//			if (userList.users.size() >= 9) {
//				User user = userList.users.get(8);
//				ImageLoader.getInstance().displayImage(user.getImg_url(),
//						holder.iv9);
//			}
//		}
//		return convertView;
//	}
//
//	static class ViewHolder {
//		public ImageView ivDazhaohu9;
//		public ImageView ivDazhaohu8;
//		public ImageView ivDazhaohu7;
//		public ImageView ivDazhaohu6;
//		public ImageView ivDazhaohu5;
//		public ImageView ivDazhaohu4;
//		public ImageView ivDazhaohu3;
//		public ImageView ivDazhaohu2;
//		public ImageView ivDazhaohu1;
//		public ImageView iv1;
//		public ImageView iv2;
//		public ImageView iv3;
//		public ImageView iv4;
//		public ImageView iv5;
//		public ImageView iv6;
//		public ImageView iv7;
//		public ImageView iv8;
//		public ImageView iv9;
//		public TextView tvName;
//		public TextView tvAge;
//	}
//
//}
