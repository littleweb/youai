package com.Aiputt.taohuayun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.Aiputt.taohuayun.utils.CommonUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class SuperAdapter<T> extends BaseAdapter {
	protected LayoutInflater mInflater;
	protected List<T> mList;
	protected Context mContext;
    protected ImageLoader mImageLoader;

	public SuperAdapter(List<T> list) {
		this(CommonUtil.getApplication(), list);
	}

	public SuperAdapter(Context context) {
		this(context, null);
	}

	public SuperAdapter(Context context, List<T> list) {
        mImageLoader = ImageLoader.getInstance();
		mContext = context;
		mList = list;
		mInflater = LayoutInflater.from(context);
	}

	public List<T> getList() {
		return mList;
	}

	public void addAll(Collection<T> data) {
		if (data == null)
			return;

		if (mList == null) {
			mList = new ArrayList<T>(data);
		} else {
			mList.addAll(data);
		}
	}

	public void setList(List<T> list) {
		mList = list;
		notifyDataSetChanged();
	}

    public void replaceAll(List<T> list){
        if(mList == null){
            mList = new ArrayList<T>();
        }
        mList.clear();
        mList.addAll(list);
    }

	public void setList(T[] array) {
		List<T> list = new ArrayList<T>(array.length);
		for (T t : array) {
			list.add(t);
		}
		setList(list);
	}

	@Override
	public int getCount() {
		if (mList != null)
			return mList.size();
		else
			return 0;
	}

	@Override
	public T getItem(int position) {
		return mList == null ? null : mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	abstract public View getView(int position, View convertView,
			ViewGroup parent);

}
