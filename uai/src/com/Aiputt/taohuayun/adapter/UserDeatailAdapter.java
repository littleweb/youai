package com.Aiputt.taohuayun.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhonglq on 2015/8/8.
 */
public class UserDeatailAdapter extends RecyclerView.Adapter<UserDeatailAdapter.ViewHolder> {

    private Context context;
    private List<ImageList> imageLists = new ArrayList<ImageList>();
    private boolean isMineSpace;

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public UserDeatailAdapter(Context context, boolean isMineSpace) {
        this.context = context;
        this.isMineSpace = isMineSpace;
        imageLists.add(new ImageList());
    }

    public void replace(List<ImageList> newList) {
        if (imageLists == null) {
            imageLists = new ArrayList<ImageList>();
        }
        imageLists.clear();
        imageLists.add(new ImageList());
        imageLists.addAll(newList);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = View.inflate(context, R.layout.user_head_photo_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        ImageList imageList = imageLists.get(i);
        if (i == 0) {
            if (isMineSpace) {
                viewHolder.photoItem.setImageResource(R.drawable.photo_up);
            } else {
                viewHolder.photoItem.setImageResource(R.drawable.ic_request_photo);
            }
        } else {
            ImageLoader.getInstance().displayImage(imageList.getImg_url_small(), viewHolder.photoItem, ImageLoadUtils.getHeadPhotoOptions());
            if (imageList.getStatus().equals("0")) {
                viewHolder.reviewing.setVisibility(View.VISIBLE);
            } else {
                viewHolder.reviewing.setVisibility(View.GONE);
            }
        }
        if (onItemClickListener != null) {
            viewHolder.photoItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(i);
                }
            });
        }
    }

    public ImageList getCurrentItem(int i) {
        return imageLists.get(i);
    }

    @Override
    public int getItemCount() {
        return imageLists.size();
    }

    public void addImageList(ImageList imageList) {
        imageLists.add(imageList);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView photoItem;
        public TextView reviewing;

        public ViewHolder(View itemView) {
            super(itemView);
            photoItem = (RoundedImageView) itemView.findViewById(R.id.ivHeadPhotoItem);
            reviewing = (TextView) itemView.findViewById(R.id.reviewing);
        }
    }
}
