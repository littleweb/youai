package com.Aiputt.taohuayun.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.AgeInfo;

public class AgeAdapter extends BaseAdapter {

	private List<AgeInfo> ageInfos;
	private Context mContext;

	public AgeAdapter(Context mContext, List<AgeInfo> ageInfos) {
		this.mContext = mContext;
		this.ageInfos = ageInfos;
	}

	@Override
	public int getCount() {
		return ageInfos.size();
	}

	@Override
	public Object getItem(int position) {
		return ageInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = View
					.inflate(mContext, R.layout.dialog_age_item, null);
			holder = new ViewHolder();
			holder.tvLabel = (TextView) convertView.findViewById(R.id.label);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		AgeInfo disposition = ageInfos.get(position);
		holder.tvLabel.setText(disposition.value);
		return convertView;
	}

	static class ViewHolder {
		public TextView tvLabel;
	}
}
