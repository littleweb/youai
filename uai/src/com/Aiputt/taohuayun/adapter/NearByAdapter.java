
package com.Aiputt.taohuayun.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.widgets.RoundedImageView;

import java.util.List;

/**
 * Created by zhonglq on 2015/7/21.
 */
public class NearByAdapter extends SuperAdapter<User> {

    private OnClickSayHiListener onClickSayHiListener;

    public void setOnClickSayHiListener(OnClickSayHiListener onClickSayHiListener) {
        this.onClickSayHiListener = onClickSayHiListener;
    }

    public NearByAdapter(Context context) {
        super(context);
    }

    public NearByAdapter(List<User> list) {
        super(list);
    }

    public NearByAdapter(Context context, List<User> list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.fragment_nearby_person_list_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final User user = mList.get(position);
//        holder.headPhoto.setImageResource(R.drawable.round_image_head_girl);
        holder.nickName.setText(user.getUser_nick());
        holder.personAge.setText(user.getUser_age() + "岁");
        if (TextUtils.isEmpty(user.getUser_feature())) {
            holder.personChracter.setVisibility(View.GONE);
        } else {
            holder.personChracter.setVisibility(View.VISIBLE);
            holder.personChracter.setText(user.getUser_feature());
        }
        if (TextUtils.isEmpty(user.getUser_hobby())) {
            holder.personHobby.setVisibility(View.GONE);
        } else {
            holder.personHobby.setVisibility(View.VISIBLE);
            holder.personChracter.setText(user.getUser_hobby());
        }
        holder.personHeight.setText(user.getUser_height() + "cm");
        holder.personIncome.setText(user.getUser_income());
        holder.photoCount.setText(user.getImg_cnt() + "张");
        holder.tvnearbypersonitemdistance.setText(Float.parseFloat(user.getDistance()) / 1000 + "km");
        if (user.getIsSayHello() == 1) {
            holder.sayHello.setBackgroundResource(R.drawable.n_ck);
        } else {
            holder.sayHello.setBackgroundResource(R.drawable.n_ck_press);
        }
        holder.sayHello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.getIsSayHello() == 0 && onClickSayHiListener != null) {
                    onClickSayHiListener.onClickSayHi(user);
                }
            }
        });
        holder.persobLocation.setText(user.getUser_province());
        if (!user.getUser_face().equals(holder.headPhoto.getTag())) {
            mImageLoader.displayImage(user.getUser_face(), holder.headPhoto, ImageLoadUtils.getHeadPhotoOptions());
        }
        holder.headPhoto.setTag(user.getUser_face());
        return convertView;
    }

    public interface OnClickSayHiListener {
        void onClickSayHi(User user);
    }

    public class ViewHolder {
        public final RoundedImageView headPhoto;
        public final TextView nickName;
        public final TextView tvnearbypersonitemdistance;
        public final LinearLayout llnearbypersonitemdis;
        public final TextView personAge;
        public final TextView persobLocation;
        public final TextView personHeight;
        public final TextView personIncome;
        public final TextView personHobby;
        public final TextView personChracter;
        public final TextView photoCount;
        public final LinearLayout llnearbypersonitemcharacter;
        public final TextView sayHello;
        public final View root;

        public ViewHolder(View root) {
            headPhoto = (RoundedImageView) root.findViewById(R.id.headPhoto);
            nickName = (TextView) root.findViewById(R.id.nickName);
            tvnearbypersonitemdistance = (TextView) root.findViewById(R.id.tv_nearby_person_item_distance);
            llnearbypersonitemdis = (LinearLayout) root.findViewById(R.id.ll_nearby_person_item_dis);
            personAge = (TextView) root.findViewById(R.id.personAge);
            personHeight = (TextView) root.findViewById(R.id.tvHeight);
            persobLocation = (TextView) root.findViewById(R.id.persobLocation);
            personIncome = (TextView) root.findViewById(R.id.personIncome);
            personHobby = (TextView) root.findViewById(R.id.personHobby);
            personChracter = (TextView) root.findViewById(R.id.personChracter);
            photoCount = (TextView) root.findViewById(R.id.photoCount);
            llnearbypersonitemcharacter = (LinearLayout) root.findViewById(R.id.ll_nearby_person_item_character);
            sayHello = (TextView) root.findViewById(R.id.sayHello);
            this.root = root;
        }
    }
}
