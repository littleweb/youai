package com.Aiputt.taohuayun.utils;

import android.graphics.Bitmap;

import com.Aiputt.taohuayun.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class ImageLoadUtils {

    /**
     * 获取一般的加载配置选项
     * @return
     */
    public static DisplayImageOptions getNormalOptions() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_image)
                .showImageForEmptyUri(R.drawable.default_image)
                .showImageOnFail(R.drawable.default_image)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .resetViewBeforeLoading(true)
                .displayer(new FadeInBitmapDisplayer(100))
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return options;
    }
    /**
     * 获取头像的加载配置选项
     * @return
     */
    public static DisplayImageOptions getHeadPhotoOptions() {
        DisplayImageOptions options_head = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_image)
                .showImageForEmptyUri(R.drawable.default_image)
                .showImageOnFail(R.drawable.default_image)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .displayer(new RoundedBitmapDisplayer(10))
                .imageScaleType(ImageScaleType.EXACTLY)
                .resetViewBeforeLoading(true)
                .displayer(new FadeInBitmapDisplayer(100))
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        return options_head;
    }

}
