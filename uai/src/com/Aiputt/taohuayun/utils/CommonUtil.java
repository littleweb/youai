package com.Aiputt.taohuayun.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import com.Aiputt.taohuayun.domain.NetworkOperator;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class CommonUtil {

    private static final String RUNTIME_MODE = "RUNTIME_MODE";
    private static final String RELEASE_MODE = "release";
    private static final String DEBUG_MODE = "debug";

    private static final String DEFAULT_UUID = "UNKOWN_ANDROID_USER";
    private static final String DEBUG_SIGN_MD5 = "37c5f570b51b6a463dfb52710b40193f";
    /**
     * 全局唯一实例
     */
    private final static Gson mGson = new Gson();
    private static boolean DEBUG = false;

    /**
     * 全局Application 引用
     */
    private static Context context;

    private static String packageName;
    private static String versionName;
    private static int versionCode;
    private static Bundle metaData;

    public static void init(Application application) {
        context = application;

        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = info.versionName;
            versionCode = info.versionCode;

            packageName = context.getPackageName();

            ApplicationInfo appInfo = pm.getApplicationInfo(packageName,
                    PackageManager.GET_META_DATA);

            metaData = appInfo.metaData;


        } catch (PackageManager.NameNotFoundException e) {
            // D.e(e.toString());
        }
    }

    public static String getApplicationName() {
        return packageName;
    }

    /**
     * 获取全局引用值
     */
    public static Context getApplication() {
        return context;
    }

    /**
     * 获取应用版本号
     *
     * @return String
     */
    public static String getVersionName() {
        return versionName;
    }

    /**
     * 获取应用版本标识,用于检测更新
     *
     * @return int
     */
    public static int getVersionCode() {
        return versionCode;
    }

    /**
     * 网络是否连接
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetInfo != null && activeNetInfo.isConnectedOrConnecting();
    }

    /**
     * wifi是否连接
     */
    public static boolean isWifiAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo mWifi = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public static boolean isDebugMode() {
        return DEBUG;
    }

    /**
     * 判断当前设备是否是模拟器。如果返回TRUE，则当前是模拟器，不是返回FALSE
     *
     * @param context
     * @return
     */
    public static boolean isEmulator(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String imei = tm.getDeviceId();
            if ("000000000000000".equals(imei)) {
                return true;
            }

            boolean isE = (Build.MODEL.equals("sdk"))
                    || (Build.MODEL.equals("google_sdk") || Build.MODEL
                    .equals("Droid4X-WIN"));
            // ymt modified later
            /*
             * StatEvent statEvent = new StatEvent("emulator");
			 * statEvent.put("model", Build.MODEL);
			 * StatEventData.statTrack(statEvent);
			 */
            return isE;
        } catch (Exception e) {

        }
        return false;
    }

    public static NetworkOperator getNetworkOperator(Context context) {
        TelephonyManager iPhoneManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String iNumeric = iPhoneManager.getSubscriberId();
        if (!TextUtils.isEmpty(iNumeric)) {
            if (iNumeric.startsWith("46000") || iNumeric.startsWith("46002")) {
                return NetworkOperator.CHINA_MOBILE;
            } else if (iNumeric.startsWith("46001")) {
                return NetworkOperator.CHINA_UNICOM;
            } else if (iNumeric.startsWith("46003")) {
                return NetworkOperator.CHINA_TELECOM;
            }
        }
        return null;
    }

    /**
     * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
     *
     * @param context
     * @return true 表示开启
     */
    public static boolean isOPen(Context context) {
        LocationManager locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * 获取app设备标示
     */
    public static String getUUID() {
        String uuid = getDeviceId();
        if (TextUtils.isEmpty(uuid)) {
            uuid = getMacAddress();
        }

        if (TextUtils.isEmpty(uuid)) {
            uuid = getAndroidId();
        }

        if (TextUtils.isEmpty(uuid)) {
            uuid = DEFAULT_UUID;
        }
        return md5Encode(uuid);
    }

    public static String getMacAddress() {
        try {
            WifiManager localWifiManager = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo localWifiInfo = localWifiManager.getConnectionInfo();
            return localWifiInfo.getMacAddress();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getDeviceId() {
        try {
            TelephonyManager manager = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            return manager.getDeviceId();
        } catch (Exception e) {
            return null;
        }
    }

    private static String getAndroidId() {
        String id = Settings.Secure.getString(context.getContentResolver(),
                "android_id");
        return id;
    }

    /**
     * 获取友盟的渠道号
     */
    public static String getUmengChannel() {
        Bundle metaData = getMetaData();
        if (metaData == null)
            return null;

        return metaData.getString("UMENG_CHANNEL");
    }

    public static Bundle getMetaData() {
        return metaData;
    }

    /**
     * 获取app屏幕尺寸
     */
    public static DisplayMetrics getDisplayMetrics() {
        return context.getResources().getDisplayMetrics();
    }

    public static String md5Encode(byte[] data) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(data);

            byte[] byteArray = messageDigest.digest();
            StringBuilder buffer = new StringBuilder();

            for (byte item : byteArray) {
                if (Integer.toHexString(0xFF & item).length() == 1) {
                    buffer.append("0").append(Integer.toHexString(0xFF & item));
                } else {
                    buffer.append(Integer.toHexString(0xFF & item));
                }
            }

            return buffer.toString();
        } catch (Exception e) {
            // D.e(e.toString());
        }
        return null;
    }

    /**
     * MD5加密
     *
     * @param str 待加密字符串
     * @return 加密后字符串
     */
    public static String md5Encode(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            return md5Encode(str.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            if (isDebugMode()) {
                Log.e("CommonUtil", "md5 加密出错，[str:" + str + "]", e);
            }
        }

        return null;
    }

    public static String getMobileTypeNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return "other";
        } else if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            return "wifi";
        } else if (networkInfo.getType() == ConnectivityManager.TYPE_WIMAX) {
            return "wimax";
        } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
            return networkInfo.getSubtypeName();
        } else {
            return "other";
        }
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * json转化为java实体对象
     */
    public static <T> T fromJson(String json, Class<T> clazz) {
        return mGson.fromJson(json, clazz);
    }

    /**
     * json转化为java实体对象
     */
    public static <T> T fromJson(String json, Type type) {
        return mGson.fromJson(json, type);
    }

    /**
     * java 实体对象转化为json字符串
     */
    public static String toJson(Object entity) {
        return mGson.toJson(entity);
    }

    /**
     * 获取进程对应的名称
     *
     * @param pid , 进程id
     */
    public static String getProcessName(int pid) {
        ActivityManager mActivityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processes = mActivityManager
                .getRunningAppProcesses();
        if (processes != null) {
            for (ActivityManager.RunningAppProcessInfo appProcess : processes) {
                if (appProcess.pid == pid) {
                    return appProcess.processName;
                }
            }
        }
        return String.format("Process_%s", pid);
    }

    public static boolean externalStorageEnable() {
        return Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState());
    }

    public static boolean isRunningForeground() {
        boolean isForeGround = false;
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        String currentPackageName = cn.getPackageName();
        if (!TextUtils.isEmpty(currentPackageName)
                && currentPackageName.equals(context.getPackageName())) {
            isForeGround = true;
        }
        return isForeGround;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean isHighEndGfx(Activity activity) {
        ActivityManager am = (ActivityManager) activity
                .getSystemService(Context.ACTIVITY_SERVICE);
        if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
            MemoryInfo outInfo = new MemoryInfo();
            am.getMemoryInfo(outInfo);
            if (outInfo.totalMem >= (1024 * 1024 * 1024)) {
                return true;
            }
            return false;
        } else if (VERSION.SDK_INT < VERSION_CODES.JELLY_BEAN
                && VERSION.SDK_INT >= VERSION_CODES.ICE_CREAM_SANDWICH) {
            Display display = activity.getWindowManager().getDefaultDisplay();
            try {
                Method method = am.getClass().getDeclaredMethod("isHighEndGfx",
                        Display.class);
                if (method != null) {
                    Boolean ret = (Boolean) method.invoke(am,
                            new Object[]{display});
                    if (ret != null) {
                        return ret;
                    }
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private static String mChannel = "";

    public static String getCustomChannelInfo() {

        if (!TextUtils.isEmpty(mChannel)) {
            return mChannel;
        }

        mChannel = "999999";

        ApplicationInfo appinfo = context.getApplicationInfo();
        String sourceDir = appinfo.sourceDir;

        ZipFile zf = null;
        InputStream in = null;
        ZipInputStream zin = null;

        try {
            zf = new ZipFile(sourceDir);

            in = new BufferedInputStream(new FileInputStream(sourceDir));
            zin = new ZipInputStream(in);

            ZipEntry ze;
            Enumeration<?> entries = zf.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = ((ZipEntry) entries.nextElement());
                if (entry.getName().equalsIgnoreCase("META-INF/channel_info")) {
                    long size = entry.getSize();
                    if (size > 0) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(zf.getInputStream(entry)));
                        String line;
                        while ((line = br.readLine()) != null) {
                            mChannel = line;
                        }

                        br.close();
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {

                }
            }

            if (zin != null) {
                try {
                    zin.closeEntry();
                } catch (Exception e) {

                }
            }

            if (zf != null) {
                try {
                    zin.closeEntry();
                } catch (Exception e) {

                }
            }
        }

        return mChannel;

    }

}
