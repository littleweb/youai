package com.Aiputt.taohuayun.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.Aiputt.taohuayun.R;

import cn.jpush.android.api.JPushInterface;

public class MsgUtil {

    private static final String TAG = MsgUtil.class.getSimpleName();

    private static final int NOTIFY_TYPE_ACCOUNT_MSG = 0;
    private static final int NOTIFY_TYPE_SYSTEM_MSG = 1;
    
    
    public static void cancelNotification(Context context, int notifiId) {
	NotificationManager manager =
		    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    manager.cancel(notifiId);
    }

    public static void clearAccountNotify(Context ctx) {
        NotificationManager nm = (NotificationManager) ctx
                .getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(NOTIFY_TYPE_ACCOUNT_MSG);
    }



    public static void handleMessage(Context ctx, Intent it) {
        Bundle bd = it.getExtras();
        if (bd != null) {
            String messageString = it
                    .getStringExtra(JPushInterface.EXTRA_EXTRA);
            if (!TextUtils.isEmpty(messageString)) {
               System.out.println("====receive msg------"+messageString);
            }
        }
    }

   
    private static void showNotify(Context ctx, Intent targetIntent,
            String title, String msg, int type) {
        Notification notification = new Notification(R.drawable.ic_launcher,
                msg, System.currentTimeMillis());
        notification.flags = Notification.DEFAULT_LIGHTS
                | Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_SOUND;
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0,
                targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(ctx, title, msg, pendingIntent);
        NotificationManager nm = (NotificationManager) ctx
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (type == 1) {
            nm.notify(NOTIFY_TYPE_ACCOUNT_MSG, notification);
        } else {
            nm.notify(NOTIFY_TYPE_SYSTEM_MSG, notification);
        }
    }

    
}
