package com.Aiputt.taohuayun.utils;

import com.baidu.location.BDLocation;
import com.baidu.location.LocationClientOption;

/**
 * Created by zhonglq on 2015/7/19.
 */
public class LocationUtil {

    private static LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Hight_Accuracy;
    private static String tempcoor = "bd09ll";//gcj02  bd09
    private static String latitude;
    private static String longitude;
    private static String city;

    private LocationUtil(){}
    private static LocationUtil locationUtil = new LocationUtil();

    public static LocationUtil getInstance(){
        return locationUtil;
    }

    private BDLocation bdLocation;

    public static void setLatitude(String latitude) {
        LocationUtil.latitude = latitude;
    }

    public static String getLatitude() {
        return latitude;
    }

    public static void setLongitude(String longitude) {
        LocationUtil.longitude = longitude;
    }

    public static String getLongitude() {
        return longitude;
    }

    public static void setCity(String city) {
        LocationUtil.city = city;
    }

    public static String getCity() {
        return city;
    }

    public void setBDLocation(BDLocation bdLocation){
        this.bdLocation = bdLocation;
    }

    public BDLocation getBdLocation(){
        return bdLocation;
    }

    /**
     * 获取百度定位选项配置信息
     * @return
     */
    public static LocationClientOption initLocation(){
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType(tempcoor);//可选，默认gcj02，设置返回的定位结果坐标系，
//        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        return option;
    }
}
