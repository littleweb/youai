package com.Aiputt.taohuayun.utils;

import java.security.Key;
import java.security.MessageDigest;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Administrator AES加密，还包括获取token的方法
 */
public class AESUtils {

    private static final String STATIC_KEY = "1234567890123456";

    private static final String SINSECRET = "86d51ce7753a36079041fc15f7248035";
//	private static final String SINSECRET = "916634c30abe02232c45294ea1c515bb";

    /**
     * 签名生成算法
     *
     * @param params <String,String> params 请求参数集，所有参数必须已转换为字符串类型
     * @return 签名
     */
    public static String getSignature(Map<String, String> params) {
        StringBuilder sign = new StringBuilder();
        try {
            // 先将参数以其参数名的字典序升序进行排序
            Map<String, String> sortedParams = new TreeMap<String, String>(
                    params);
            Set<Entry<String, String>> entrys = sortedParams.entrySet();

            // 遍历排序后的字典，将所有参数按"key=value"格式拼接在一起
            StringBuilder basestring = new StringBuilder();
            for (Entry<String, String> param : entrys) {
                basestring.append(param.getKey()).append("=")
                        .append(param.getValue());
            }
            basestring.append(SINSECRET);
            // 使用MD5对待签名串求签
            byte[] bytes = null;
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            bytes = md5.digest(basestring.toString().getBytes("UTF-8"));

            // 将MD5输出的二进制结果转换为小写的十六进制
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(bytes[i] & 0xFF);
                if (hex.length() == 1) {
                    sign.append("0");
                }
                sign.append(hex);
            }
        } catch (Exception e) {
            return null;
        }
        return sign.toString();
    }

    /**
     * @param data 加密数据
     */
    public static String AES_Decrypt(String data) {
        return AES_DecryptP(STATIC_KEY, data);
    }

    private static String AES_DecryptP(String keyStr, String encryptData) {
        byte[] decrypt = null;
        try {
            Key key = generateKey(keyStr);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decrypt = cipher.doFinal(android.util.Base64.decode(encryptData,
                    android.util.Base64.DEFAULT));
            return new String(decrypt);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String AES_Encrypt(String data) {
        return AES_EncryptP(STATIC_KEY, data);
    }

    private static String AES_EncryptP(String keyStr, String plainText) {
        byte[] encrypt = null;
        try {
            Key key = generateKey(keyStr);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encrypt = cipher.doFinal(plainText.getBytes());
            return new String(android.util.Base64.encode(encrypt,
                    android.util.Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Key generateKey(String key) throws Exception {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");
            return keySpec;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

}
