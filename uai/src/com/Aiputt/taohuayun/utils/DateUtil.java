package com.Aiputt.taohuayun.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	private static final String DOUBLE_ZERO = "00";
	private static final String SINGLE_ZERO = "0";
	private static final String COMMA = ":";

	/**
	 * 根据毫秒数来返回日期包括时间
	 * 
	 * @param time
	 *            毫秒数
	 * @return
	 */
	public static String get24Time(long time) {
		Date today = new Date(time);
		SimpleDateFormat f = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
		return f.format(today);
	}

	/**
	 * 根据毫秒数来返回日期年月日
	 * 
	 * @param time
	 *            毫秒数
	 * @return
	 */
	public static String getYMDTime(long time) {
		Date today = new Date(time);
		SimpleDateFormat f = new SimpleDateFormat("yyyy.MM.dd");
		return f.format(today);
	}

	/**
	 * 计算两个日期之间相差的天数
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int daysBetween(Date date1, Date date2) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		long time1 = cal.getTimeInMillis();
		cal.setTime(date2);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);
		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 在当前日期上加N天后的日期
	 * 
	 * @param n
	 * @return
	 */
	public static String getNextNDay(int n) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) + n);
		return format.format(c.getTime());
	}

	public static CharSequence getDisplayForDuration(long duration) {
		long hour = duration / 3600;
		long minute = (duration - hour * 3600) / 60;
		long second = duration % 60;

		StringBuilder sb = new StringBuilder();
		if (hour != 0) {
			if (hour >= 10) {
				sb.append(hour);
			} else {
				sb.append(SINGLE_ZERO);
				sb.append(hour);
			}
		} else {
			sb.append(DOUBLE_ZERO);
		}
		sb.append(COMMA);

		if (minute != 0) {
			if (minute >= 10) {
				sb.append(minute);
			} else {
				sb.append(SINGLE_ZERO);
				sb.append(minute);
			}
		} else {
			sb.append(DOUBLE_ZERO);
		}
		sb.append(COMMA);

		if (second != 0) {
			if (second >= 10) {
				sb.append(second);
			} else {
				sb.append(SINGLE_ZERO);
				sb.append(second);
			}
		} else {
			sb.append(DOUBLE_ZERO);
		}

		return sb.toString();
	}
}
