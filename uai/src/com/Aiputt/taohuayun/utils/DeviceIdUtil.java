package com.Aiputt.taohuayun.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

public class DeviceIdUtil {

	static final String TAG = "DeviceIdUtil";

	private static final String ROOT_DIR = "rong360";
	private static final String SP_UNIQUE_ID = "sp_unique_id";
	
	private static final String GLOBAL_SP = "rong360_global_sp";
	private static final String SD_UNIQUE_ID = "sd_unique_id";

	/**
	 * 获取设备ID
	 * 
	 * @return
	 */
	public static String getDeviceId(Context context) {
		if (context == null) {
			return "";
		}

		String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		if (isValidDeviceId(deviceId)) {
			return deviceId;
		}

		try {
			String deviceIdSP = getUniqueIdFromSP(context);
			String deviceIdSD = readUniqueIdFromSD(context);
			if (TextUtils.isEmpty(deviceIdSP) && TextUtils.isEmpty(deviceIdSD)) {
				deviceId = generateRandomDeviceId();
				if (TextUtils.isEmpty(deviceId)) {
					return "";
				}
				writeUniqueIdToSD(context, deviceId);
				putUniqueIdToSP(context, deviceId);
			} else if (!TextUtils.isEmpty(deviceIdSP) && TextUtils.isEmpty(deviceIdSD)) {
				deviceId = deviceIdSP;
				writeUniqueIdToSD(context, deviceId);
			} else if (TextUtils.isEmpty(deviceIdSP) && !TextUtils.isEmpty(deviceIdSD)) {
				deviceId = deviceIdSD;
				putUniqueIdToSP(context, deviceId);
			} else {
				deviceId = deviceIdSD;
				if (!deviceIdSP.equals(deviceIdSD)) {
					putUniqueIdToSP(context, deviceIdSD);
				}
			}
		} catch (IOException e) {
			Log.e(TAG, "IOException: " + e.getMessage());
			return "";
		}

		return deviceId;
	}

	private static boolean isValidDeviceId(String deviceId) {
		return !TextUtils.isEmpty(deviceId)
				 && deviceId.length() >= 6
				 && !deviceId.startsWith("000000")
				 && !deviceId.equals("0")
				 && !deviceId.startsWith("111111");
	}

	private static String generateRandomDeviceId() {
		String deviceId = "";
		try {
			deviceId = UUID.randomUUID().toString().replace("-", "_");
		} catch (Exception e) {
			Log.e(TAG, "UUID Exception: " + e.getMessage());
		}
		return deviceId;
	}

	private static String readUniqueIdFromSD(Context context) throws IOException {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			String deviceId = "";
			File file = new File(new File(Environment.getExternalStorageDirectory(), ROOT_DIR), SD_UNIQUE_ID);
			if (file.exists()) {
				RandomAccessFile f = new RandomAccessFile(file, "r");
				byte[] bytes = new byte[(int) f.length()];
				f.readFully(bytes);
				f.close();
				deviceId = new String(bytes);
			}
			return deviceId;
		} else {
			throw new IOException("SD card is not exist");
		}
	}

	private static void writeUniqueIdToSD(Context context, String deviceId) throws IOException {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			File file = new File(new File(Environment.getExternalStorageDirectory(), ROOT_DIR), SD_UNIQUE_ID);
			file.mkdirs();
			FileOutputStream out = new FileOutputStream(file);
			out.write(deviceId.getBytes());
			out.close();
		} else {
			throw new IOException("SD card is not exist");
		}
	}

	private static String getUniqueIdFromSP(Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences(GLOBAL_SP, Context.MODE_PRIVATE);
		return sharedPrefs.getString(SP_UNIQUE_ID, null);
	}

	private static void putUniqueIdToSP(Context context, String deviceId) {
		if (!TextUtils.isEmpty(deviceId)) {
			SharedPreferences sharedPrefs = context.getSharedPreferences(GLOBAL_SP, Context.MODE_PRIVATE);
			sharedPrefs.edit().putString(SP_UNIQUE_ID, deviceId).commit();
		}
	}

}