package com.Aiputt.taohuayun.utils;


import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * 吐司帮助类
 * 
 * @Title: PromptManager.java
 * @Package com.rong.fastloan.util
 * @Description: TODO(用一句话描述该文件做什么)
 * @author zhonglq
 * @date 2014-12-10 下午3:47:19
 * @version V1.0
 */
public class PromptManager {

	static Context context;

	public static void init(Context context) {
		PromptManager.context = context;
	}

	/**
	 * 显示无网络连接toast
	 * 
	 */
	public static void showNoNetWorkToast() {
		Toast.makeText(context, "当前网络不可用,请检查网络连接设置", Toast.LENGTH_SHORT).show();
	}

	/**
	 * 显示1长土司
	 * 
	 * @param msg
	 */
	public static void showLongToast(String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}

	/**
	 * 显示1长土司
	 * 
	 * @param msgResId
	 */
	public static void showLongToast(int msgResId) {
		Toast.makeText(context, msgResId, Toast.LENGTH_LONG).show();
	}

	/**
	 * 显示0短土司
	 * 
	 * @param msg
	 */
	public static void showShortToast(String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 显示0短土司
	 * 
	 * @param msgResId
	 */
	public static void showShortToast(int msgResId) {
		Toast.makeText(context, msgResId, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 显示中间位置吐司
	 * 
	 * @param msg
	 */
	public static void showCenterToast(String msg) {
		Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}


}
