package com.Aiputt.taohuayun.utils;

import android.content.Context;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.Disposition;
import com.Aiputt.taohuayun.domain.Hobby;

import org.litepal.crud.DataSupport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Administrator on 2015/7/28.
 */
public class ProductUserPropertyUtils {

    private Context mContext;

    private static String[] STARARRAY = {"魔羯座", "水瓶座", "双鱼座", "牡羊座",
            "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座"};
    private static int[] DAYARRAY = {22, 20, 19, 21, 21, 21, 22, 23, 23, 23, 23, 22};  // 两个星座分割日

    /**
     * 获取身高值
     *
     * @return
     */
    public static String[] getHeightValue() {
        String[] data = new String[49];
        for (int i = 0; i < data.length; i++) {
            data[i] = 150 + i + "";
        }
        return data;
    }

    /**
     * 获取体重值
     *
     * @return
     */
    public static String[] getWeightValue() {
        String[] data = new String[150];
        for (int i = 0; i < data.length; i++) {
            data[i] = 50 + i + "";
        }
        return data;
    }

    /**
     * 获取血型
     *
     * @return
     */
    public static String[] getBloodTypeValue() {
        return new String[]{"A", "AB", "B", "O"};
    }

    /**
     * 获取年数
     *
     * @return
     */
    public static String[] getYearsVale() {
        String[] years = new String[50];
        for (int i = 0; i < years.length; i++) {
            years[i] = 1950 + i + "";
        }
        return years;
    }

    /**
     * 获取年数
     *
     * @return
     */
    public static String[] getAgesVale() {
        String[] years = new String[50];
        for (int i = 0; i < years.length; i++) {
            years[i] = 18 + i + "";
        }
        return years;
    }

    /**
     * 获取月份
     *
     * @return
     */
    public static String[] getMonthValue() {
        String[] months = new String[12];
        for (int i = 0; i < months.length; i++) {
            months[i] = i + 1 + "";
        }
        return months;
    }

    /**
     * 根据月份获取天数
     *
     * @return
     */
    public static String[] getDayValue(String data) {
        int day = getDaysByYearMonth(data);
        String[] days = new String[day];
        for (int i = 0; i < days.length; i++) {
            days[i] = i + 1 + "";
        }
        return days;
    }

    /**
     * 根据日期计算当月的天数
     *
     * @param dateStr
     * @return
     */
    public static int getDaysByYearMonth(String dateStr) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy/MM");
        try {
            calendar.setTime(simpleDate.parse(dateStr));
            return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 根据日期计算星座
     *
     * @param month
     * @param day
     * @return
     */
    public static String getConstellation(int month, int day) {
        int index = month;
        if (day < DAYARRAY[month - 1]) {
            index = index - 1;
        }
        // 返回索引指向的星座string
        return STARARRAY[index];
    }

    /**
     * 计算年龄
     *
     * @param year
     * @return
     */
    public static String getAge(int year) {
        Calendar calendar = Calendar.getInstance();
        int newYear = calendar.get(Calendar.YEAR);
        return String.valueOf(newYear - year + 1);
    }

    /**
     * 获取学历
     *
     * @return
     */
    public static String[] getEducation() {
        String[] educations = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_education);
        return educations;
    }

    /**
     * 获取住房情况
     *
     * @return
     */
    public static String[] getHousing() {
        String[] houses = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_housing);
        return houses;
    }

    /**
     * 获取住房情况
     *
     * @return
     */
    public static String[] getIncome() {
        String[] incomes = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_income);
        return incomes;
    }

    /**
     * 获取工作
     *
     * @return
     */
    public static String[] getJobs() {
        String[] jobs = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_job);
        return jobs;
    }

    /**
     * 获取婚姻状况
     *
     * @return
     */
    public static String[] getMarriages() {
        String[] marriages = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_marriage);
        return marriages;
    }

    /**
     * 获取异地恋
     *
     * @return
     */
    public static String[] getYidi() {
        String[] yidi = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_yidi);
        return yidi;
    }

    /**
     * 获取兴趣爱好
     *
     * @return
     */
    public static ArrayList<Hobby> getHobbyData() {
        ArrayList<Hobby> allHobbys = null;
        allHobbys = (ArrayList<Hobby>) DataSupport.findAll(Hobby.class);
        if (allHobbys == null || allHobbys.isEmpty()) {
            Hobby hobby1 = new Hobby(1, "上网");
            allHobbys.add(hobby1);
            Hobby hobby2 = new Hobby(2, "研究汽车");
            allHobbys.add(hobby2);
            Hobby hobby3 = new Hobby(3, "养小动物");
            allHobbys.add(hobby3);
            Hobby hobby4 = new Hobby(4, "摄影");
            allHobbys.add(hobby4);
            Hobby hobby5 = new Hobby(5, "看电影");
            allHobbys.add(hobby5);
            Hobby hobby6 = new Hobby(6, "听音乐");
            allHobbys.add(hobby6);
            Hobby hobby7 = new Hobby(7, "写作");
            allHobbys.add(hobby7);
            Hobby hobby8 = new Hobby(8, "购物");
            allHobbys.add(hobby8);
            Hobby hobby9 = new Hobby(9, "做手工艺");
            allHobbys.add(hobby9);
            Hobby hobby10 = new Hobby(10, "做手工");
            allHobbys.add(hobby10);
            Hobby hobby11 = new Hobby(11, "跳舞");
            allHobbys.add(hobby11);
            Hobby hobby12 = new Hobby(12, "看展览");
            allHobbys.add(hobby12);
            Hobby hobby13 = new Hobby(13, "烹饪");
            allHobbys.add(hobby13);
            Hobby hobby14 = new Hobby(14, "读书");
            allHobbys.add(hobby14);
            Hobby hobby15 = new Hobby(15, "会话");
            allHobbys.add(hobby15);
            Hobby hobby16 = new Hobby(16, "研究计算机");
            allHobbys.add(hobby16);
            Hobby hobby17 = new Hobby(17, "做运动");
            allHobbys.add(hobby17);
            Hobby hobby18 = new Hobby(18, "旅游");
            allHobbys.add(hobby18);
            Hobby hobby19 = new Hobby(19, "玩电玩");
            allHobbys.add(hobby19);
            Hobby hobby20 = new Hobby(20, "其它");
            allHobbys.add(hobby20);
            DataSupport.saveAll(allHobbys);
        }
        return allHobbys;
    }

    /**
     * 获取个性特征
     *
     * @return
     */
    public static ArrayList<Disposition> getDispositionData() {
        ArrayList<Disposition> allDisposition = null;
        allDisposition = (ArrayList<Disposition>) DataSupport.findAll(Disposition.class);
        if (allDisposition == null || allDisposition.isEmpty()) {
            Disposition disposition1 = new Disposition(1, "宅");
            allDisposition.add(disposition1);
            Disposition disposition2 = new Disposition(2, "感性");
            allDisposition.add(disposition2);
            Disposition disposition3 = new Disposition(3, "体贴");
            allDisposition.add(disposition3);
            Disposition disposition4 = new Disposition(4, "憨厚");
            allDisposition.add(disposition4);
            Disposition disposition5 = new Disposition(5, "稳重");
            allDisposition.add(disposition5);
            Disposition disposition6 = new Disposition(6, "好强");
            allDisposition.add(disposition6);
            Disposition disposition7 = new Disposition(7, "冷静");
            allDisposition.add(disposition7);
            Disposition disposition8 = new Disposition(8, "温柔");
            allDisposition.add(disposition8);
            Disposition disposition9 = new Disposition(9, "闷骚");
            allDisposition.add(disposition9);
            Disposition disposition10 = new Disposition(10, "自我");
            allDisposition.add(disposition10);
            Disposition disposition11 = new Disposition(11, "幽默");
            allDisposition.add(disposition11);
            Disposition disposition12 = new Disposition(12, "正直");
            allDisposition.add(disposition12);
            Disposition disposition13 = new Disposition(13, "讲义气");
            allDisposition.add(disposition13);
            Disposition disposition14 = new Disposition(14, "孝顺");
            allDisposition.add(disposition14);
            Disposition disposition15 = new Disposition(15, "勇敢");
            allDisposition.add(disposition15);
            Disposition disposition16 = new Disposition(16, "有责任心");
            allDisposition.add(disposition16);
            Disposition disposition17 = new Disposition(17, "好动");
            allDisposition.add(disposition17);
            Disposition disposition18 = new Disposition(18, "随和");
            allDisposition.add(disposition18);
            DataSupport.saveAll(allDisposition);
        }
        return allDisposition;
    }

    /**
     * 根据年月日计算年龄
     *
     * @param value
     */
    public static String calculateAge(String value) {
        String[] str = value.split("-");
        String ageStr = getAge(Integer.parseInt(str[0]));
        return ageStr;
    }

    /**
     * 根据年月日计算星座
     *
     * @param value
     */
    public static String calculateConstelation(String value) {
        String[] str = value.split("-");
        String constellation1 = getConstellation(Integer.parseInt(str[1]), Integer.parseInt(str[2]));
        return constellation1;
    }

    /**
     * 获取可选择的年龄
     */
    public static String[] getChoiceAge() {
        String[] ages = new String[45];
        for (int i = 0; i < ages.length; i++) {
            ages[i] = 18 + i + "";
        }
        return ages;
    }

    public static String[] getAgesValueByLeft(String leftCurrent) {
        int newLeftValue = Integer.parseInt(leftCurrent);
        String[] values = new String[46];
        if (newLeftValue == 45) {
            values[0] = 65 + "";
        } else {
            for (int i = 0; i < values.length; i++) {
                if (newLeftValue > 65) {
                    return values;
                } else {
                    newLeftValue++;
                    values[i] = newLeftValue + "";
                }
            }
        }
        return values;
    }


    /**
     * 获取可选择的年龄
     */
    public static String[] getChoiceHeight() {
        String[] data = new String[49];
        for (int i = 0; i < data.length; i++) {
            data[i] = 150 + i + "";
        }
        return data;
    }

    /**
     * 是否要小孩
     *
     * @return
     */
    public static String[] getHasChildren() {
        String[] childrens = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_has_children);
        return childrens;
    }

    /**
     * 与父母同住
     *
     * @return
     */
    public static String[] getWithParent() {
        String[] childrens = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_with_parent);
        return childrens;
    }

    /**
     * 亲密行为
     *
     * @return
     */
    public static String[] getQinMi() {
        String[] qinmi = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_qinmi);
        return qinmi;
    }

    /**
     * 喜欢的异性
     *
     * @return
     */
    public static String[] getLoveOther() {
        String[] loveOther = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_love_other);
        return loveOther;
    }

    /**
     * 魅力部位
     *
     * @return
     */
    public static String[] getGoodPlace() {
        String[] goodPlace = CommonUtil.getApplication().getResources().
                getStringArray(R.array.select_user_good_place);
        return goodPlace;
    }
}
