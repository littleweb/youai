package com.Aiputt.taohuayun.utils;

import android.content.Context;
import android.text.TextUtils;

import com.Aiputt.taohuayun.domain.Area;
import com.Aiputt.taohuayun.domain.City;
import com.Aiputt.taohuayun.domain.Province;
import com.Aiputt.taohuayun.resources.Constants;
import com.alibaba.fastjson.JSON;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Administrator on 2015/7/24.
 */
public class ProvinceCityAreaUtils {

    /**
     * 根据省份名称和城市名获取三级城市名称
     * @param provinceName
     * @param cityName
     * @return
     */
    public static String[] getAreaArrayByProvinceCity(String provinceName, String cityName){
        List<Area> areaList = getAreaListByProvinceCity(provinceName, cityName);
        if(areaList!=null){
            String[] areas = new String[areaList.size()];
            for(int i=0;i<areaList.size();i++){
                Area area = areaList.get(i);
                areas[i] = area.getName();
            }
            return  areas;
        }
        return  null;
    }

    /**
     * 根据城市名称获取区域列表
     *
     * @param provinceName
     * @param cityName
     * @return
     */
    public static List<Area> getAreaListByProvinceCity(String provinceName, String cityName) {
        List<City> cityList = getCityListByProvince(provinceName);
        for (City city : cityList) {
            if (city.getName().equals(cityName)) {
                return city.getAreaList();
            }
        }
        return null;
    }

    /**
     * 根据省份名称获取cityList列表
     * @param provinceName
     * @return
     */
    public static List<City> getCityListByProvince(String provinceName){
        if(Constants.PROVINCELIST == null){
            getProvinceList("");
        }
        List<City> cityList = null;
        for (Province province : Constants.PROVINCELIST) {
            if (province.getName().equals(provinceName)) {
                cityList = province.getCityList();
            }
        }
        return cityList;
    }

    /**
     * 根据省份名称获取cityArray列表
     *
     * @param provinceName
     * @return
     */
    public static String[] getCityArrayByProvince(String provinceName) {
        List<City> cityList = getCityListByProvince(provinceName);
        if(cityList!=null) {
            String[] cityNames = new String[cityList.size()];
            for (int i=0;i<cityList.size();i++){
                City city = cityList.get(i);
                cityNames[i] = city.getName();
            }
            return cityNames;
        }
        return null;
    }

    /**
     * 加载区域数据
     *
     * @param context
     * @return
     */
    public static String loadArea(Context context) {
        try {
            InputStreamReader isr = new InputStreamReader(context.getAssets().open("area.txt"));
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            String result = "";
            while ((line = br.readLine()) != null) {
                result += line;
            }
            isr.close();
            br.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取所有省份名称
     * @return
     */
    public static String[] getAllProvinces(){
        if(Constants.PROVINCELIST == null){
            getProvinceList("");
        }
        String[] provinceDatas = new String[Constants.PROVINCELIST.size()];
        for(int i=0;i< Constants.PROVINCELIST.size();i++){
            Province province = Constants.PROVINCELIST.get(i);
            provinceDatas[i] = province.getName();
        }
        return provinceDatas;
    }

    /**
     * 获取所有序列化的省份信息
     * @param content
     * @return
     */
    public static List<Province> getProvinceList(String content){
        if(TextUtils.isEmpty(content)){
            content = loadArea(CommonUtil.getApplication());
        }
        Constants.PROVINCELIST = JSON.parseArray(content,Province.class);
        return Constants.PROVINCELIST;
    }

}
