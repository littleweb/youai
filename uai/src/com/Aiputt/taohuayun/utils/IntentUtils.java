package com.Aiputt.taohuayun.utils;

import android.content.Context;
import android.content.Intent;

import com.Aiputt.taohuayun.chat.ChatActivity;
import com.Aiputt.taohuayun.resources.Constants;

public class IntentUtils {

	public static void enterHuanxin(Context context, String userNo,
			String userName, String uid, String otherUserFace) {
		Constants.otherUserFace = otherUserFace;
		Constants.otherUserId = Integer.parseInt(uid);
		Intent intent = new Intent(context, ChatActivity.class);
		intent.putExtra("userId", userNo);
		intent.putExtra("userName", userName);
		intent.putExtra("uid", uid);
		context.startActivity(intent);
	}
}
