package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Aiputt.taohuayun.wheel.OnWheelChangedListener;
import com.Aiputt.taohuayun.wheel.WheelView;
import com.Aiputt.taohuayun.wheel.adapter.ArrayWheelAdapter;
import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.UserInfoConfig;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class RadioDialog implements OnWheelChangedListener {


    private Context mContext;
    private Dialog mDialog;
    private Button btnOK;
    private WheelView lvDetail;
    private String[] mData;
    private int currentPosition;
    private TextView left_title;
    private TextView tvTitle;

    private int type;
    private int selectedItem;
    private int lastType;

    public RadioDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_radio, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        lvDetail = (WheelView) view.findViewById(R.id.lvDetail);
        btnOK = (Button) view.findViewById(R.id.btn_ok);
        left_title = (TextView) view.findViewById(R.id.left_title);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);

        lvDetail.setWheelBackground(R.drawable.bg_dialog_white_color);
//        lvDetail.setWheelForeground(R.drawable.bg_dialog_transparent_color);
        lvDetail.setDrawShadows(false);
        lvDetail.setVisibleItems(5);
        lvDetail.addChangingListener(this);

    }

    public void setType(int type) {
        this.type = type;
        String restoreValue = UserInfoConfig.USEINFO_CONFIG.get(type);
        if (!TextUtils.isEmpty(UserInfoConfig.getUserSelectedInfo(restoreValue))) {
            selectedItem = Integer.parseInt(UserInfoConfig.getUserSelectedInfo(restoreValue));
        }
        switch (type) {
            case UserInfoConfig.TYPE_HEIGHT:
                tvTitle.setText("身高");
                left_title.setText("cm");
                left_title.setVisibility(View.VISIBLE);
                break;
            case UserInfoConfig.TYPE_WEIGHT:
                tvTitle.setText("体重");
                left_title.setText("斤");
                left_title.setVisibility(View.VISIBLE);
                break;
            case UserInfoConfig.TYPE_BLOOD_TYPE:
                tvTitle.setText("血型");
                left_title.setText("型");
                left_title.setVisibility(View.VISIBLE);
                break;
            case UserInfoConfig.TYPE_EDUCATION:
                tvTitle.setText("学历");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_HOUSE:
                tvTitle.setText("住房情况");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_INCOME:
                tvTitle.setText("收入");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_MARRIAGE:
                tvTitle.setText("婚姻状况");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_OCCUPATION:
                tvTitle.setText("职业");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_YIDILIAN:
                tvTitle.setText("异地恋");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_GOOD_PLACE:
                tvTitle.setText("魅力部位");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_LOVE_OTHER:
                tvTitle.setText("喜欢的异性类型");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_QIN_MI:
                tvTitle.setText("接受亲密行为");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_WITH_PARENT:
                tvTitle.setText("和父母同住");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_HAS_CHILDREN:
                tvTitle.setText("是否要小孩");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_HIS_EDUCATION:
                tvTitle.setText("最低学历");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_HIS_INCOME:
                tvTitle.setText("最低收入");
                left_title.setVisibility(View.GONE);
                break;
            case UserInfoConfig.TYPE_HIS_LOCATION:
                tvTitle.setText("所在地");
                left_title.setVisibility(View.GONE);
                break;
        }

    }

    public void setData(String[] data) {
        this.mData = data;
        ArrayWheelAdapter<String> mAdapter = new ArrayWheelAdapter<String>(mContext, data);
        mAdapter.setTextSize(16);
        mAdapter.setTextColor(mContext.getResources().getColor(R.color.gray));
        lvDetail.setViewAdapter(mAdapter);
        if (selectedItem > data.length) {
            selectedItem = 0;
        }
        lvDetail.setCurrentItem(selectedItem);
    }

    public String getValue() {
        String restoreValue = UserInfoConfig.USEINFO_CONFIG.get(type);
        UserInfoConfig.saveUserSelectedInfo(restoreValue, currentPosition + "");
        return mData[currentPosition];
    }

    public void setOnClickListener(View.OnClickListener listener) {
        btnOK.setOnClickListener(listener);
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            lastType = type;
            mDialog.dismiss();
        }
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        currentPosition = newValue;
    }
}
