package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.utils.ProductUserPropertyUtils;
import com.Aiputt.taohuayun.wheel.OnWheelChangedListener;
import com.Aiputt.taohuayun.wheel.WheelView;
import com.Aiputt.taohuayun.wheel.adapter.ArrayWheelAdapter;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class IntervalSelectionDialog implements OnWheelChangedListener {

    private Context mContext;
    private Dialog mDialog;
    private Button btnOK;
    private TextView dialog_title;

    private WheelView mLeft;
    private WheelView mRight;

    private String[] mLeftDatas;
    private String[] mRightDatas;

    private int leftCurrent;
    private int rightCurrent;
    private String loadStringCach;
    private int type;

    public IntervalSelectionDialog(Context context, int type) {
        this.mContext = context;
        this.type = type;
        View view = View.inflate(context, R.layout.dialog_interval_selection, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);


        btnOK = (Button) view.findViewById(R.id.btnOK);
        mLeft = (WheelView) view.findViewById(R.id.idLeft);
        mRight = (WheelView) view.findViewById(R.id.idRight);
        dialog_title = (TextView) view.findViewById(R.id.dialog_title);

        //set current item
        if(type == UserInfoConfig.TYPE_HIS_AGE) {
            dialog_title.setText("年龄区间");
            mLeftDatas = ProductUserPropertyUtils.getAgesVale();
            mRightDatas = ProductUserPropertyUtils.getAgesVale();
            loadStringCach = UserInfoConfig.getUserSelectedInfo(UserInfoConfig.INDEX_SELECTED_HIS_AGE);
        }else if(type == UserInfoConfig.TYPE_HIS_HEIGHT){
            dialog_title.setText("身高区间");
            mLeftDatas = ProductUserPropertyUtils.getChoiceHeight();
            mRightDatas = ProductUserPropertyUtils.getChoiceHeight();
            loadStringCach = UserInfoConfig.getUserSelectedInfo(UserInfoConfig.INDEX_SELECTED_HIS_HEIGHT);
        }

        ArrayWheelAdapter<String> leftAdapter = new ArrayWheelAdapter<String>(context, mLeftDatas);
        leftAdapter.setTextSize(16);
        leftAdapter.setTextColor(mContext.getResources().getColor(R.color.cf_actionbar_left_btn_text_color));
        mLeft.setViewAdapter(leftAdapter);

        ArrayWheelAdapter<String> rightAdapter = new ArrayWheelAdapter<String>(mContext, mLeftDatas);
        rightAdapter.setTextSize(16);
        rightAdapter.setTextColor(mContext.getResources().getColor(R.color.cf_actionbar_left_btn_text_color));
        mRight.setViewAdapter(rightAdapter);
        // 添加change事件
        mLeft.addChangingListener(this);
        mRight.addChangingListener(this);
        //wheelbg
        mLeft.setWheelBackground(R.drawable.bg_dialog_white_color);
        mRight.setWheelBackground(R.drawable.bg_dialog_white_color);
        //shadows
        mLeft.setDrawShadows(false);
        mRight.setDrawShadows(false);
        mLeft.setVisibleItems(5);
        mRight.setVisibleItems(5);

        if (!TextUtils.isEmpty(loadStringCach)) {
            String[] split = loadStringCach.split(",");
            leftCurrent = Integer.parseInt(split[0]);
            mLeft.setCurrentItem(leftCurrent);
            rightCurrent = Integer.parseInt(split[1]);
            mRight.setCurrentItem(rightCurrent);
        } else {
            updateRight();
        }
    }

    private void updateRight() {
        mRight.setCurrentItem(0);
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {

    }

    public void setButtonClickListener(View.OnClickListener listener) {
        btnOK.setOnClickListener(listener);
    }

    public String getValue() {
        String value = "";
        if (type == UserInfoConfig.TYPE_HIS_AGE) {
            UserInfoConfig.saveUserSelectedInfo(UserInfoConfig.INDEX_SELECTED_HIS_AGE, mLeft.getCurrentItem() + "," + mRight.getCurrentItem());
        }else if(type == UserInfoConfig.TYPE_HIS_HEIGHT){
            UserInfoConfig.saveUserSelectedInfo(UserInfoConfig.INDEX_SELECTED_HIS_HEIGHT, mLeft.getCurrentItem() + "," + mRight.getCurrentItem());
        }
        value = mLeftDatas[mLeft.getCurrentItem()] + "," + mRightDatas[mRight.getCurrentItem()];
        return value;
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

}
