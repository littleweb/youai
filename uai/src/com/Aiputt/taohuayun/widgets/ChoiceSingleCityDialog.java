package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.Aiputt.taohuayun.wheel.OnWheelChangedListener;
import com.Aiputt.taohuayun.wheel.WheelView;
import com.Aiputt.taohuayun.wheel.adapter.ArrayWheelAdapter;
import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.domain.SearchConfig;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.utils.ProvinceCityAreaUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class ChoiceSingleCityDialog implements OnWheelChangedListener {

	private Context mContext;
	private Dialog mDialog;
	private Button btnOK;
	/**
	 * 省的WheelView控件
	 */
	private WheelView mProvince;
	/**
	 * 市的WheelView控件
	 */
	private WheelView mCity;
	/**
	 * 区的WheelView控件
	 */
	private WheelView mArea;

	/**
	 * 所有省
	 */
	private String[] mProvinceDatas;
	/**
	 * key - 省 value - 市s
	 */
	private Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 市 values - 区s
	 */
	private Map<String, String[]> mAreaDatasMap = new HashMap<String, String[]>();

	/**
	 * 当前省的名称
	 */
	private String mCurrentProviceName;
	/**
	 * 当前市的名称
	 */
	private String mCurrentCityName;
	/**
	 * 当前区的名称
	 */
	private String mCurrentAreaName = "";

	private int provinceCurrent;
	private int cityCurrent;
	private int areaCurrent;
	private String loadStringCach;

	public ChoiceSingleCityDialog(Context context) {
		this.mContext = context;
		View view = View.inflate(context, R.layout.dialog_cities, null);
		mDialog = new Dialog(context, R.style.dialog);
		mDialog.setContentView(view);
		mDialog.setCancelable(true);

		// ProvinceCityAreaUtils.loadArea(context);
		mProvinceDatas = ProvinceCityAreaUtils.getAllProvinces();

		btnOK = (Button) view.findViewById(R.id.btnOK);
		mProvince = (WheelView) view.findViewById(R.id.id_province);
		mCity = (WheelView) view.findViewById(R.id.id_city);
		mArea = (WheelView) view.findViewById(R.id.id_area);

		ArrayWheelAdapter<String> provinceAdapter = new ArrayWheelAdapter<String>(
				context, mProvinceDatas);
		provinceAdapter.setTextSize(16);
		provinceAdapter.setTextColor(mContext.getResources().getColor(
				R.color.cf_actionbar_left_btn_text_color));
		mProvince.setViewAdapter(provinceAdapter);
		// 添加change事件
		mProvince.addChangingListener(this);
		mCity.addChangingListener(this);
		mArea.addChangingListener(this);
		// wheelbg
		mProvince.setWheelBackground(R.drawable.bg_dialog_white_color);
		mCity.setWheelBackground(R.drawable.bg_dialog_white_color);
		mArea.setWheelBackground(R.drawable.bg_dialog_white_color);
		// shadows
		mProvince.setDrawShadows(false);
		mCity.setDrawShadows(false);
		mArea.setDrawShadows(false);
		mProvince.setVisibleItems(5);
		mCity.setVisibleItems(5);
		mArea.setVisibleItems(5);
		mArea.setVisibility(View.GONE);

		// set current item
		loadStringCach = UserInfoConfig
				.getUserSelectedInfo(SearchConfig.SEARCH_CITY);
		if (!TextUtils.isEmpty(loadStringCach)) {
			String[] split = loadStringCach.split(",");
			provinceCurrent = Integer.parseInt(split[0]);
			mProvince.setCurrentItem(provinceCurrent);
			cityCurrent = Integer.parseInt(split[1]);
			updateCities();
			updateAreas();
			mCity.setCurrentItem(cityCurrent);
		} else {
			updateCities();
			updateAreas();
		}
	}

	

	/**
	 * 根据当前的省，更新市WheelView的信息
	 */
	private void updateCities() {
		provinceCurrent = mProvince.getCurrentItem();
		mCurrentProviceName = mProvinceDatas[provinceCurrent];
		String[] cities = ProvinceCityAreaUtils
				.getCityArrayByProvince(mCurrentProviceName);
		if (cities == null) {
			cities = new String[] { "" };
		}
		ArrayWheelAdapter<String> cityAdapter = new ArrayWheelAdapter<String>(
				mContext, cities);
		cityAdapter.setTextSize(16);
		cityAdapter.setTextColor(mContext.getResources().getColor(
				R.color.cf_actionbar_left_btn_text_color));
		mCity.setViewAdapter(cityAdapter);
		mCity.setCurrentItem(0);
	}

	
	 /**
     * 根据当前的市，更新区WheelView的信息
     */
    private void updateAreas() {
        cityCurrent = mCity.getCurrentItem();
        mCurrentCityName = ProvinceCityAreaUtils.getCityListByProvince(mCurrentProviceName).get(cityCurrent).getName();
        String[] areas = ProvinceCityAreaUtils.getAreaArrayByProvinceCity(mCurrentProviceName, mCurrentCityName);

        if (areas == null) {
            areas = new String[]{""};
        }
        ArrayWheelAdapter<String> areaAdapter = new ArrayWheelAdapter<String>(mContext, areas);
        areaAdapter.setTextSize(16);
        areaAdapter.setTextColor(mContext.getResources().getColor(R.color.cf_actionbar_left_btn_text_color));
        mArea.setViewAdapter(areaAdapter);
        mArea.setCurrentItem(0);

        areaCurrent = mArea.getCurrentItem();
        mCurrentAreaName = areas[areaCurrent];

    }

	/**
	 * change事件的处理
	 */
	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		if (wheel == mProvince) {
			updateCities();
			updateAreas();
		} else if (wheel == mCity) {
			updateAreas();
		} else if (wheel == mArea) {
			areaCurrent = newValue;
			mCurrentAreaName = ProvinceCityAreaUtils
					.getAreaListByProvinceCity(mCurrentProviceName,
							mCurrentCityName).get(newValue).getName();
		}
	}

	public void setButtonClickListener(View.OnClickListener listener) {
		btnOK.setOnClickListener(listener);
	}

	public String getValue() {
		String value;
		SharePCach.saveStringCach(SearchConfig.SEARCH_CITY, provinceCurrent
				+ "," + cityCurrent);
		value = mCurrentProviceName + "-"+ mCurrentCityName;
		return value;
	}

	public void show() {
		if (mDialog != null) {
			mDialog.show();
		}
	}

	public void dismiss() {
		if (mDialog != null) {
			mDialog.dismiss();
		}
	}

}
