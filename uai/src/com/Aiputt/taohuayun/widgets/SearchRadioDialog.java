package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.SearchConfig;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.wheel.OnWheelChangedListener;
import com.Aiputt.taohuayun.wheel.WheelView;
import com.Aiputt.taohuayun.wheel.adapter.ArrayWheelAdapter;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class SearchRadioDialog implements OnWheelChangedListener {


    private Context mContext;
    private Dialog mDialog;
    private Button btnOK;
    private WheelView lvDetail;
    private String[] mData;
    private int currentPosition;
    private TextView left_title;

    private int type;
    private int selectedItem;
    private int lastType;

    public SearchRadioDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_radio, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        lvDetail = (WheelView) view.findViewById(R.id.lvDetail);
        btnOK = (Button) view.findViewById(R.id.btn_ok);
        left_title = (TextView) view.findViewById(R.id.left_title);

        lvDetail.setWheelBackground(R.drawable.bg_dialog_white_color);
//        lvDetail.setWheelForeground(R.drawable.bg_dialog_transparent_color);
        lvDetail.setDrawShadows(false);
        lvDetail.setVisibleItems(5);
        lvDetail.addChangingListener(this);

    }

    public void setType(int type){
        this.type = type;
        String restoreValue = SearchConfig.USEINFO_CONFIG.get(type);
        if(!TextUtils.isEmpty(UserInfoConfig.getUserSelectedInfo(restoreValue))) {
            selectedItem = Integer.parseInt(UserInfoConfig.getUserSelectedInfo(restoreValue));
        }
        if(type == UserInfoConfig.TYPE_HEIGHT){
            left_title.setText("cm");
            left_title.setVisibility(View.VISIBLE);
        }else if(type == UserInfoConfig.TYPE_WEIGHT){
            left_title.setText("斤");
            left_title.setVisibility(View.VISIBLE);
        }else if(type == UserInfoConfig.TYPE_BLOOD_TYPE){
            left_title.setText("型");
            left_title.setVisibility(View.VISIBLE);
        }else if(type == UserInfoConfig.TYPE_EDUCATION ||type == UserInfoConfig.TYPE_HOUSE
                ||type == UserInfoConfig.TYPE_INCOME||type == UserInfoConfig.TYPE_MARRIAGE
                ||type == UserInfoConfig.TYPE_OCCUPATION||type == UserInfoConfig.TYPE_YIDILIAN){
            left_title.setVisibility(View.GONE);
        }
    }

    public void setData(String[] data){
        this.mData = data;
        ArrayWheelAdapter<String> mAdapter = new ArrayWheelAdapter<String>(mContext, data);
        mAdapter.setTextSize(16);
        mAdapter.setTextColor(mContext.getResources().getColor(R.color.gray));
        lvDetail.setViewAdapter(mAdapter);
        if(selectedItem>data.length){
            selectedItem = 0;
        }
        lvDetail.setCurrentItem(selectedItem);
    }

    public String getValue(){
        String restoreValue = SearchConfig.USEINFO_CONFIG.get(type);
        UserInfoConfig.saveUserSelectedInfo(restoreValue,currentPosition+"");
        
        return mData[currentPosition];
    }

    public void setOnClickListener(View.OnClickListener listener){
        btnOK.setOnClickListener(listener);
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            lastType = type;
            mDialog.dismiss();
        }
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        currentPosition = newValue;
    }
}
