package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.Aiputt.taohuayun.R;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class PhotoUpDialog {

    private Context mContext;
    private Dialog mDialog;
    private LinearLayout takePhoto;
    private LinearLayout takeGallary;

    public PhotoUpDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_photoup, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        takePhoto = (LinearLayout) view.findViewById(R.id.takePhoto);
        takeGallary = (LinearLayout) view.findViewById(R.id.takeGallary);

    }

    public void setTakePhotoListener(View.OnClickListener listener){
        takePhoto.setOnClickListener(listener);
    }
    public void setTakeGallaryListener(View.OnClickListener listener){
        takeGallary.setOnClickListener(listener);
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

}
