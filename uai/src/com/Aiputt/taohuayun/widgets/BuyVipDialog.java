package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.Aiputt.taohuayun.R;

/**
 * Created by zhonglq on 2015/8/29.
 */
public class BuyVipDialog {

    private Context mContext;
    private Dialog mDialog;
    private Button btnCancel;
    private Button btnOK;

    public BuyVipDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_buy_vip, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        btnOK = (Button) view.findViewById(R.id.btnOK);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

    }

    public void setOnClickListener(View.OnClickListener listener) {
        btnOK.setOnClickListener(listener);
    }


    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

}
