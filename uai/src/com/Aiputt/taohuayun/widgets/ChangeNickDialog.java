package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class ChangeNickDialog {

    private Context mContext;
    private Dialog mDialog;
    private TextView tvSize;
    private EditText etValue;
    private Button btnOK;

    public ChangeNickDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_modify_nickname, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        tvSize = (TextView) view.findViewById(R.id.tvSize);
        etValue = (EditText) view.findViewById(R.id.etValue);
        btnOK = (Button) view.findViewById(R.id.btnOK);

    }

    public void setOnClickListener(View.OnClickListener listener){
        btnOK.setOnClickListener(listener);
    }

    public String getValue(){
        return etValue.getText().toString().trim();
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

}
