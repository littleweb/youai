package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.Disposition;

import java.util.List;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class MultiplyDialog {

    private Context mContext;
    private String title;
    private Dialog mDialog;
    private GridView gridView;
    public Button btnSure;
    private TextView mTitle;

    public MultiplyDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_multiply_choice, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        mTitle = (TextView) view.findViewById(R.id.dialog_title);
        gridView = (GridView) view.findViewById(R.id.gvItem);
        btnSure = (Button) view.findViewById(R.id.btnSure);

    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setGridViewAdapter(BaseAdapter baseAdapter){
        gridView.setAdapter(baseAdapter);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        gridView.setOnItemClickListener(listener);
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    interface OnSureClickListener{

    }

    private class MultiplyAdapter extends BaseAdapter{

        private List<Disposition> dispositionList;
        private Context mContext;

        private MultiplyAdapter(Context mContext, List<Disposition> dispositionList) {
            this.mContext = mContext;
            this.dispositionList = dispositionList;
        }

        @Override
        public int getCount() {
            return dispositionList.size();
        }

        @Override
        public Object getItem(int position) {
            return dispositionList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView == null){
                convertView = View.inflate(mContext,R.layout.dialog_multiply_choice_item,null);
                holder = new ViewHolder();
                holder.tvLabel = (TextView) convertView.findViewById(R.id.label);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }
            Disposition disposition = dispositionList.get(position);
            holder.tvLabel.setText(disposition.getDescription());
            return convertView;
        }
    }
    static class ViewHolder{
        public TextView tvLabel;
    }
}
