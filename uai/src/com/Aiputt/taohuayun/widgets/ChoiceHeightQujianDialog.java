package com.Aiputt.taohuayun.widgets;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.Aiputt.taohuayun.wheel.OnWheelChangedListener;
import com.Aiputt.taohuayun.wheel.WheelView;
import com.Aiputt.taohuayun.wheel.adapter.ArrayWheelAdapter;
import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.domain.SearchConfig;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.utils.ProductUserPropertyUtils;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class ChoiceHeightQujianDialog implements OnWheelChangedListener {

    private Context mContext;
    private Dialog mDialog;
    private Button btnOK;
    private WheelView mYear;
    private WheelView mMonth;

    private String[] mYearDatas;
    private String[] mMonthDatas;

    private String mYearCurrentName;
    private String mMonthCurrentName;
    private String mDayCurrentName = "";

    private int yearCurrent;
    private int monthCurrent;
    private String loadStringCach;

    public ChoiceHeightQujianDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_age, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        mYearDatas = ProductUserPropertyUtils.getHeightValue();
        mMonthDatas = ProductUserPropertyUtils.getHeightValue();

        btnOK = (Button) view.findViewById(R.id.btnOK);
        mYear = (WheelView) view.findViewById(R.id.id_year);
        mMonth = (WheelView) view.findViewById(R.id.id_month);

        ArrayWheelAdapter<String> provinceAdapter = new ArrayWheelAdapter<String>(context, mYearDatas);
        provinceAdapter.setTextSize(16);
        provinceAdapter.setTextColor(mContext.getResources().getColor(R.color.cf_actionbar_left_btn_text_color));
        mYear.setViewAdapter(provinceAdapter);
        // 添加change事件
        mYear.addChangingListener(this);
        mMonth.addChangingListener(this);
        //wheelbg
        mYear.setWheelBackground(R.drawable.bg_dialog_white_color);
        mMonth.setWheelBackground(R.drawable.bg_dialog_white_color);
        //shadows
        mYear.setDrawShadows(false);
        mMonth.setDrawShadows(false);
//        mYear.setVisibleItems(5);
//        mMonth.setVisibleItems(5);
        //set current item
        loadStringCach = UserInfoConfig.getUserSelectedInfo(SearchConfig.SEARCH_HEIGHT);
        if (!TextUtils.isEmpty(loadStringCach)) {
            String[] split = loadStringCach.split("-");
            yearCurrent = Integer.parseInt(split[0]);
            mYear.setCurrentItem(yearCurrent);
            monthCurrent = Integer.parseInt(split[1]);
            updateMonth();
            mMonth.setCurrentItem(monthCurrent);
        } else {
        	yearCurrent = 150;
        	monthCurrent = 150;
            updateMonth();
        }
    }


    /**
     * 更新月
     */
    private void updateMonth() {
        ArrayWheelAdapter<String> monthAdapter = new ArrayWheelAdapter<String>(mContext, mMonthDatas);
        monthAdapter.setTextSize(16);
        monthAdapter.setTextColor(mContext.getResources().getColor(R.color.cf_actionbar_left_btn_text_color));
        mMonth.setViewAdapter(monthAdapter);
        mMonth.setCurrentItem(0);
    }

    /**
     * change事件的处理
     */
    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        if (wheel == mYear) {
            yearCurrent = newValue;
            mYearCurrentName = mYearDatas[newValue];
            updateMonth();
        } else if (wheel == mMonth) {
            monthCurrent = newValue;
            mMonthCurrentName = mMonthDatas[newValue];
        }
    }

    public void setButtonClickListener(View.OnClickListener listener) {
        btnOK.setOnClickListener(listener);
    }

    public String getValue() {
        SharePCach.saveStringCach(SearchConfig.SEARCH_HEIGHT,yearCurrent + "-" + monthCurrent);
        return mYearCurrentName + "-" + mMonthCurrentName ;
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

}
