package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.LogoutDialog;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;

/**
 * Created by zhonglq on 2015/7/18.
 */
public class SettingActivity extends BaseActivity {

    private RelativeLayout rlSafe;
    private RelativeLayout rlHelp;
    private RelativeLayout rlCheckVersion;
    private TextView tvLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle("设置");
        rlSafe = (RelativeLayout) this.findViewById(R.id.rlSafe);
        rlHelp = (RelativeLayout) this.findViewById(R.id.rlHelp);
        rlCheckVersion = (RelativeLayout) this.findViewById(R.id.rlCheckVersion);
        tvLogout = (TextView) this.findViewById(R.id.tvLogout);
        ivLeft.setVisibility(View.VISIBLE);

        rlSafe.setOnClickListener(this);
        rlHelp.setOnClickListener(this);
        rlCheckVersion.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Intent intent = null;
        switch (v.getId()) {
            case R.id.rlSafe:
                intent = WebViewActivity.createIntent(this, "安全中心", "http://www.aiputt.com/app/android/v1/safe.html");
                startActivity(intent);
                break;
            case R.id.rlHelp:
                intent = WebViewActivity.createIntent(this, "帮助答疑", "http://www.aiputt.com/app/android/v1/help.html");
                startActivity(intent);
                break;
            case R.id.rlCheckVersion:
                checkVersion();
                break;
            case R.id.tvLogout:
                final LogoutDialog dialog = new LogoutDialog(this);
                dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserController.getInstance().logout();
                        dialog.dismiss();
                        Intent intent = new Intent(SettingActivity.this,LoginActivity.class);
                        intent.putExtra("fromSet", true);
                        startActivity(intent);
                        SettingActivity.this.finish();
                    }
                });
                dialog.show();
                break;
        }
    }

    /**
     * 检测更新
     */
    private void checkVersion() {
        UmengUpdateAgent.setUpdateAutoPopup(false);
        UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
            @Override
            public void onUpdateReturned(int updateStatus, UpdateResponse updateInfo) {
                switch (updateStatus) {
                    case UpdateStatus.Yes: // has update
                        UmengUpdateAgent.showUpdateDialog(mContext, updateInfo);
                        break;
                    case UpdateStatus.No: // has no update
                        PromptManager.showCenterToast("当前版本已是最新版");
                        break;
                    case UpdateStatus.Timeout: // time out
                        PromptManager.showCenterToast("联网超时,请稍后再试");
                        break;
                }
            }
        });
        UmengUpdateAgent.update(this);
    }

    public static Intent createIntent(Context context) {
        return new Intent(context, SettingActivity.class);
    }


}
