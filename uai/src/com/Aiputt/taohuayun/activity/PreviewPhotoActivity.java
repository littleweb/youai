
package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.controller.NearByController;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.DeletePhotoEvent;
import com.Aiputt.taohuayun.event.ModifyHeadPhotoEvent;
import com.Aiputt.taohuayun.event.NotifyUserPhotoChangeEvent;
import com.Aiputt.taohuayun.event.SayHiEventEvent;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/7/27.
 */
public class PreviewPhotoActivity extends BaseActivity {

    public static final String IMAGELIST = "imageList";
    public static final String CURRENT_POSITION = "currentPosition";
    public static final String ISHEADPHOTO = "isHeadPhoto";
    public static final String ISMYSELF = "isMySelf";
    public static final String USERINFO = "user";

    private ViewPager viewPager;
    private ImageView leftButton;
    private TextView ivSayHello;
    private TextView tvTitle;
    private List<View> viewList = new ArrayList<View>();
    private List<ImageList> imageLists = new ArrayList<ImageList>();
    private int currentPosition;
    private boolean isHeadPhoto;

    private NearByController mNearByController = NearByController.getInstance();
    private PreviewPhotoHandler mHandler = new PreviewPhotoHandler();
    private PhotoViewAdapter mAdapter;
    private User user;
    private boolean isMyself;
    private RelativeLayout rlMineOperate;

    private TextView tvSetHeadPhoto;
    private TextView tvDeletePhoto;
    private TextView tvColse;

    private ImageList removeImageList = null;
    private View removeView = null;

    private UserController mUserController = UserController.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideTitleBar();
        setContentView(R.layout.activity_view_photo);
        mHandler.register();
        viewPager = (ViewPager) this.findViewById(R.id.viewPage);
        leftButton = (ImageView) this.findViewById(R.id.leftButton);
        tvTitle = (TextView) this.findViewById(R.id.title_name);
        ivSayHello = (TextView) this.findViewById(R.id.ivSayHello);
        rlMineOperate = (RelativeLayout) this.findViewById(R.id.rlMineOperate);
        tvSetHeadPhoto = (TextView) this.findViewById(R.id.tvSetHeadPhoto);
        tvDeletePhoto = (TextView) this.findViewById(R.id.tvDeletePhoto);
        tvColse = (TextView) this.findViewById(R.id.tvColse);

        isHeadPhoto = getIntent().getBooleanExtra(ISHEADPHOTO, true);
        isMyself = getIntent().getBooleanExtra(ISMYSELF, true);
        user = (User) getIntent().getSerializableExtra(USERINFO);
        currentPosition = getIntent().getIntExtra(CURRENT_POSITION, 0);

        if (isHeadPhoto) {
            tvTitle.setText("1/1");
            ImageList imageList = new ImageList();
            imageList.setImg_url_big(user.getUser_face_big());
            imageLists.add(imageList);
            rlMineOperate.setVisibility(View.GONE);
        } else {
            imageLists = user.getImg_list();
            tvTitle.setText(currentPosition + 1 + "/" + imageLists.size());
        }

        tvSetHeadPhoto.setOnClickListener(this);
        tvDeletePhoto.setOnClickListener(this);
        tvColse.setOnClickListener(this);

        if (isMyself) {
            ivSayHello.setVisibility(View.GONE);
            if (isHeadPhoto) {
                rlMineOperate.setVisibility(View.GONE);
            } else {
                rlMineOperate.setVisibility(View.VISIBLE);
            }
        } else {
            ivSayHello.setVisibility(View.VISIBLE);
            rlMineOperate.setVisibility(View.GONE);
        }

        if (user.getIsSayHello() == 1) {
            ivSayHello.setBackgroundResource(R.drawable.n_ck);
            ivSayHello.setEnabled(false);
        }

        initViewList();

        mAdapter = new PhotoViewAdapter(this, viewList, imageLists, currentPosition);
        viewPager.setAdapter(mAdapter);

        leftButton.setOnClickListener(this);

        ivSayHello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog("正在打招呼");
                mNearByController.sayHi(user);
            }
        });
        viewPager.setCurrentItem(currentPosition);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                currentPosition = i;
                tvTitle.setText(currentPosition + 1 + "/" + imageLists.size());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private int currentItem = 0;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.leftButton:
                finish();
                break;
            case R.id.ivSayHello:

                break;
            case R.id.tvSetHeadPhoto:
                showProgressDialog("设置中");
                currentItem = viewPager.getCurrentItem();
                ImageList imageList = imageLists.get(currentItem);
                mUserController.setHeadPhoto(imageList);
                break;
            case R.id.tvDeletePhoto:
                showProgressDialog("删除中");
                currentItem = viewPager.getCurrentItem();
                removeView = viewList.get(currentItem);
                removeImageList = imageLists.get(currentItem);
                mUserController.deletePhoto(removeImageList, removeView);
                break;
            case R.id.tvColse:
                finish();
                break;
        }
        super.onClick(v);
    }

    /**
     * 初始化adapter的view
     */
    private void initViewList() {
        View v;
        if (isHeadPhoto) {
            v = View.inflate(this, R.layout.photo_view_item, null);
            viewList.add(v);
        } else {
            for (int i = 0; i < imageLists.size(); i++) {
                v = View.inflate(this, R.layout.photo_view_item, null);
                viewList.add(v);
            }
        }
    }

    private class PhotoViewAdapter extends PagerAdapter {

        private List<ImageList> mImageList;
        private ImageLoader mImageLoader;
        private List<View> viewList;

        private PhotoViewAdapter(Context mContext, List<View> viewList, List<ImageList> mImageList, int currentPosition) {
            this.mImageList = mImageList;
            this.viewList = viewList;
            mImageLoader = ImageLoader.getInstance();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = viewList.get(position);
            ImageView ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            mImageLoader.displayImage(mImageList.get(position).getImg_url_big(), ivPhoto, ImageLoadUtils.getNormalOptions());
            container.removeView(viewList.get(position));
            container.addView(viewList.get(position));
            return viewList.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (position == viewList.size()) {
                container.removeView(viewList.get(position - 1));
            } else {
                container.removeView(viewList.get(position));
            }
        }

        @Override
        public int getCount() {
            return viewList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        public void replace(List<ImageList> imageLists, List<View> viewList) {
            this.mImageList = imageLists;
            this.viewList = viewList;
        }
    }

    private class PreviewPhotoHandler extends EventHandler {
        public void onEvent(SayHiEventEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                PromptManager.showCenterToast("已成功打招呼,请等待对方回信");
                user.setIsSayHello(1);
                ivSayHello.setBackgroundResource(R.drawable.n_ck);
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

        public void onEvent(DeletePhotoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                boolean b = imageLists.remove(removeImageList);
                boolean b1 = viewList.remove(removeView);
                PromptManager.showCenterToast("删除成功");
                notifyUserPhotoChanged(event.imageList);
                if (imageLists.isEmpty()) {
                    finish();
                } else {
                    tvTitle.setText(1 + "/" + imageLists.size());
                    mAdapter.replace(imageLists, viewList);
                    mAdapter.notifyDataSetChanged();
                    viewPager.setCurrentItem(0);
                }
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

        public void onEvent(ModifyHeadPhotoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                PromptManager.showCenterToast("设置头像成功");
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

    }


    private void notifyUserPhotoChanged(ImageList imageList) {
        NotifyUserPhotoChangeEvent event = new NotifyUserPhotoChangeEvent();
        event.imageList = imageList;
        EventCenter.getInstance().send(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    public static Intent createIntent(Context context, int currentPosition, boolean isHeadPhoto, User user, boolean isMySelf) {
        Intent intent = new Intent(context, PreviewPhotoActivity.class);
        intent.putExtra(CURRENT_POSITION, currentPosition);
        intent.putExtra(ISHEADPHOTO, isHeadPhoto);
        intent.putExtra(ISMYSELF, isMySelf);
        intent.putExtra(USERINFO, user);
        return intent;
    }

}
