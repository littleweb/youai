
package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.controller.MailController;
import com.Aiputt.taohuayun.event.NotifyEvent;
import com.Aiputt.taohuayun.event.RengongEvent;
import com.Aiputt.taohuayun.event.SendSugEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.PromptManager;

/**
 * Created by zhonglq on 2015/8/3.
 */
public class NoticeWebViewActivity extends BaseActivity {

    private static final int PAYMENT_TYPE_ALIPAY = 1;
    private static final int PAYEMNT_TYPE_YINLIAN_YUYIN = 2;
    private static final int PAYMENT_TYPE_CHONGZHIKA = 3;

    private static final String TAG = "WebViewActivity";
    private static final String URL = "url";
    private static final String TITLE = "title";
    private static final String NOTICE = "notice";

    private ProgressBar progressBar;
    private WebView webView;
    private String mTitle;
    private String mUrl;
    private NotifyHandler mHandler = new NotifyHandler();
	private MailController mController = MailController.getInstance();
    private boolean isSystemNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mHandler.register();
        mTitle = getIntent().getStringExtra(TITLE);
        mUrl = getIntent().getStringExtra(URL);
        isSystemNotice = getIntent().getBooleanExtra(NOTICE, false);
        ivLeft.setVisibility(View.VISIBLE);
        setTitle(mTitle);
        progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        webView = (WebView) this.findViewById(R.id.webView);

        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JsApi(), "taohuayun");
        webView.getSettings().setDefaultTextEncodingName("utf-8");

        webView.setWebViewClient(webViewClient);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
            }

        });

        if (isSystemNotice) {
        	showProgressDialog("加载中");
			mController.getNotifyList();
		}else {
			showProgressDialog("加载中");
			mController.getRengongList();
		}
        

    }

    private void onWebPageStarted() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    private String mDate;
    private class JsApi {
        @JavascriptInterface
        public String getNoticeData() {
            return mDate;
        }
        
        @JavascriptInterface
        public void sendSuggestion(String content) {
            mController.sendSuggest(content);
        }
        
        @JavascriptInterface
        public void completeInfo() {
            Intent intent = new Intent(NoticeWebViewActivity.this,PerfectInformationActivity.class);
            NoticeWebViewActivity.this.startActivity(intent);
        }

    }

    

    private class NotifyHandler extends EventHandler {

		public void onEvent(NotifyEvent event) {
			dismissProgressDialog();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				if (event.data != null) {
					System.out.println("===tijiao=="+event.data);
					mDate = event.data;
					webView.loadUrl(mUrl);
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
			dismissProgressDialog();
		}
		public void onEvent(RengongEvent event) {
			dismissProgressDialog();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				if (event.data != null) {
					System.out.println("===tijiao=="+event.data);
					mDate = event.data;
					webView.loadUrl(mUrl);
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
			dismissProgressDialog();
		}
		
		public void onEvent(SendSugEvent event) {
			dismissProgressDialog();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				if (event.data != null) {
					
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
			dismissProgressDialog();
		}

	}


    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            onWebPageStarted();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            webView.loadUrl(url);
            return true;
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    public static Intent createIntent(Context context, String title, String url,boolean isSystemNotice) {
        Intent intent = new Intent(context, NoticeWebViewActivity.class);
        intent.putExtra(URL, url);
        intent.putExtra(TITLE, title);
        intent.putExtra(NOTICE, isSystemNotice);
        return intent;
    }

}
