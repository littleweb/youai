package com.Aiputt.taohuayun.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.adapter.RegistQaAdapter;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.controller.LoginRegisterController;
import com.Aiputt.taohuayun.domain.QustionOption;
import com.Aiputt.taohuayun.domain.RegisterQa;
import com.Aiputt.taohuayun.event.RegisterQaEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.utils.DateUtil;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class RegistQaActivity extends BaseActivity {

	private RoundedImageView ivImage;
	private TextView tvQaTitle;
	private TextView tvIndex;
	private ListView lvList;
	private ArrayList<RegisterQa> registerQas;
	private RegisterQa currentQa;
	private int currentPosition;
	private String oldDate;
	private String date;

	private RegisterQaHandler mHandler = new RegisterQaHandler();
	private LoginRegisterController mController = LoginRegisterController
			.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_regist_qa);
		oldDate = SharePCach.loadStringCach("recommendTime");
		date = DateUtil.getYMDTime(System.currentTimeMillis());
		
		mHandler.register();
		setTitle("回答问题");
		ivImage = (RoundedImageView) findViewById(R.id.ivImage);
		tvQaTitle = (TextView) findViewById(R.id.tvQaTitle);
		tvIndex = (TextView) findViewById(R.id.tvIndex);
		lvList = (ListView) findViewById(R.id.lvList);
		Intent intent = getIntent();
		String gender = intent.getStringExtra("gender");
		String age = intent.getStringExtra("age");
		showProgressDialog(R.string.pl_wait);
		mController.actionRegisterQa(gender, age);

		lvList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				QustionOption option = (QustionOption) parent
						.getItemAtPosition(position);
				if (option != null) {
					showProgressDialog(R.string.pl_wait);
					mController.actionAnswerQa(
							String.valueOf(currentQa.question_id), option.key,currentQa.user_info.uid,currentQa.user_info.user_no);
				}

			}
		});
	}

	@Override
	protected void onDestroy() {
		mHandler.unregister();
		super.onDestroy();
	}

	class RegisterQaHandler extends EventHandler {
		public void onEvent(RegisterQaEvent event) {
			dismissProgressDialog();
			if (event.code == 0) {
				registerQas = event.data;
				if (registerQas != null && !registerQas.isEmpty()) {
					fillViewContent(registerQas.get(0), 0);
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}

		public void onEvent(ResultEvent event) {
			dismissProgressDialog();
			if (event.code == 0) {
				if (registerQas != null
						&& registerQas.size() > currentPosition + 1) {
					fillViewContent(registerQas.get(currentPosition + 1),
							currentPosition + 1);
				} else {
					
//					if (!oldDate.equals(date)) {
//						startActivity(EveryDayRecommendActivity
//								.createIntent(getApplicationContext()));
//					} else {
						startActivity(MainActivity
								.createIntent(getApplicationContext()));
//					}
					SharePCach.saveBooleanCach("index", true);
					finish();
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}

	}

	private void fillViewContent(RegisterQa qa, int position) {
		currentQa = qa;
		currentPosition = position;
		int cu = position+1;
		tvIndex.setText(cu+"/"+registerQas.size()+"选择如下选项");
		tvQaTitle.setText(qa.question);
		ImageLoader.getInstance().displayImage(qa.user_info.user_face, ivImage);
		Map<String, String> options = qa.options;

		if (options != null) {
			List<QustionOption> qustionOptions = new ArrayList<QustionOption>();
			Set<String> keySet = options.keySet();
			for (Iterator iterator = keySet.iterator(); iterator.hasNext();) {
				String string = (String) iterator.next();
				QustionOption qustionOption = new QustionOption();
				qustionOption.key = string;
				qustionOption.content = options.get(string);
				qustionOptions.add(qustionOption);
			}
			System.out.println("---------------BBB====" + qustionOptions);
			lvList.setAdapter(new RegistQaAdapter(this, qustionOptions));
		}

	}

}
