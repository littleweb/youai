package com.Aiputt.taohuayun.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.utils.DateUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

public class BaseActivity extends FragmentActivity implements OnClickListener {

    protected boolean destroyed = false;

    protected ImageLoader mImageLoader = ImageLoader.getInstance();

    protected TextView tvLeft;
    protected ImageView ivLeft;
    protected ImageView ivRight;
    protected TextView tvRight;
    protected TextView tvTitle;

    private FragmentManager mFragmentManager = null;

    protected RelativeLayout mTitleBar = null;
    protected View vLine = null;
    private LinearLayout mContent = null;

    private boolean hasFragment;
    private String mPageName;
    protected Context mContext;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.title_bar);
        mContext = this;
        mPageName = this.getClass().getName();
        mFragmentManager = getSupportFragmentManager();

        mTitleBar = (RelativeLayout) findViewById(R.id.common_title_bar);
        mContent = (LinearLayout) findViewById(R.id.root_content);
        vLine = findViewById(R.id.vLine);
        tvTitle = (TextView) mTitleBar.findViewById(R.id.tvTitle);
        tvLeft = (TextView) mTitleBar.findViewById(R.id.tvLeft);
        tvRight = (TextView) mTitleBar.findViewById(R.id.tvRight);
        ivLeft = (ImageView) mTitleBar.findViewById(R.id.ivLeft);
        ivRight = (ImageView) mTitleBar.findViewById(R.id.ivRight);

        tvLeft.setOnClickListener(this);
        tvRight.setOnClickListener(this);
        ivLeft.setOnClickListener(this);
        ivRight.setOnClickListener(this);

    }

    /**
     * 设置左上角的button标签
     */
    protected void setLeftButtonVisible() {
        ivLeft.setVisibility(View.VISIBLE);
    }

    /**
     * 设置右上角的button标签
     */
    protected void setRightButtonLabel(String label) {
        ivRight.setVisibility(View.GONE);
        tvRight.setVisibility(View.VISIBLE);
        tvRight.setText(label);
    }

    protected void setRightButtonEnable(boolean enable) {
        tvRight.setEnabled(enable);
    }

    /**
     * 设置标题
     * *
     */
    public void setTitle(int title) {
        setTitle(getString(title));
    }

    /**
     * 设置标题
     * *
     */
    public void setTitle(String title) {
        if (mTitleBar == null || mTitleBar.getVisibility() != View.VISIBLE) {
            return;
        }

        tvTitle.setText(title);
    }

    public void setContentView(int layoutResID) {
        LayoutInflater.from(this).inflate(layoutResID, mContent);
    }

    public void setContentView(View view) {
        mContent.addView(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        mContent.addView(view, params);
    }

    public void addContentView(View view, ViewGroup.LayoutParams params) {
        mContent.addView(view, params);
    }

    private static Dialog dialog;
    private static ProgressBar pb;

    /**
     * 隐藏loading对话框
     */
    public static void dismissProgressDialog() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private String oldDate;
    private String date;

    @Override
    protected void onResume() {
        MobclickAgent.onResume(this);
        List<Fragment> fragments = mFragmentManager.getFragments();
        hasFragment = fragments != null && !fragments.isEmpty();
        if (!hasFragment) {
            MobclickAgent.onPageStart(mPageName);
        }
        oldDate = SharePCach.loadStringCach("recommendTime");
        date = DateUtil.getYMDTime(System.currentTimeMillis());
        if (SharePCach.loadBooleanCach("index") && !TextUtils.isEmpty(oldDate)) {
            if (!oldDate.equals(date)) {
                startActivity(EveryDayRecommendActivity
                        .createIntent(getApplicationContext()));
            }
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        if (!hasFragment) {
            MobclickAgent.onPageEnd(mPageName);
        }
    }

    public TextView txt;
    public ImageView delete_img;

    public void showProgressDialog(String msg) {
        showProgressDialog(msg, true);
    }

    public void showProgressDialog(int resID) {

        showProgressDialog(getString(resID), true);
    }

    /**
     * loading对话框 显示一个半透明的的loading对话框
     *
     * @param message 要显示的文字的资源
     */
    public void showProgressDialog(String message, boolean cancelable) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new Dialog(this, R.style.processDialog);
        View view = LayoutInflater.from(this).inflate(
                R.layout.loading_dialog_layout, null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.addContentView(view, params);
        pb = (ProgressBar) view.findViewById(R.id.loading_dialog_progressBar);
        delete_img = (ImageView) view.findViewById(R.id.delete_img);
        txt = (TextView) view.findViewById(R.id.loading_message);
        txt.setText(message);
        txt.setTextSize(15);
        dialog.setCancelable(cancelable);
        Window window = dialog.getWindow();
        window.setWindowAnimations(R.style.my_dialog);
        delete_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissProgressDialog();
            }
        });
        dialog.show();
    }

    /**
     * 隐藏默认的titlebar
     */
    public void hideTitleBar() {
        if (mTitleBar != null) {
            mTitleBar.setVisibility(View.GONE);
        }
        if (vLine != null) {
            vLine.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        destroyed = true;
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        destroyed = true;
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ivLeft) {
            onBackPressed();
        } else if (id == R.id.ivRight || id == R.id.tvRight) {
            onRightButtonClick();
        }
    }

    /**
     * 当右键的点击事件发生时触发该事件
     */
    protected void onRightButtonClick() {

    }
}
