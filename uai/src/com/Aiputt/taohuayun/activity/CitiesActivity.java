package com.Aiputt.taohuayun.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.Aiputt.taohuayun.wheel.OnWheelChangedListener;
import com.Aiputt.taohuayun.wheel.WheelView;
import com.Aiputt.taohuayun.wheel.adapter.ArrayWheelAdapter;
import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.utils.ProvinceCityAreaUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/7/24.
 */
public class CitiesActivity extends Activity implements OnWheelChangedListener {

    /**
     * 省的WheelView控件
     */
    private WheelView mProvince;
    /**
     * 市的WheelView控件
     */
    private WheelView mCity;
    /**
     * 区的WheelView控件
     */
    private WheelView mArea;

    /**
     * 所有省
     */
    private String[] mProvinceDatas;
    /**
     * key - 省 value - 市s
     */
    private Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
    /**
     * key - 市 values - 区s
     */
    private Map<String, String[]> mAreaDatasMap = new HashMap<String, String[]>();

    /**
     * 当前省的名称
     */
    private String mCurrentProviceName;
    /**
     * 当前市的名称
     */
    private String mCurrentCityName;
    /**
     * 当前区的名称
     */
    private String mCurrentAreaName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cities);

        ProvinceCityAreaUtils.loadArea(this);
        mProvinceDatas = ProvinceCityAreaUtils.getAllProvinces();

        mProvince = (WheelView) findViewById(R.id.id_province);
        mCity = (WheelView) findViewById(R.id.id_city);
        mArea = (WheelView) findViewById(R.id.id_area);

        mProvince.setViewAdapter(new ArrayWheelAdapter<String>(this, mProvinceDatas));
        // 添加change事件
        mProvince.addChangingListener(this);
        // 添加change事件
        mCity.addChangingListener(this);
        // 添加change事件
        mArea.addChangingListener(this);

        mProvince.setVisibleItems(5);
        mCity.setVisibleItems(5);
        mArea.setVisibleItems(5);
        updateCities();
        updateAreas();

    }

    /**
     * 根据当前的市，更新区WheelView的信息
     */
    private void updateAreas() {
        int pCurrent = mCity.getCurrentItem();
        mCurrentCityName = ProvinceCityAreaUtils.getCityListByProvince(mCurrentProviceName).get(pCurrent).getName();
        String[] areas = ProvinceCityAreaUtils.getAreaArrayByProvinceCity(mCurrentProviceName, mCurrentCityName);

        if (areas == null) {
            areas = new String[]{""};
        }
        mArea.setViewAdapter(new ArrayWheelAdapter<String>(this, areas));
        mArea.setCurrentItem(0);

        int aCurrent = mArea.getCurrentItem();
        mCurrentAreaName = areas[aCurrent];

    }

    /**
     * 根据当前的省，更新市WheelView的信息
     */
    private void updateCities() {
        int pCurrent = mProvince.getCurrentItem();
        mCurrentProviceName = mProvinceDatas[pCurrent];
        String[] cities = ProvinceCityAreaUtils.getCityArrayByProvince(mCurrentProviceName);
        if (cities == null) {
            cities = new String[]{""};
        }
        mCity.setViewAdapter(new ArrayWheelAdapter<String>(this, cities));
        mCity.setCurrentItem(0);
        updateAreas();
    }

    /**
     * change事件的处理
     */
    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        if (wheel == mProvince) {
            updateCities();
        } else if (wheel == mCity) {
            updateAreas();
        } else if (wheel == mArea) {
            mCurrentAreaName = ProvinceCityAreaUtils.getAreaListByProvinceCity(mCurrentProviceName,mCurrentCityName).get(newValue).getName();
        }
    }

    public void showChoose(View view) {
        Toast.makeText(this, mCurrentProviceName + mCurrentCityName + mCurrentAreaName, Toast.LENGTH_SHORT).show();
    }
}
