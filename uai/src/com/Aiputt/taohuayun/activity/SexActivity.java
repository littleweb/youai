package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.adapter.AgeAdapter;
import com.Aiputt.taohuayun.app.ULoveApplication;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.controller.LoginRegisterController;
import com.Aiputt.taohuayun.domain.AgeInfo;
import com.Aiputt.taohuayun.event.RegisterEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widget.SelectAgeDialog;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMGroupManager;

import java.util.ArrayList;
import java.util.List;


public class SexActivity extends BaseActivity {

	private TextView tvXieyi;
	private TextView tvMale;
	private TextView tvFemale;
	private TextView tvLogin;
	private ImageView ivMale;
	private ImageView ivFemale;
	private SelectAgeDialog mAgeDialog;
	private String selectAge;
	private String selectGender;
	private RegisterHandler mHandler = new RegisterHandler();
	private LoginRegisterController mController = LoginRegisterController
			.getInstance();

	public static Intent createIntent(Context context) {
		return new Intent(context, SexActivity.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sex);
		mHandler.register();
		tvLogin = (TextView) findViewById(R.id.tvLogin);
		tvXieyi = (TextView) findViewById(R.id.tvXieyi);
		tvMale = (TextView) findViewById(R.id.tvMale);
		tvFemale = (TextView) findViewById(R.id.tvFemale);
		ivMale = (ImageView) findViewById(R.id.ivMale);
		ivFemale = (ImageView) findViewById(R.id.ivFemale);

		tvMale.setOnClickListener(this);
		tvFemale.setOnClickListener(this);
		ivMale.setOnClickListener(this);
		ivFemale.setOnClickListener(this);
		tvLogin.setOnClickListener(this);
		tvXieyi.setOnClickListener(this);

		setTitle("登录");

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.unregister();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {
		case R.id.tvMale:
			isMale();
			break;
		case R.id.tvFemale:
			isFemale();
			break;
		case R.id.ivMale:
			isMale();
			break;
		case R.id.ivFemale:
			isFemale();
			break;
		case R.id.tvLogin:
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			break;
		case R.id.tvXieyi:
			Intent wintent = WebViewActivity.createIntent(this, "协议",
					"http://www.aiputt.com/app/android/v1/xieyi.html");
			startActivity(wintent);
			break;
		}
	}

	private void isMale() {
		mAgeDialog = new SelectAgeDialog(this);
		mAgeDialog.setGridViewAdapter(getAgeAdapter());
		mAgeDialog.show();
		mAgeDialog.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				AgeInfo ageInfo = (AgeInfo) arg0.getItemAtPosition(arg2);
				if (ageInfo != null) {
					selectAge = ageInfo.value;
				}
				mAgeDialog.dismiss();
				selectGender = "0";
				showProgressDialog(R.string.pl_wait);
				mController.actionRegister("0", selectAge);

			}
		});
	}

	private void isFemale() {
		mAgeDialog = new SelectAgeDialog(this);
		mAgeDialog.setGridViewAdapter(getAgeAdapter());
		mAgeDialog.show();
		mAgeDialog.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				AgeInfo ageInfo = (AgeInfo) arg0.getItemAtPosition(arg2);
				if (ageInfo != null) {
					selectAge = ageInfo.value;
				}
				mAgeDialog.dismiss();
				selectGender = "1";
				showProgressDialog(R.string.pl_wait);
				mController.actionRegister("1", selectAge);

			}
		});
	}

	private AgeAdapter getAgeAdapter() {
		List<AgeInfo> ageInfos = new ArrayList<AgeInfo>();
		for (int i = 18; i < 61; i++) {
			AgeInfo ageInfo = new AgeInfo();
			ageInfo.value = String.valueOf(i);
			ageInfos.add(ageInfo);
		}
		AgeAdapter adapter = new AgeAdapter(this, ageInfos);
		return adapter;
	}

	class RegisterHandler extends EventHandler {
		public void onEvent(RegisterEvent event) {
			
			if (event.code == 0) {
				event.data.save();
				SharePCach.saveStringCach(SharePCach.TOKEN,
						event.data.getToken());
				SharePCach.saveStringCach(SharePCach.USER_NO,
						event.data.getUser_no());
				SharePCach.saveStringCach(SharePCach.USER_PWD,
						event.data.getUser_pass());
				SharePCach.saveStringCach(SharePCach.HPWD,
						event.data.getEasemod_password());
				login();
			} else {
				dismissProgressDialog();
				PromptManager.showCenterToast(event.msg);
			}
		}
	}

	private void nextPage() {
		dismissProgressDialog();
		SharePCach.saveStringCach("gender", selectGender);
		SharePCach.saveStringCach("age", selectAge);
		Intent intent = new Intent(SexActivity.this, RegistQaActivity.class);
		intent.putExtra("gender", selectGender);
		intent.putExtra("age", selectAge);
		startActivity(intent);
		finish();
	}

	/**
	 * 登录
	 * 
	 */
	public void login() {

		final long start = System.currentTimeMillis();
		// 调用sdk登陆方法登陆聊天服务器
		System.out.println("--n--"+SharePCach.loadStringCach(SharePCach.USER_NO));
		System.out.println("--p--"+SharePCach.loadStringCach(SharePCach.HPWD));
		EMChatManager.getInstance().login(
				SharePCach.loadStringCach(SharePCach.USER_NO),
				SharePCach.loadStringCach(SharePCach.HPWD), new EMCallBack() {

					@Override
					public void onSuccess() {
						// 登陆成功，保存用户名密码
						Log.e("LoginActivity", "Login success");
						ULoveApplication.mApp.setUserName(SharePCach
								.loadStringCach(SharePCach.USER_NO));
						ULoveApplication.mApp.setPassword(SharePCach
								.loadStringCach(SharePCach.HPWD));

						try {
							// ** 第一次登录或者之前logout后再登录，加载所有本地群和回话
							// ** manually load all local groups and
							EMGroupManager.getInstance().loadAllGroups();
							EMChatManager.getInstance().loadAllConversations();
							// 处理好友和群组
							// initializeContacts();
						} catch (Exception e) {
							e.printStackTrace();
							// 取好友或者群聊失败，不让进入主页面
							runOnUiThread(new Runnable() {
								public void run() {
									ULoveApplication.mApp.logout(null);
									Toast.makeText(getApplicationContext(),
											R.string.login_failure_failed,
											Toast.LENGTH_SHORT).show();
								}
							});
							return;
						}
						// 更新当前用户的nickname 此方法的作用是在ios离线推送时能够显示用户nick
						boolean updatenick = EMChatManager
								.getInstance()
								.updateCurrentUserNick(
										ULoveApplication.currentUserNick.trim());
						if (!updatenick) {
							Log.e("LoginActivity",
									"update current user nick fail");
						}
						nextPage();

						finish();
					}

					@Override
					public void onProgress(int progress, String status) {
					}

					@Override
					public void onError(final int code, final String message) {
						nextPage();
						runOnUiThread(new Runnable() {
							public void run() {
								Toast.makeText(
										getApplicationContext(),
										getString(R.string.Login_failed)
												+ message, Toast.LENGTH_SHORT)
										.show();
							}
						});
					}
				});
	}

}
