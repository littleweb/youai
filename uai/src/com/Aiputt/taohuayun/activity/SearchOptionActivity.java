package com.Aiputt.taohuayun.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.domain.SearchConfig;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.ProductUserPropertyUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.ChoiceAgeQujianDialog;
import com.Aiputt.taohuayun.widgets.ChoiceHeightQujianDialog;
import com.Aiputt.taohuayun.widgets.ChoiceSingleCityDialog;
import com.Aiputt.taohuayun.widgets.RadioDialog;
import com.Aiputt.taohuayun.widgets.SearchRadioDialog;

public class SearchOptionActivity extends BaseActivity {

	private RelativeLayout rlCity;
	private RelativeLayout rlAge;
	private RelativeLayout rlHeight;
	private RelativeLayout rlXueli;
	private RelativeLayout rlShouru;

	private TextView tvCity;
	private TextView tvAge;
	private TextView tvHeight;
	private TextView tvXueli;
	private TextView tvShouru;

	private SearchRadioDialog radioDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_option);

		rlCity = (RelativeLayout) findViewById(R.id.rlCity);
		rlAge = (RelativeLayout) findViewById(R.id.rlAge);
		rlHeight = (RelativeLayout) findViewById(R.id.rlHeight);
		rlXueli = (RelativeLayout) findViewById(R.id.rlXueli);
		rlShouru = (RelativeLayout) findViewById(R.id.rlShouru);

		tvCity = (TextView) findViewById(R.id.tvCity);
		tvAge = (TextView) findViewById(R.id.tvAge);
		tvHeight = (TextView) findViewById(R.id.tvHeight);
		tvXueli = (TextView) findViewById(R.id.tvXueli);
		tvShouru = (TextView) findViewById(R.id.tvShouru);
		
		tvCity.setText(SharePCach.loadStringCach(SearchConfig.SEARCH_POVISION_SEARCH)+SharePCach.loadStringCach(SearchConfig.SEARCH_CITY_SEARCH));
		tvAge.setText(SharePCach.loadStringCach(SearchConfig.SEARCH_AGE_SEARCH));
		tvHeight.setText(SharePCach.loadStringCach(SearchConfig.SEARCH_HEIGHT_SEARCH));

		rlCity.setOnClickListener(this);
		rlAge.setOnClickListener(this);
		rlHeight.setOnClickListener(this);
		rlXueli.setOnClickListener(this);
		rlShouru.setOnClickListener(this);

		setTitle("搜索");
		tvRight.setText("保存");
		tvRight.setVisibility(View.VISIBLE);
		tvRight.setOnClickListener(this);
		radioDialog = new SearchRadioDialog(this);
		setLeftButtonVisible();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {
		case R.id.rlCity:
			choiceLocation();
			break;
		case R.id.rlAge:
			choiceAge();
			break;
		case R.id.rlHeight:
			choiceHeight();
			break;
		case R.id.rlXueli:
			choiceHeight(UserInfoConfig.TYPE_EDUCATION);
			break;
		case R.id.rlShouru:
			choiceHeight(UserInfoConfig.TYPE_INCOME);
			break;
		case R.id.tvRight:
			Constants.mNeedSearch = true; 
			finish();
			break;
		}
	}

	/**
	 * 选择居住地
	 * 
	 * @param type
	 *            1籍贯 2居住地
	 * @param isShowArea
	 *            是否显示居住地
	 */
	private void choiceLocation() {
		final ChoiceSingleCityDialog choiceCityDialog = new ChoiceSingleCityDialog(
				this);
		choiceCityDialog.setButtonClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = choiceCityDialog.getValue();
				String[] split = value.split("-");
				String povisiton = split[0];
				String city = split[1];
				SharePCach.saveStringCach(SearchConfig.SEARCH_POVISION_SEARCH, povisiton);
				SharePCach.saveStringCach(SearchConfig.SEARCH_CITY_SEARCH, city);
				tvCity.setText(value);

				choiceCityDialog.dismiss();
			}
		});
		choiceCityDialog.show();
	}

	private void choiceAge() {
		final ChoiceAgeQujianDialog choiceCityDialog = new ChoiceAgeQujianDialog(
				this);
		choiceCityDialog.setButtonClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = choiceCityDialog.getValue();
				String[] split = value.split("-");
				int first = 0;
				int second = 0;
				try {
					first = Integer.parseInt(split[0]);
					second = Integer.parseInt(split[1]);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (first >= second) {
					PromptManager.showCenterToast("后面的年龄应该比前面的大");
				} else {
					SharePCach.saveStringCach(SearchConfig.SEARCH_AGE_SEARCH, value);
					tvAge.setText(value);
				}
				choiceCityDialog.dismiss();
			}
		});
		choiceCityDialog.show();
	}

	private void choiceHeight() {
		final ChoiceHeightQujianDialog choiceCityDialog = new ChoiceHeightQujianDialog(
				this);
		choiceCityDialog.setButtonClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = choiceCityDialog.getValue();
				String[] split = value.split("-");
				int first = 0;
				int second = 0;
				try {
					first = Integer.parseInt(split[0]);
					second = Integer.parseInt(split[1]);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (first >= second) {
					PromptManager.showCenterToast("后面的身高应该比前面的大");
				} else {
					SharePCach.saveStringCach(SearchConfig.SEARCH_HEIGHT_SEARCH, value);
					tvHeight.setText(value);
				}
				choiceCityDialog.dismiss();
			}
		});
		choiceCityDialog.show();
	}

	/**
	 * 选择身高
	 * 
	 * @param type
	 *            1 身高 2 体重 3 血型
	 */
	private void choiceHeight(final int type) {
		String[] data = null;
		radioDialog.setType(type);
		if (type == UserInfoConfig.TYPE_HEIGHT) {
			data = ProductUserPropertyUtils.getHeightValue();
		} else if (type == UserInfoConfig.TYPE_WEIGHT) {
			data = ProductUserPropertyUtils.getWeightValue();
		} else if (type == UserInfoConfig.TYPE_BLOOD_TYPE) {
			data = ProductUserPropertyUtils.getBloodTypeValue();
		} else if (type == UserInfoConfig.TYPE_EDUCATION) {
			data = ProductUserPropertyUtils.getEducation();
		} else if (type == UserInfoConfig.TYPE_OCCUPATION) {
			data = ProductUserPropertyUtils.getJobs();
		} else if (type == UserInfoConfig.TYPE_INCOME) {
			data = ProductUserPropertyUtils.getIncome();
		} else if (type == UserInfoConfig.TYPE_MARRIAGE) {
			data = ProductUserPropertyUtils.getMarriages();
		} else if (type == UserInfoConfig.TYPE_HOUSE) {
			data = ProductUserPropertyUtils.getHousing();
		} else if (type == UserInfoConfig.TYPE_YIDILIAN) {
			data = ProductUserPropertyUtils.getYidi();
		}
		radioDialog.setData(data);
		radioDialog.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String value = radioDialog.getValue();
				if (type == UserInfoConfig.TYPE_HEIGHT) {
					tvHeight.setText(value);
				} else if (type == UserInfoConfig.TYPE_WEIGHT) {
				} else if (type == UserInfoConfig.TYPE_BLOOD_TYPE) {
				} else if (type == UserInfoConfig.TYPE_EDUCATION) {
					tvXueli.setText(value);
				} else if (type == UserInfoConfig.TYPE_OCCUPATION) {
				} else if (type == UserInfoConfig.TYPE_INCOME) {
					tvShouru.setText(value);
				} else if (type == UserInfoConfig.TYPE_MARRIAGE) {
				} else if (type == UserInfoConfig.TYPE_HOUSE) {
				} else if (type == UserInfoConfig.TYPE_YIDILIAN) {
				}
				radioDialog.dismiss();
			}
		});
		radioDialog.show();
	}

}
