package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.app.ULoveApplication;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.chat.DemoHXSDKHelper;
import com.Aiputt.taohuayun.controller.InitController;
import com.Aiputt.taohuayun.event.LocationEvent;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.utils.CommonUtil;
import com.Aiputt.taohuayun.utils.DateUtil;
import com.Aiputt.taohuayun.utils.LocationUtil;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMGroupManager;
import com.umeng.analytics.AnalyticsConfig;
import com.umeng.update.UmengUpdateAgent;

import cn.jpush.android.api.InstrumentedActivity;

public class WelcomeActivity extends InstrumentedActivity {
    private static final String TAG = "WelcomeActivity";

    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener();

    private String oldDate;
    private String date;
    private InitController mController = InitController
            .getInstance();

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mContext = this;
        AnalyticsConfig.setChannel(CommonUtil.getCustomChannelInfo());
        UmengUpdateAgent.update(this);
        jumpToMainActivity();
        oldDate = SharePCach.loadStringCach("recommendTime");
        date = DateUtil.getYMDTime(System.currentTimeMillis());

        mController.initDevice();

        mLocationClient = new LocationClient(getApplicationContext());
        mLocationClient.registerLocationListener(myListener);
        initLocation();
    }

    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系，
        option.setScanSpan(0);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        mLocationClient.setLocOption(option);
        mLocationClient.start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean hasIndex = false;

    private void jumpToMainActivity() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (SharePCach.loadBooleanCach("index")) {
                    hasIndex = true;
                    // 如果用户名密码都有，直接进入主页面
                    if (DemoHXSDKHelper.getInstance().isLogined()) {
                        nextPage();
                    } else {
                        login();
                    }

                } else {
                    hasIndex = false;
                    // if (SharePCach.loadBooleanCach("fromLogin")) {
                    // Intent intent = new Intent(WelcomeActivity.this,
                    // MainActivity.class);
                    // startActivity(intent);
                    // } else {
                    if (!TextUtils.isEmpty(SharePCach.loadStringCach("gender"))) {
                        if (DemoHXSDKHelper.getInstance().isLogined()) {
                            nextQa();
                        } else {
                            login();
                        }

                    } else {
                        startActivity(SexActivity
                                .createIntent(getApplicationContext()));
                    }
                }
                finish();
            }
            // }
        }, 1000);
    }

    private void nextQa() {
        Intent intent = new Intent(WelcomeActivity.this, RegistQaActivity.class);
        intent.putExtra("gender", SharePCach.loadStringCach("gender"));
        intent.putExtra("age", SharePCach.loadStringCach("age"));
        startActivity(intent);
        finish();
    }

    private void nextPage() {
        if (hasIndex) {
            if (!oldDate.equals(date)) {
                startActivity(EveryDayRecommendActivity
                        .createIntent(getApplicationContext()));
            } else {
                startActivity(MainActivity
                        .createIntent(getApplicationContext()));
            }
        } else {
            nextQa();
        }
        finish();
    }

    /**
     * 登录
     */
    public void login() {

        final long start = System.currentTimeMillis();
        // 调用sdk登陆方法登陆聊天服务器
        EMChatManager.getInstance().login(
                SharePCach.loadStringCach(SharePCach.USER_NO),
                SharePCach.loadStringCach(SharePCach.HPWD), new EMCallBack() {

                    @Override
                    public void onSuccess() {
                        // 登陆成功，保存用户名密码
                        Log.e("LoginActivity", "Login success");
                        ULoveApplication.mApp.setUserName(SharePCach
                                .loadStringCach(SharePCach.USER_NO));
                        ULoveApplication.mApp.setPassword(SharePCach
                                .loadStringCach(SharePCach.HPWD));

                        try {
                            // ** 第一次登录或者之前logout后再登录，加载所有本地群和回话
                            // ** manually load all local groups and
                            EMGroupManager.getInstance().loadAllGroups();
                            EMChatManager.getInstance().loadAllConversations();
                            // 处理好友和群组
                            // initializeContacts();
                        } catch (Exception e) {
                            e.printStackTrace();
                            // 取好友或者群聊失败，不让进入主页面
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    ULoveApplication.mApp.logout(null);
                                    Toast.makeText(getApplicationContext(),
                                            R.string.login_failure_failed,
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        }
                        // 更新当前用户的nickname 此方法的作用是在ios离线推送时能够显示用户nick
                        boolean updatenick = EMChatManager
                                .getInstance()
                                .updateCurrentUserNick(
                                        ULoveApplication.currentUserNick.trim());
                        if (!updatenick) {
                            Log.e("LoginActivity",
                                    "update current user nick fail");
                        }
                        nextPage();

                        finish();
                    }

                    @Override
                    public void onProgress(int progress, String status) {
                    }

                    @Override
                    public void onError(final int code, final String message) {
                        nextPage();
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(
                                        getApplicationContext(),
                                        getString(R.string.Login_failed)
                                                + message, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        });
                    }
                });
    }

    /**
     * 实现实时位置回调监听
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            BDLocation mBDLocation = null;
            //Receive Location
            if (location != null) {
                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                    Log.i(TAG, "gps定位成功");
                    mBDLocation = location;
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                    Log.i(TAG, "网络定位成功");
                    mBDLocation = location;
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                    Log.i(TAG, "离线定位成功");
                    mBDLocation = location;
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    Log.i(TAG, "服务端网络定位失败");
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    Log.i(TAG, "网络不通导致定位失败，请检查网络是否通畅");
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    Log.i(TAG, "无法获取有效定位依据导致定位失败");
                }
                if (mLocationClient != null) {
                    mLocationClient.stop();
                }
                if (mBDLocation != null) {
                    LocationEvent event = new LocationEvent();
                    event.mBDLocation = mBDLocation;
                    LocationUtil.setCity(mBDLocation.getCity());
                    LocationUtil.setLatitude(mBDLocation.getLatitude() + "");
                    LocationUtil.setLongitude(mBDLocation.getLongitude() + "");
                    EventCenter.getInstance().send(event);
                }
            }
        }

    }
}
