
package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.alipay.utils.PayResult;
import com.Aiputt.taohuayun.controller.PayController;
import com.Aiputt.taohuayun.domain.OrderNoResponse;
import com.Aiputt.taohuayun.event.AlipayEvent;
import com.Aiputt.taohuayun.event.BuyVipEvent;
import com.Aiputt.taohuayun.event.DnaPayEvent;
import com.Aiputt.taohuayun.event.OrderNoEvent;
import com.Aiputt.taohuayun.event.RechargePayEvent;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.PromptManager;

/**
 * Created by zhonglq on 2015/8/3.
 */
public class WebViewActivity extends BaseActivity {

    private static final int PAYMENT_TYPE_ALIPAY = 1;
    private static final int PAYEMNT_TYPE_YINLIAN_YUYIN = 2;
    private static final int PAYMENT_TYPE_CHONGZHIKA = 3;

    private static final int YIDONG_CARD_LENGTH = 17;
    private static final int YIDONG_PWD_LENGTH = 18;
    private static final int LIANTONG_CARD_LENGTH = 15;
    private static final int LIANTONG_PWD_LENGTH = 19;
    private static final String YIDONG_INDEX = "000101";
    private static final String LIANTONG_INDEX = "000201";

    private static final String TAG = "WebViewActivity";
    private static final String URL = "url";
    private static final String TITLE = "title";

    private ProgressBar progressBar;
    private WebView webView;
    private String mTitle;
    private String mUrl;
    private PayController mPayController = PayController.getInstance();
    private PayHandler mHandler = new PayHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        mHandler.register();
        mTitle = getIntent().getStringExtra(TITLE);
        mUrl = getIntent().getStringExtra(URL);
        ivLeft.setVisibility(View.VISIBLE);
        setTitle(mTitle);
        progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        webView = (WebView) this.findViewById(R.id.webView);

        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();
        webView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JsApi(), "taohuayun");
        webView.getSettings().setDefaultTextEncodingName("utf-8");

        webView.setWebViewClient(webViewClient);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
            }

        });

        webView.loadUrl(mUrl);

    }

    private void onWebPageStarted() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private class JsApi {
        @JavascriptInterface
        public void showToast(String msg) {
            Log.i("WebViewActivity", "showToast");
            PromptManager.showShortToast(msg);
        }

        @JavascriptInterface
        public void aliPay(String productId, String payType) {
            showProgressDialog("加载中");
            mPayController.getOrderNo(productId, payType);
        }

        @JavascriptInterface
        public void doRechargeCard(String productId, String payType, String rechargeCardNumber, String rechargeCardPassword, String rechargeCardMoney) {
            String cardcode = "";
            if (rechargeCardNumber.trim().length() == LIANTONG_CARD_LENGTH && rechargeCardPassword.trim().length() == LIANTONG_PWD_LENGTH) {
                cardcode = LIANTONG_INDEX + rechargeCardMoney;
            } else if (rechargeCardNumber.length() == YIDONG_CARD_LENGTH && rechargeCardPassword.length() == YIDONG_PWD_LENGTH) {
                cardcode = YIDONG_INDEX + rechargeCardMoney;
            } else {
                PromptManager.showCenterToast("请输入有效的充值卡卡号和密码");
                return;
            }
            showProgressDialog("加载中");
            mPayController.doRechargeCardPay(productId, rechargeCardNumber, rechargeCardPassword);
        }

        @JavascriptInterface
        public void doDnaPay(String productid, String accountno, String mobileno, String name, String idcardno) {
            showProgressDialog("加载中");
            mPayController.doDnaPay(productid, accountno, mobileno, name, idcardno);
        }

    }

    /**
     * 支付宝测试
     */
    private void doAlipay(OrderNoResponse orderNoResponse) {
        mPayController.doAlipay(this, orderNoResponse);
    }

    private class PayHandler extends EventHandler {

        public void onEvent(OrderNoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                if (event.payType == PAYMENT_TYPE_ALIPAY) {
                    doAlipay(event.orderNoResponse);
                }
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

        public void onEvent(AlipayEvent event) {
            checkAlipayResult(event.result);
        }

        public void onEvent(DnaPayEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                Constants.IS_NEED_REFLASH = true;
                webView.loadUrl("http://www.aiputt.com/app/android/v1.2/pay/result.html");
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

        public void onEvent(RechargePayEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                PromptManager.showCenterToast("支付成功");
                notifyUpdateUserInfo();
                finish();
            } else {
                PromptManager.showCenterToast(event.msg);
            }
            dismissProgressDialog();
        }

    }

    /**
     * 支付宝支付完成
     *
     * @param result
     */
    private void checkAlipayResult(String result) {
        PayResult payResult = new PayResult(result);
        String resultInfo = payResult.getResult();
        String resultStatus = payResult.getResultStatus();
        if (TextUtils.equals(resultStatus, "9000")) {
            PromptManager.showCenterToast("支付成功");
            notifyUpdateUserInfo();
            finish();
        } else {
            if (TextUtils.equals(resultStatus, "8000")) {
                PromptManager.showCenterToast("支付结果确认中");
            } else {
                PromptManager.showCenterToast("支付失败");
            }
        }
    }


    private void notifyUpdateUserInfo() {
        EventCenter.getInstance().send(new BuyVipEvent());

    }

    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            onWebPageStarted();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            webView.loadUrl(url);
            return true;
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    public static Intent createIntent(Context context, String title, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(URL, url);
        intent.putExtra(TITLE, title);
        return intent;
    }

}
