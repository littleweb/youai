package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.domain.UserTipInfo;
import com.Aiputt.taohuayun.event.ChangeTabMineEvent;
import com.Aiputt.taohuayun.event.LoadPopInfoEvent;
import com.Aiputt.taohuayun.event.RefreshNearByEvent;
import com.Aiputt.taohuayun.event.StopLocationEvent;
import com.Aiputt.taohuayun.event.StopPopInfoEvent;
import com.Aiputt.taohuayun.event.UpdateUserInfoEvent;
import com.Aiputt.taohuayun.fragment.MailBoxFragment;
import com.Aiputt.taohuayun.fragment.MainFragment;
import com.Aiputt.taohuayun.fragment.MineFragment;
import com.Aiputt.taohuayun.fragment.NearByFragment;
import com.Aiputt.taohuayun.fragment.SearchFragment;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.CommonUtil;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.utils.LocationUtil;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.ChoicePrivinceDialog;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FragmentActivity implements OnClickListener {

    private final static int INDEX_MAIN = 0;
    private final static int INDEX_SERARCH = 1;
    private final static int INDEX_NEARBY = 2;
    private final static int INDEX_MAILBOX = 3;
    private final static int INDEX_MINE = 4;

    private int currentIndex = 0;

    private FragmentManager mFragmentManager;

    private MainFragment mMainFragment;
    private SearchFragment mSearchFragment;
    private NearByFragment mNearByFragment;
    private MineFragment mMineFragment;
    private MailBoxFragment mMailBoxFragment;

    private LinearLayout llMain;
    private ImageButton ibMain;
    private TextView tvMain;
    private LinearLayout llSearch;
    private ImageButton ibSearch;
    private TextView tvSearch;
    private LinearLayout llMailBox;
    private ImageButton ibMailBox;
    private TextView tvMailBox;
    private LinearLayout llNearBy;
    private ImageButton ibNearBy;
    private TextView tvNearBy;
    private LinearLayout llMine;
    private ImageButton ibMine;
    private TextView tvMine;
    private int focusColor;

    private ImageView ivLeft;
    private ImageView ivRight;
    private TextView tvLeft;
    private TextView tvTitle;
    private TextView tvRight;

    public static ImageView ivIcon;
    private String mPageName;
    private UserController mUserController = UserController.getInstance();
    private UpdateUserInfoHandler mHandler = new UpdateUserInfoHandler();

    private TextView tvChoiceLeft;
    private TextView tvChoiceMiddle;
    private TextView tvChoiceRight;
    private RelativeLayout rlNewMessage;

    private RoundedImageView ivHeadPhotoItem;
    private TextView questionTitle;
    private UserTipInfo userTipInfo;
    private boolean isExit = true;

    private class UpdateUserInfoHandler extends EventHandler {
        public void onEvent(UpdateUserInfoEvent event) {
            if (event.code == Constants.SERVER_CONNECT_OK) {
                PromptManager.showCenterToast("保存成功");
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }
    }

    private MainHandler mJumpHandler = new MainHandler();

    private class MainHandler extends EventHandler {
        public void onEvent(ChangeTabMineEvent event) {
            mMineFragment = null;
            setTabSelection(INDEX_MINE);
        }

        public void onEvent(LoadPopInfoEvent event) {
            if (event.code == Constants.SERVER_CONNECT_OK) {
                rlNewMessage.setVisibility(View.VISIBLE);
                userTipInfo = event.userTipInfo;
                ImageLoader.getInstance().displayImage(userTipInfo.getUser_info().getUser_face(), ivHeadPhotoItem, ImageLoadUtils.getNormalOptions());
                questionTitle.setText(userTipInfo.getQa_info().getQuestion());
                tvChoiceLeft.setText(userTipInfo.getQa_info().getOptions().get(0));
                tvChoiceMiddle.setText(userTipInfo.getQa_info().getOptions().get(1));
                tvChoiceRight.setText(userTipInfo.getQa_info().getOptions().get(2));
                startAnim();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPageName = this.getClass().getName();
        initViews();
        mJumpHandler.register();
        mHandler.register();
        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.popBackStackImmediate();
        setTabSelection(INDEX_MAIN);
        UmengUpdateAgent.update(this);
        UserController.getInstance().loadUserInfo();
    }

    private void startAnim() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(
                rlNewMessage, "translationX", -1500f, 0f);
        animator.setDuration(2000);
        if (!isExit) {
            exitAnim();
            animator.setStartDelay(5000);
        }
        isExit = false;
        animator.start();
    }

    @Override
    protected void onResume() {
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart(mPageName);
        super.onResume();
    }

    @Override
    protected void onPause() {
        MobclickAgent.onPause(this);
        MobclickAgent.onPageEnd(mPageName);
        super.onPause();
        mHandler.unregister();
    }

    /**
     * 初始化控件
     */
    private void initViews() {
        focusColor = Color.parseColor("#DD368C");
        llMain = (LinearLayout) findViewById(R.id.ll_main);
        ibMain = (ImageButton) findViewById(R.id.ib_main);
        tvLeft = (TextView) findViewById(R.id.tvLeft);
        tvMain = (TextView) findViewById(R.id.tv_main);
        tvRight = (TextView) findViewById(R.id.tvRight);
        llSearch = (LinearLayout) findViewById(R.id.ll_search);
        ibSearch = (ImageButton) findViewById(R.id.ib_search);
        tvSearch = (TextView) findViewById(R.id.tv_search);
        llMailBox = (LinearLayout) findViewById(R.id.ll_mailBox);
        ibMailBox = (ImageButton) findViewById(R.id.ib_mailBox);
        tvMailBox = (TextView) findViewById(R.id.tv_mailBox);
        llNearBy = (LinearLayout) findViewById(R.id.ll_nearBy);
        ibNearBy = (ImageButton) findViewById(R.id.ib_nearBy);
        tvNearBy = (TextView) findViewById(R.id.tv_nearBy);
        llMine = (LinearLayout) findViewById(R.id.ll_mine);
        ibMine = (ImageButton) findViewById(R.id.ib_mine);
        tvMine = (TextView) findViewById(R.id.tv_mine);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        ivLeft = (ImageView) findViewById(R.id.ivLeft);
        ivRight = (ImageView) findViewById(R.id.ivRight);
        ivIcon = (ImageView) findViewById(R.id.ivIcon);
        llMain.setOnClickListener(this);
        llSearch.setOnClickListener(this);
        llMailBox.setOnClickListener(this);
        llNearBy.setOnClickListener(this);
        llMine.setOnClickListener(this);
        ivLeft.setOnClickListener(this);
        ivRight.setOnClickListener(this);
        tvRight.setOnClickListener(this);
        tvLeft.setOnClickListener(this);

        rlNewMessage = (RelativeLayout) this.findViewById(R.id.rlNewMessage);
        tvChoiceLeft = (TextView) findViewById(R.id.tvChoiceLeft);
        tvChoiceMiddle = (TextView) findViewById(R.id.tvChoiceMiddle);
        tvChoiceRight = (TextView) findViewById(R.id.tvChoiceRight);
        questionTitle = (TextView) findViewById(R.id.questionTitle);
        ivHeadPhotoItem = (RoundedImageView) findViewById(R.id.ivHeadPhotoItem);

        tvChoiceLeft.setOnClickListener(this);
        tvChoiceMiddle.setOnClickListener(this);
        tvChoiceRight.setOnClickListener(this);

    }

    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_main:
                setTabSelection(INDEX_MAIN);
                break;
            case R.id.ll_search:
                setTabSelection(INDEX_SERARCH);
                break;
            case R.id.ll_mailBox:
                setTabSelection(INDEX_MAILBOX);
                break;
            case R.id.ll_nearBy:
                setTabSelection(INDEX_NEARBY);
                break;
            case R.id.ll_mine:
                setTabSelection(INDEX_MINE);
                break;
            case R.id.ivRight:
                if (currentIndex == INDEX_MINE) {
                    startActivity(SettingActivity.createIntent(this));
                } else if (currentIndex == INDEX_NEARBY) {
                    EventCenter.getInstance().send(new RefreshNearByEvent());
                } else if (currentIndex == INDEX_MAIN) {
                    if (mMainFragment != null) {
                        mMainFragment.onRefresh();
                    }
                }
                break;
            case R.id.tvRight:
                Intent intent = new Intent(this, SearchOptionActivity.class);
                startActivity(intent);
                break;
            case R.id.tvLeft:
                choiceLocation();
                break;
            case R.id.tvChoiceLeft:
                exitAnim();
                break;
            case R.id.tvChoiceMiddle:
                PromptManager.showCenterToast("打招呼成功");
                exitAnim();
                UserController.getInstance().sayHi(userTipInfo.getUser_info().getUid());
                break;
            case R.id.tvChoiceRight:
                PromptManager.showCenterToast("打招呼成功");
                exitAnim();
                UserController.getInstance().sayHi(userTipInfo.getUser_info().getUid());
                break;
        }

    }

    private void exitAnim() {
        isExit = true;
        ObjectAnimator animator = ObjectAnimator.ofFloat(
                rlNewMessage, "translationX", 0f, -CommonUtil.getDisplayMetrics().widthPixels);
        animator.setDuration(3000);
        animator.start();
    }

    /**
     * 选择居住地
     */
    private void choiceLocation() {
        final ChoicePrivinceDialog choiceCityDialog = new ChoicePrivinceDialog(
                this);
        choiceCityDialog.setButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value = choiceCityDialog.getValue();
                tvLeft.setText(value);
                Map<String, String> map = new HashMap<String, String>();
                map.put("province", value);
                mUserController.updateUserInfo(map);
                choiceCityDialog.dismiss();
            }
        });
        choiceCityDialog.show();
    }

    private void setTabSelection(int index) {
        currentIndex = index;
        resetBottomStatus();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        if (CommonUtil.isHighEndGfx(this) == true) {
            hideFragments(transaction);
        }
        switch (index) {
            case INDEX_MAIN:
                ibMain.setImageResource(R.drawable.tab_icon_1_focused);
                tvMain.setTextColor(focusColor);
                if (mMainFragment == null) {
                    mMainFragment = new MainFragment();
                    Bundle para = new Bundle();
                    para.putString("title", "main");
                    mMainFragment.setArguments(para);
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.add(R.id.id_content, mMainFragment);
                    }
                } else {
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.show(mMainFragment);
                    }
                }
                if (CommonUtil.isHighEndGfx(this) == false) {
                    transaction.replace(R.id.id_content, mMainFragment);
                }
                break;
            case INDEX_SERARCH:
                ibSearch.setImageResource(R.drawable.tab_icon_2_focused);
                tvSearch.setTextColor(focusColor);
                if (mSearchFragment == null) {
                    mSearchFragment = new SearchFragment();
                    Bundle para = new Bundle();
                    para.putString("title", "main");
                    mSearchFragment.setArguments(para);
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.add(R.id.id_content, mSearchFragment);
                    }
                } else {
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.show(mSearchFragment);
                    }
                }
                if (CommonUtil.isHighEndGfx(this) == false) {
                    transaction.replace(R.id.id_content, mSearchFragment);
                }
                break;
            case INDEX_NEARBY:
                ibNearBy.setImageResource(R.drawable.tab_icon_4_foucsed);
                tvNearBy.setTextColor(focusColor);
                if (mNearByFragment == null) {
                    mNearByFragment = new NearByFragment();
                    Bundle para = new Bundle();
                    para.putString("title", "main");
                    mNearByFragment.setArguments(para);
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.add(R.id.id_content, mNearByFragment);
                    }
                } else {
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.show(mNearByFragment);
                    }
                }
                if (CommonUtil.isHighEndGfx(this) == false) {
                    transaction.replace(R.id.id_content, mNearByFragment);
                }
                break;
            case INDEX_MAILBOX:
                ibMailBox.setImageResource(R.drawable.tab_icon_3_focused);
                tvMailBox.setTextColor(focusColor);
                if (mMailBoxFragment == null) {
                    mMailBoxFragment = new MailBoxFragment();
                    Bundle para = new Bundle();
                    para.putString("title", "main");
                    mMailBoxFragment.setArguments(para);
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.add(R.id.id_content, mMailBoxFragment);
                    }
                } else {
                    if (CommonUtil.isHighEndGfx(this) == true) {

                        transaction.show(mMailBoxFragment);
                        mMailBoxFragment.updateData();
                    }
                }
                if (CommonUtil.isHighEndGfx(this) == false) {
                    transaction.replace(R.id.id_content, mMailBoxFragment);
                    if (mMailBoxFragment != null) {
                        mMailBoxFragment.updateData();

                    }
                }
                break;
            case INDEX_MINE:
                ibMine.setImageResource(R.drawable.tab_icon_5_focused);
                tvMine.setTextColor(focusColor);
                if (mMineFragment == null) {
                    mMineFragment = new MineFragment();
                    Bundle para = new Bundle();
                    para.putString("title", "main");
                    mMineFragment.setArguments(para);
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.add(R.id.id_content, mMineFragment);
                    }
                } else {
                    if (CommonUtil.isHighEndGfx(this) == true) {
                        transaction.show(mMineFragment);
                    }
                }
                if (CommonUtil.isHighEndGfx(this) == false) {
                    transaction.replace(R.id.id_content, mMineFragment);
                }
                break;
        }
        transaction.commitAllowingStateLoss();

        setTitlBarInfo(index);
    }

    private void setTitlBarInfo(int index) {
        switch (index) {
            case INDEX_MAIN:
                tvLeft.setVisibility(View.GONE);
                String city = SharePCach.loadStringCach("selectCity");
                if (TextUtils.isEmpty(city)) {
                    if (!TextUtils.isEmpty(LocationUtil.getCity())) {
                        SharePCach.saveStringCach("selectCity", LocationUtil.getCity());
                        tvLeft.setText(LocationUtil.getCity());
                    } else {
                        tvLeft.setText("北京");
                    }
                } else {
                    tvLeft.setText(city);
                }

                tvRight.setVisibility(View.GONE);
                ivRight.setVisibility(View.VISIBLE);
                ivRight.setImageResource(R.drawable.bg_refresh_selector);
                tvTitle.setText("缘分");
                break;
            case INDEX_SERARCH:
                tvLeft.setVisibility(View.GONE);
                tvLeft.setText("");
                tvRight.setVisibility(View.VISIBLE);
                ivRight.setVisibility(View.GONE);
                tvTitle.setText("搜索");
                break;
            case INDEX_NEARBY:
                tvLeft.setVisibility(View.GONE);
                tvLeft.setText("");
                tvTitle.setText("附近");
                tvRight.setVisibility(View.GONE);
                ivRight.setVisibility(View.VISIBLE);
                ivRight.setImageResource(R.drawable.bg_refresh_selector);
                break;
            case INDEX_MAILBOX:
                tvLeft.setVisibility(View.GONE);
                tvLeft.setText("");
                tvTitle.setText("信箱");
                tvRight.setVisibility(View.GONE);
                ivRight.setVisibility(View.VISIBLE);
                ivRight.setImageResource(R.drawable.bg_refresh_selector);
                break;
            case INDEX_MINE:
                tvLeft.setVisibility(View.GONE);
                tvTitle.setText("我的空间");
                tvRight.setVisibility(View.GONE);
                ivRight.setVisibility(View.VISIBLE);
                ivRight.setImageResource(R.drawable.bg_setting_selector);
                break;
        }
    }

    /**
     * 隐藏fragments
     *
     * @param transaction
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (mMainFragment != null) {
            transaction.hide(mMainFragment);
        }
        if (mSearchFragment != null) {
            transaction.hide(mSearchFragment);
        }
        if (mMailBoxFragment != null) {
            transaction.hide(mMailBoxFragment);
        }
        if (mNearByFragment != null) {
            transaction.hide(mNearByFragment);
        }
        if (mMineFragment != null) {
            transaction.hide(mMineFragment);
        }
    }

    /**
     * 重置底部菜单栏状态
     */
    private void resetBottomStatus() {
        ibMain.setImageResource(R.drawable.tab_icon_1_default);
        tvMain.setTextColor(getResources().getColor(R.color.tab_text));
        ibSearch.setImageResource(R.drawable.tab_icon_2_default);
        tvSearch.setTextColor(getResources().getColor(R.color.tab_text));
        ibMailBox.setImageResource(R.drawable.tab_icon_3_default);
        tvMailBox.setTextColor(getResources().getColor(R.color.tab_text));
        ibNearBy.setImageResource(R.drawable.tab_icon_4_default);
        tvNearBy.setTextColor(getResources().getColor(R.color.tab_text));
        ibMine.setImageResource(R.drawable.tab_icon_5_default);
        tvMine.setTextColor(getResources().getColor(R.color.tab_text));

    }

    @Override
    protected void onDestroy() {
        mHandler.unregister();
        mJumpHandler.unregister();
        super.onDestroy();
    }

    long exitTime = 0;

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - exitTime > 2000) {
            PromptManager.showShortToast("再次点击退出!");
            exitTime = System.currentTimeMillis();
        } else {
            EventCenter.getInstance().send(new StopLocationEvent());
            EventCenter.getInstance().send(new StopPopInfoEvent());
            super.onBackPressed();
        }
    }
}
