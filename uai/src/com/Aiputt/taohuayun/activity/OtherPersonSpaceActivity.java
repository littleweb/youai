package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.adapter.UserDeatailAdapter;
import com.Aiputt.taohuayun.controller.NearByController;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.ChangeTabMineEvent;
import com.Aiputt.taohuayun.event.RandomInfoEvent;
import com.Aiputt.taohuayun.event.SayHiEventEvent;
import com.Aiputt.taohuayun.event.UserInfoEvent;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.Aiputt.taohuayun.widgets.TipDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/7/17.
 */
public class OtherPersonSpaceActivity extends BaseActivity implements UserDeatailAdapter.OnItemClickListener {

    private static final String USER_ID = "user_id";
    private User mUser;
    private int useId;
    private UserController mUserController = UserController.getInstance();
    private OtherSpaceHandler mHandler = new OtherSpaceHandler();
    private LinearLayout.LayoutParams layoutParams;

    private RoundedImageView userHeadPhoto;
    private TextView userDuBai;
    private TextView userEducation;
    private TextView userWork;
    private TextView userIncome;
    private TextView userMarital;
    private TextView userHouse;
    private TextView userYiDiLian;
    private TextView userConstellation;
    private TextView userFeature;
    private TextView userHobby;
    private RelativeLayout rlSayHi;

    private TextView tvName;
    private TextView tvID;
    private TextView tvAge;
    private TextView tvLocation;
    private TextView tvDisposition;
    private TextView tvHobby;
    private LinearLayout llFeatureHobby;
    private RecyclerView userPhoto;
    private UserDeatailAdapter adapter;
    private ArrayList<ImageList> mUserImg_list;

    private RelativeLayout show_user_state_layout;

    private TextView mVip;
    private TextView mCompleteInfo;
    private TextView zhengyouyaoqiu;
    private TextView goodPlace;
    private TextView qinminxingwei;
    private TextView hasChild;

    private TextView tvHiText;
    private boolean isFromThisSayHi = false;
    private ImageView mNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_person_space);
        mHandler.register();
        initView();
        setListener();
        useId = getIntent().getIntExtra(USER_ID, 0);
        if (useId != 0) {
            showProgressDialog("加载中");
            loadUserInfo();
        } else {
            PromptManager.showCenterToast("亲，没有传用户哦");
        }
    }

    /**
     * 加载用户信息
     */
    private void loadUserInfo() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("uid", useId + "");
        mUserController.getUserDetail(map);
    }


    /**
     * 初始化控件
     */
    private void initView() {
        mNext = (ImageView) findViewById(R.id.next);
        tvHiText = (TextView) findViewById(R.id.tvHiText);
        tvName = (TextView) findViewById(R.id.tvName);
        tvID = (TextView) findViewById(R.id.tvID);
        tvAge = (TextView) findViewById(R.id.tvAge);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvDisposition = (TextView) findViewById(R.id.tvDisposition);
        tvHobby = (TextView) findViewById(R.id.tvHobby);
        llFeatureHobby = (LinearLayout) findViewById(R.id.llFeatureHobby);
        adapter = new UserDeatailAdapter(this, false);
        userPhoto = (RecyclerView) findViewById(R.id.userPhoto);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        userPhoto.setLayoutManager(layoutManager);
        adapter.setOnItemClickListener(this);
        userPhoto.setAdapter(adapter);

        userHeadPhoto = (RoundedImageView) findViewById(R.id.userHeadPhoto);
        userDuBai = (TextView) findViewById(R.id.userDuBai);
        userEducation = (TextView) findViewById(R.id.userEducation);
        userWork = (TextView) findViewById(R.id.userWork);
        userIncome = (TextView) findViewById(R.id.userIncome);
        userMarital = (TextView) findViewById(R.id.userMarital);
        userHouse = (TextView) findViewById(R.id.userHouse);
        userYiDiLian = (TextView) findViewById(R.id.userYiDiLian);
        userConstellation = (TextView) findViewById(R.id.userConstellation);
        userFeature = (TextView) findViewById(R.id.userFeature);
        userHobby = (TextView) findViewById(R.id.userHobby);
        rlSayHi = (RelativeLayout) findViewById(R.id.rlSayHi);

        show_user_state_layout = (RelativeLayout) findViewById(R.id.rlUserStatus);
        mVip = (TextView) findViewById(R.id.vip);
        mCompleteInfo = (TextView) findViewById(R.id.completeInfo);
        zhengyouyaoqiu = (TextView) findViewById(R.id.zhengyouyaoqiu);
        goodPlace = (TextView) findViewById(R.id.goodPlace);
        qinminxingwei = (TextView) findViewById(R.id.qinMiXingWei);
        hasChild = (TextView) findViewById(R.id.hasChild);
    }

    private void setListener() {
        mNext.setOnClickListener(this);
        rlSayHi.setOnClickListener(this);
        ivLeft.setVisibility(View.VISIBLE);
        userHeadPhoto.setOnClickListener(this);
    }

    /**
     * 填充用户数据
     */
    private void setUserData() {
        setTitle("她的空间");
        tvName.setText(mUser.getUser_nick());
        tvAge.setText(mUser.getUser_age() + "岁");
        tvLocation.setText(mUser.getUser_location());
        tvID.setText("ID:" + mUser.getUser_no());
        if (TextUtils.isEmpty(mUser.getUser_feature()) && TextUtils.isEmpty(mUser.getUser_hobby())) {
            llFeatureHobby.setVisibility(View.GONE);
            tvDisposition.setVisibility(View.GONE);
        } else {
            llFeatureHobby.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(mUser.getUser_hobby())) {
                tvHobby.setVisibility(View.GONE);
            } else {
                tvHobby.setText(mUser.getUser_hobby().split(",")[0]);
            }
            if (TextUtils.isEmpty(mUser.getUser_feature())) {
                tvDisposition.setVisibility(View.GONE);
            } else {
                tvDisposition.setText(mUser.getUser_feature().split(",")[0]);
            }
        }
        mImageLoader.displayImage(mUser.getUser_face(), userHeadPhoto, ImageLoadUtils.getHeadPhotoOptions());
        userDuBai.setText(dealWithField(mUser.getUser_description()));
        userEducation.setText(dealWithField(mUser.getUser_education()));
        userWork.setText(dealWithField(mUser.getUser_job()));
        userIncome.setText(dealWithField(mUser.getUser_income()));
        userMarital.setText(dealWithField(mUser.getUser_marriage()));
        userHouse.setText(dealWithField(mUser.getUser_housing()));
        userYiDiLian.setText(dealWithField(mUser.getUser_yidi()));
        userConstellation.setText(dealWithField(mUser.getUser_xz()));
        userFeature.setText(dealWithField(mUser.getUser_feature()));
        userHobby.setText(dealWithField(mUser.getUser_hobby()));
        if (mUser.getIsSayHello() == 1) {
            rlSayHi.setBackgroundResource(R.drawable.ic_say_hello_bg_pressed);
            tvHiText.setText("已打过招呼");
        } else {
            tvHiText.setText("打招呼");
            rlSayHi.setEnabled(true);
            rlSayHi.setBackgroundResource(R.drawable.ic_say_hello_bg_normal);
        }
        addPhotoView();
        if (mUser.getScore() == 100) {
            mCompleteInfo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_complete_info, 0, 0);
        } else {
            mCompleteInfo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_complete_info_gray, 0, 0);
        }

        if (mUser.getIs_vip() == 1) {
            show_user_state_layout.setVisibility(View.GONE);
            mVip.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vip, 0, 0);
        } else {
            show_user_state_layout.setVisibility(View.VISIBLE);
            mVip.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vip_gray, 0, 0);
        }

        zhengyouyaoqiu.setText(TextUtils.isEmpty(mUser.getPartner_conditions()) ? "暂无" : mUser.getPartner_conditions());

        qinminxingwei.setText(dealWithField(mUser.getUser_premarital_sex()));
        goodPlace.setText(dealWithField(mUser.getUser_charm()));
        hasChild.setText(dealWithField(mUser.getUser_have_baby()));


    }

    private String dealWithField(String data) {
        if (TextUtils.isEmpty(data) || data.equals("0")) {
            return "保密";
        } else {
            return data;
        }
    }

    /**
     * 添加照片
     */
    private void addPhotoView() {
        mUserImg_list = mUser.getImg_list();
        if (mUserImg_list != null && !mUserImg_list.isEmpty()) {
            adapter.replace(mUserImg_list);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlSayHi:
                showProgressDialog("正在打招呼");
                isFromThisSayHi = true;
                NearByController.getInstance().sayHi(mUser);
                break;
            case R.id.userHeadPhoto:
                ImageList imageList = new ImageList();
                imageList.setImg_url_big(mUser.getUser_face_big());
                ArrayList<ImageList> list = new ArrayList<ImageList>();
                list.add(imageList);
                Intent intent = PreviewPhotoActivity.createIntent(mContext, 0, true, mUser, false);
                startActivity(intent);
                break;
            case R.id.next:
                showProgressDialog("加载中");
                mUserController.loadRandomInfo();
                break;
        }
        super.onClick(v);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    @Override
    public void onItemClick(int position) {
        if (mUserImg_list == null || position == 0) {
            if (TextUtils.isEmpty(UserController.getInstance().getUser().getUser_face())) {
                final TipDialog dialog = new TipDialog(OtherPersonSpaceActivity.this);
                dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        OtherPersonSpaceActivity.this.finish();
                        EventCenter.getInstance().send(new ChangeTabMineEvent());
                    }
                });
                dialog.show();
            } else {
//                NearByController.getInstance().requestPhoto(mUser);
                PromptManager.showCenterToast("索要照片成功,请耐心等待对方的回复");
            }
        } else {
            Intent intent = PreviewPhotoActivity.createIntent(this, position - 1, false, mUser, false);
            startActivity(intent);
        }
    }

    class OtherSpaceHandler extends EventHandler {
        public boolean onEvent(UserInfoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                if (event.mUser != null) {
                    mUser = event.mUser;
                    setUserData();
                } else {
                    PromptManager.showCenterToast("加载失败,请稍后再试");
                }
            } else {
                PromptManager.showCenterToast(event.msg);
            }
            return true;
        }

        public void onEvent(SayHiEventEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                if (isFromThisSayHi) {
                    PromptManager.showCenterToast("已成功打招呼，等待对方回信");
                }
                tvHiText.setText("已打过招呼");
                mUser.setIsSayHello(1);
                rlSayHi.setEnabled(false);
                rlSayHi.setBackgroundResource(R.drawable.ic_say_hello_bg_pressed);
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

        public void onEvent(RandomInfoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                mUser = event.mUser;
                setUserData();
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

    }

    public static Intent createIntent(Context context, int uid) {
        Intent intent = new Intent(context, OtherPersonSpaceActivity.class);
        intent.putExtra(USER_ID, uid);
        return intent;
    }

}
