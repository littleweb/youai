package com.Aiputt.taohuayun.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.adapter.SuperAdapter;
import com.Aiputt.taohuayun.controller.RecomController;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.RecomEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.DateUtil;
import com.Aiputt.taohuayun.utils.HuanXinSendMessage;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.xlistview.XListView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class EveryDayRecommendActivity extends BaseActivity implements
		XListView.IXListViewListener, AdapterView.OnItemClickListener {

	private XListView xlvNearBy;
	private SearchAdapter mAdapter;
	private TextView emptyView;
	private List<User> userList = new ArrayList<User>();
	private String currentUserNo;
	private User currentUser;

	private RecomHandler mHandler = new RecomHandler();
	private RecomController mController = RecomController.getInstance();

	public static Intent createIntent(Context context) {
		return new Intent(context, EveryDayRecommendActivity.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharePCach.saveStringCach("recommendTime",
				DateUtil.getYMDTime(System.currentTimeMillis()));
		setContentView(R.layout.activity_everyday_recommend);
		mHandler.register();
		xlvNearBy = (XListView) findViewById(R.id.xlvNearBy);
		emptyView = (TextView) findViewById(R.id.emptyView);
		xlvNearBy.setPullLoadEnable(false);
		mAdapter = new SearchAdapter(this);
		xlvNearBy.setAdapter(mAdapter);
		xlvNearBy.setXListViewListener(this, 0);
		xlvNearBy.setRefreshTime();
		xlvNearBy.setOnItemClickListener(this);
		showProgressDialog(R.string.pl_wait);
		mController.getRecomList();
		setTitle("每日推荐");
		setLeftButtonVisible();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.ivLeft) {
			startActivity(MainActivity.createIntent(getApplicationContext()));
			finish();
		} else if (id == R.id.ivRight || id == R.id.tvRight) {
			onRightButtonClick();
		}
	}
	
	@Override
	public void onBackPressed() {
		startActivity(MainActivity.createIntent(getApplicationContext()));
		finish();
	}

	@Override
	protected void onDestroy() {
		mHandler.unregister();
		super.onDestroy();
	}

	private class RecomHandler extends EventHandler {

		public void onEvent(RecomEvent event) {
			dismissProgressDialog();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				if (event.userList != null && event.userList.size() != 0) {
					mAdapter.replaceAll(event.userList);
					mAdapter.notifyDataSetChanged();
				} else {
					xlvNearBy.setEmptyView(emptyView);
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
			dismissProgressDialog();
		}

		public void onEvent(ResultEvent event) {
			dismissProgressDialog();
			if (event.code == 0) {
				currentUser.setIsSayHello(1);
				mAdapter.notifyDataSetChanged();
				HuanXinSendMessage.sendTxTMessage(
						EveryDayRecommendActivity.this, currentUserNo,
						event.data);
				PromptManager.showCenterToast("打招呼成功");
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		User user = mAdapter.getItem(position - 1);
		Intent intent = OtherPersonSpaceActivity.createIntent(this,
				user.getUid());
		startActivity(intent);

	}

	@Override
	public void onRefresh(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadMore(int id) {
		// TODO Auto-generated method stub

	}

	public class SearchAdapter extends SuperAdapter<User> {

		public SearchAdapter(Context context) {
			super(context);
		}

		public SearchAdapter(List<User> list) {
			super(list);
		}

		public SearchAdapter(Context context, List<User> list) {
			super(context, list);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			if (convertView == null) {
				convertView = View.inflate(mContext, R.layout.seach_list_item,
						null);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final User user = mList.get(position);
			// holder.headPhoto.setImageResource(R.drawable.round_image_head_girl);
			ImageLoader.getInstance().displayImage(user.getUser_face(),
					holder.headPhoto);
			holder.nickName.setText(user.getUser_nick());
			holder.personAge.setText(user.getUser_age() + "岁");
			if (TextUtils.isEmpty(user.getUser_feature())) {
				holder.personChracter.setVisibility(View.GONE);
            } else {
                String feature = user.getUser_feature().split(",")[0];
                if(feature.endsWith("]")){
                    feature = feature.substring(0,feature.length()-1);
                }
                holder.personChracter.setText(feature.substring(1));
            }
			if (TextUtils.isEmpty(user.getUser_hobby())) {
				holder.personHobby.setVisibility(View.GONE);
            } else {
                String hobby = user.getUser_hobby().split(",")[0];
                if(hobby.endsWith("]")){
                    hobby = hobby.substring(0,hobby.length()-1);
                }
                holder.personHobby.setText(hobby.substring(1));
            }
			holder.personHeight.setText(user.getUser_height() + "cm");
			holder.personIncome.setText(user.getUser_income() + "元");
			if (user.getIsSayHello() == 1) {
				holder.sayHello
						.setBackgroundResource(R.drawable.n_ck);
			} else {
				holder.sayHello
						.setBackgroundResource(R.drawable.n_ck_press);
			}
			holder.sayHello.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (user.getIsSayHello() == 0) {
						currentUser = user;
						showProgressDialog(R.string.pl_wait);
						currentUserNo = user.getUser_no();
						mController.actionDazhaohu(String.valueOf(user.getUid()));
					}
				}
			});
			return convertView;
		}

		public class ViewHolder {
			public final ImageView headPhoto;
			public final TextView nickName;
			public final LinearLayout llnearbypersonitemdis;
			public final TextView personAge;
			public final TextView personHeight;
			public final TextView personIncome;
			public final TextView personHobby;
			public final TextView personChracter;
			public final LinearLayout llnearbypersonitemcharacter;
			public final TextView sayHello;
			public final View root;

			public ViewHolder(View root) {
				headPhoto = (ImageView) root.findViewById(R.id.headPhoto);
				nickName = (TextView) root.findViewById(R.id.nickName);
				llnearbypersonitemdis = (LinearLayout) root
						.findViewById(R.id.ll_nearby_person_item_dis);
				personAge = (TextView) root.findViewById(R.id.personAge);
				personHeight = (TextView) root.findViewById(R.id.personHeight);
				personIncome = (TextView) root.findViewById(R.id.personIncome);
				personHobby = (TextView) root.findViewById(R.id.personHobby);
				personChracter = (TextView) root
						.findViewById(R.id.personChracter);
				llnearbypersonitemcharacter = (LinearLayout) root
						.findViewById(R.id.ll_nearby_person_item_character);
				sayHello = (TextView) root.findViewById(R.id.sayHello);
				this.root = root;
			}
		}
	}

}
