
package com.Aiputt.taohuayun.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.adapter.DispositionAdapter;
import com.Aiputt.taohuayun.adapter.HobbyAdapter;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.domain.Disposition;
import com.Aiputt.taohuayun.domain.Hobby;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.event.UpdateUserInfoEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.ProductUserPropertyUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.utils.ProvinceCityAreaUtils;
import com.Aiputt.taohuayun.widgets.ChangeNickDialog;
import com.Aiputt.taohuayun.widgets.ChoiceCityDialog;
import com.Aiputt.taohuayun.widgets.ChoiceDateDialog;
import com.Aiputt.taohuayun.widgets.IntervalSelectionDialog;
import com.Aiputt.taohuayun.widgets.MultiplyDialog;
import com.Aiputt.taohuayun.widgets.RadioDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by zhonglq on 2015/7/19.
 */
public class PerfectInformationActivity extends BaseActivity {

    public static String USER_MODEL = "userInfo";

    private UserController mUserController = UserController.getInstance();
    private UpdateUserInfoHandler mHandler = new UpdateUserInfoHandler();

    private EditText userDuBai;
    private LinearLayout llUserFeature;
    private ImageButton ibAddFeature;
    private LinearLayout llUserHobby;
    private ImageButton ibAddHobby;
    private LinearLayout llNickName;
    private TextView nickName;
    private LinearLayout llBirthDay;
    private TextView birthDay;
    private LinearLayout llAge;
    private TextView age;
    private LinearLayout llConstellation;
    private TextView constellation;
    private LinearLayout llLiveLocation;
    private TextView liveLocation;
    private LinearLayout llJiGuan;
    private TextView jiGuan;
    private LinearLayout llHeight;
    private TextView height;
    private LinearLayout llWeight;
    private TextView weight;
    private LinearLayout llBloodType;
    private TextView bloodType;
    private LinearLayout llEducation;
    private TextView education;
    private LinearLayout llOccupation;
    private TextView occupation;
    private LinearLayout llIncome;
    private TextView income;
    private LinearLayout llGoodPlace;
    private TextView goodPlace;
    private LinearLayout llMarriage;
    private TextView marriage;
    private LinearLayout llHouse;
    private TextView house;
    private LinearLayout llYiDiLian;
    private TextView yiDiLian;
    private LinearLayout llLoveOther;
    private TextView loveOther;
    private LinearLayout llQinMiXingWei;
    private TextView qinMiXingWei;
    private LinearLayout llWithParent;
    private TextView withParent;
    private LinearLayout llHasChild;
    private TextView hasChild;
    private LinearLayout userZhengyouInfo;
    private LinearLayout llHisLocation;
    private TextView hisLocation;
    private LinearLayout llHisAge;
    private TextView hisAge;
    private LinearLayout llHisHeight;
    private TextView hisHeight;
    private LinearLayout llHisEducation;
    private TextView hisEducation;
    private LinearLayout llHisIncome;
    private TextView hisIncome;
    private Button save;

    private LinearLayout.LayoutParams layoutParams;
    private List<Disposition> allDisposition = new ArrayList<Disposition>();

    private List<Hobby> allHobbys = new ArrayList<Hobby>();
    private Set<String> hobbys;
    private Set<String> dispositions;

    private int dispositionTextCount = 0;
    private int hobbyTextCount = 0;

    private MultiplyDialog multiplyDialog;
    private HobbyAdapter hobbyAdapter;
    private DispositionAdapter dispositionAdapter;
    private boolean dispositionIsSuccess = false;
    private boolean hobbyIsSuccess = false;
    private int currentItem;// 1 特征 2 兴趣爱好

    private RadioDialog radioDialog;
    private User mUser;
    private Map<String, String> modifyParams = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfect_infomation);
        mHandler.register();
        setTitle("完善信息");
        tvRight.setText("保存");
        ivLeft.setVisibility(View.VISIBLE);
        tvRight.setVisibility(View.VISIBLE);
        if (UserController.getInstance().getUser() != null) {
            mUser = UserController.getInstance().getUser();
            initView();
            setListener();
            setData();
        } else {
            PromptManager.showCenterToast("加载用户信息失败,请重试");
        }
    }

    /**
     * 填充数据
     */
    private void setData() {
        if (TextUtils.isEmpty(mUser.getUser_birthday())) {
            birthDay.setText("请选择");
            age.setText("");
            constellation.setText("");
        } else {
            birthDay.setText(mUser.getUser_birthday());
            age.setText(ProductUserPropertyUtils.calculateAge(mUser.getUser_birthday()));
            constellation.setText(ProductUserPropertyUtils.calculateConstelation(mUser.getUser_birthday()));
        }

        nickName.setText(mUser.getUser_nick());

        userDuBai.setText(mUser.getUser_description());
        liveLocation.setText(dealWithValue(mUser.getUser_live()));
        jiGuan.setText(dealWithValue(mUser.getUser_navtie()));
        height.setText(dealWithValue(mUser.getUser_height()));
        weight.setText(dealWithValue(mUser.getUser_weight()));
        bloodType.setText(dealWithValue(mUser.getUser_blood()));

        education.setText(dealWithValue(mUser.getUser_education()));
        occupation.setText(dealWithValue(mUser.getUser_job()));
        income.setText(dealWithValue(mUser.getUser_income()));
        marriage.setText(dealWithValue(mUser.getUser_marriage()));
        house.setText(dealWithValue(mUser.getUser_housing()));
        yiDiLian.setText(dealWithValue(mUser.getUser_yidi()));

        hasChild.setText(dealWithValue(mUser.getUser_have_baby()));
        goodPlace.setText(dealWithValue(mUser.getUser_charm()));
        qinMiXingWei.setText(dealWithValue(mUser.getUser_premarital_sex()));

        hisLocation.setText(dealWithValue(mUser.getTa_province()));
        hisAge.setText(dealWithValue(mUser.getTa_age()));
        hisHeight.setText(dealWithValue(mUser.getTa_height()));
        hisEducation.setText(dealWithValue(mUser.getTa_education()));
        hisIncome.setText(dealWithValue(mUser.getTa_income()));

        dealDispositionHobby();
    }

    /**
     * 处理兴趣爱好 个性特征
     */
    private void dealDispositionHobby() {
        hobbys = new HashSet<String>();
        dispositions = new HashSet<String>();
        //处理个性特征及爱好
        if (!TextUtils.isEmpty(mUser.getUser_feature())) {
            llUserFeature.setVisibility(View.VISIBLE);
            String features = mUser.getUser_feature().substring(1, mUser.getUser_feature().length() - 1);
            String[] split = features.split(",");
            for (int i = 0; i < split.length; i++) {
                dispositions.add(split[i].trim());
            }
            fillDispotionItem();
        } else {
            llUserFeature.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(mUser.getUser_hobby())) {
            llUserHobby.setVisibility(View.VISIBLE);
            String hobbyStr = mUser.getUser_hobby().substring(1, mUser.getUser_hobby().length() - 1);
            String[] split = hobbyStr.split(",");
            for (int i = 0; i < split.length; i++) {
                hobbys.add(split[i].trim());
            }
            fillHobbyItem();
        } else {
            llUserHobby.setVisibility(View.GONE);
        }
    }

    private String dealWithValue(String value) {
        if (TextUtils.isEmpty(value) || value.equals("0")) {
            return "请选择";
        }
        return value;
    }

    private void initView() {
        userDuBai = (EditText) findViewById(R.id.userDuBai);
        llUserFeature = (LinearLayout) findViewById(R.id.llUserFeature);
        ibAddFeature = (ImageButton) findViewById(R.id.ibAddFeature);
        llUserHobby = (LinearLayout) findViewById(R.id.llUserHobby);
        ibAddHobby = (ImageButton) findViewById(R.id.ibAddHobby);
        llNickName = (LinearLayout) findViewById(R.id.llNickName);
        nickName = (TextView) findViewById(R.id.nickName);
        llBirthDay = (LinearLayout) findViewById(R.id.llBirthDay);
        birthDay = (TextView) findViewById(R.id.birthDay);
        llAge = (LinearLayout) findViewById(R.id.llAge);
        age = (TextView) findViewById(R.id.age);
        llConstellation = (LinearLayout) findViewById(R.id.llConstellation);
        constellation = (TextView) findViewById(R.id.constellation);
        llLiveLocation = (LinearLayout) findViewById(R.id.llLiveLocation);
        liveLocation = (TextView) findViewById(R.id.liveLocation);
        llJiGuan = (LinearLayout) findViewById(R.id.llJiGuan);
        jiGuan = (TextView) findViewById(R.id.jiGuan);
        llHeight = (LinearLayout) findViewById(R.id.llHeight);
        height = (TextView) findViewById(R.id.height);
        llWeight = (LinearLayout) findViewById(R.id.llWeight);
        weight = (TextView) findViewById(R.id.weight);
        llBloodType = (LinearLayout) findViewById(R.id.llBloodType);
        bloodType = (TextView) findViewById(R.id.bloodType);
        llEducation = (LinearLayout) findViewById(R.id.llEducation);
        education = (TextView) findViewById(R.id.education);
        llOccupation = (LinearLayout) findViewById(R.id.llOccupation);
        occupation = (TextView) findViewById(R.id.occupation);
        llIncome = (LinearLayout) findViewById(R.id.llIncome);
        income = (TextView) findViewById(R.id.income);
        llGoodPlace = (LinearLayout) findViewById(R.id.llGoodPlace);
        goodPlace = (TextView) findViewById(R.id.goodPlace);
        llMarriage = (LinearLayout) findViewById(R.id.llMarriage);
        marriage = (TextView) findViewById(R.id.marriage);
        llHouse = (LinearLayout) findViewById(R.id.llHouse);
        house = (TextView) findViewById(R.id.house);
        llYiDiLian = (LinearLayout) findViewById(R.id.llYiDiLian);
        yiDiLian = (TextView) findViewById(R.id.yiDiLian);
        llLoveOther = (LinearLayout) findViewById(R.id.llLoveOther);
        loveOther = (TextView) findViewById(R.id.loveOther);
        llQinMiXingWei = (LinearLayout) findViewById(R.id.llQinMiXingWei);
        qinMiXingWei = (TextView) findViewById(R.id.qinMiXingWei);
        llWithParent = (LinearLayout) findViewById(R.id.llWithParent);
        withParent = (TextView) findViewById(R.id.withParent);
        llHasChild = (LinearLayout) findViewById(R.id.llHasChild);
        hasChild = (TextView) findViewById(R.id.hasChild);
        userZhengyouInfo = (LinearLayout) findViewById(R.id.user_zhengyou_info);
        llHisLocation = (LinearLayout) findViewById(R.id.llHisLocation);
        hisLocation = (TextView) findViewById(R.id.hisLocation);
        llHisAge = (LinearLayout) findViewById(R.id.llHisAge);
        hisAge = (TextView) findViewById(R.id.hisAge);
        llHisHeight = (LinearLayout) findViewById(R.id.llHisHeight);
        hisHeight = (TextView) findViewById(R.id.hisHeight);
        llHisEducation = (LinearLayout) findViewById(R.id.llHisEducation);
        hisEducation = (TextView) findViewById(R.id.hisEducation);
        llHisIncome = (LinearLayout) findViewById(R.id.llHisIncome);
        hisIncome = (TextView) findViewById(R.id.hisIncome);
        save = (Button) findViewById(R.id.save);

        radioDialog = new RadioDialog(this);
    }

    private void setListener() {
        ibAddFeature.setOnClickListener(this);
        ibAddHobby.setOnClickListener(this);

        llNickName.setOnClickListener(this);
        llBirthDay.setOnClickListener(this);
        llAge.setOnClickListener(this);
        llConstellation.setOnClickListener(this);
        llLiveLocation.setOnClickListener(this);
        llJiGuan.setOnClickListener(this);
        llHeight.setOnClickListener(this);
        llWeight.setOnClickListener(this);
        llBloodType.setOnClickListener(this);

        llEducation.setOnClickListener(this);
        llOccupation.setOnClickListener(this);
        llIncome.setOnClickListener(this);
        llMarriage.setOnClickListener(this);
        llHouse.setOnClickListener(this);
        llYiDiLian.setOnClickListener(this);

        llWithParent.setOnClickListener(this);
        llGoodPlace.setOnClickListener(this);
        llLoveOther.setOnClickListener(this);
        llHasChild.setOnClickListener(this);
        llQinMiXingWei.setOnClickListener(this);

        llHisLocation.setOnClickListener(this);
        llHisAge.setOnClickListener(this);
        llHisHeight.setOnClickListener(this);
        llHisEducation.setOnClickListener(this);
        llHisIncome.setOnClickListener(this);

        save.setOnClickListener(this);

        ProvinceCityAreaUtils.loadArea(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibAddFeature://个性特征
                choiceCharacter();
                break;
            case R.id.ibAddHobby://兴趣爱好
                choiceHobby();
                break;
            case R.id.llNickName://昵称
                changeNickName();
                break;
            case R.id.llBirthDay://生日
                choiceBirthDay();
                break;
            case R.id.llLiveLocation://居住地
                choiceLocation(UserInfoConfig.TYPE_LIVE_LOCATION);
                break;
            case R.id.llJiGuan://籍贯
                choiceLocation(UserInfoConfig.TYPE_JIGUAN);
                break;
            case R.id.llHeight://身高
                choiceRadioItem(UserInfoConfig.TYPE_HEIGHT);
                break;
            case R.id.llWeight://体重
                choiceRadioItem(UserInfoConfig.TYPE_WEIGHT);
                break;
            case R.id.llBloodType://血型
                choiceRadioItem(UserInfoConfig.TYPE_BLOOD_TYPE);
                break;
            case R.id.llEducation://学历
                choiceRadioItem(UserInfoConfig.TYPE_EDUCATION);
                break;
            case R.id.llOccupation://职业
                choiceRadioItem(UserInfoConfig.TYPE_OCCUPATION);
                break;
            case R.id.llIncome://收入
                choiceRadioItem(UserInfoConfig.TYPE_INCOME);
                break;
            case R.id.llMarriage://婚姻状况
                choiceRadioItem(UserInfoConfig.TYPE_MARRIAGE);
                break;
            case R.id.llHouse://房
                choiceRadioItem(UserInfoConfig.TYPE_HOUSE);
                break;
            case R.id.llYiDiLian://异地恋
                choiceRadioItem(UserInfoConfig.TYPE_YIDILIAN);
                break;
            case R.id.llWithParent:
                choiceRadioItem(UserInfoConfig.TYPE_WITH_PARENT);
                break;
            case R.id.llQinMiXingWei:
                choiceRadioItem(UserInfoConfig.TYPE_QIN_MI);
                break;
            case R.id.llGoodPlace:
                choiceRadioItem(UserInfoConfig.TYPE_GOOD_PLACE);
                break;
            case R.id.llLoveOther:
                choiceRadioItem(UserInfoConfig.TYPE_LOVE_OTHER);
                break;
            case R.id.llHasChild:
                choiceRadioItem(UserInfoConfig.TYPE_HAS_CHILDREN);
                break;
            case R.id.llHisLocation:
                choiceLocation(UserInfoConfig.TYPE_HIS_LOCATION);
                break;
            case R.id.llHisAge:
                choiceIntervalItem(UserInfoConfig.TYPE_HIS_AGE);
                break;
            case R.id.llHisHeight:
                choiceIntervalItem(UserInfoConfig.TYPE_HIS_HEIGHT);
                break;
            case R.id.llHisEducation:
                choiceRadioItem(UserInfoConfig.TYPE_HIS_EDUCATION);
                break;
            case R.id.llHisIncome:
                choiceRadioItem(UserInfoConfig.TYPE_HIS_INCOME);
                break;
            case R.id.save://保存
                onRightButtonClick();
                break;
        }
        super.onClick(v);
    }

    /**
     * 选择区间范围
     *
     * @param type
     */
    private void choiceIntervalItem(final int type) {
        final IntervalSelectionDialog intervalSelectionDialog = new IntervalSelectionDialog(this, type);
        intervalSelectionDialog.setButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = intervalSelectionDialog.getValue();
                String[] split = value.split(",");
                if (Integer.parseInt(split[0]) >= Integer.parseInt(split[1])) {
                    PromptManager.showCenterToast("请选择有效的区间范围");
                    return;
                }
                if (type == UserInfoConfig.TYPE_HIS_AGE) {
                    hisAge.setText(split[0] + "-" + split[1]);
                    modifyParams.put("ta_age", value);
                } else if (type == UserInfoConfig.TYPE_HIS_HEIGHT) {
                    hisHeight.setText(split[0] + "-" + split[1]);
                    modifyParams.put("ta_height", value);
                }
                intervalSelectionDialog.dismiss();
            }
        });
        intervalSelectionDialog.show();
    }

    @Override
    protected void onRightButtonClick() {
        JSONObject jsonObject = new JSONObject(UserInfoConfig.USERINFO_VALUE);
        modifyParams.put("userFlag", jsonObject.toString());
        if (!TextUtils.isEmpty(userDuBai.getText().toString().trim())) {
            modifyParams.put("description", userDuBai.getText().toString().trim());
        }
        showProgressDialog("请稍等");
        mUserController.updateUserInfo(modifyParams);
    }

    /**
     * 选择出生日期
     */
    private void choiceBirthDay() {
        final ChoiceDateDialog choiceDateDialog = new ChoiceDateDialog(this);
        choiceDateDialog.setButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = choiceDateDialog.getValue();
                modifyParams.put("birthday", value);
                birthDay.setText(value);
                age.setText(ProductUserPropertyUtils.calculateAge(value));
                constellation.setText(ProductUserPropertyUtils.calculateConstelation(value));
                choiceDateDialog.dismiss();
            }
        });
        choiceDateDialog.show();
    }


    /**
     * 选择居住地
     *
     * @param type 1籍贯 2居住地
     */
    private void choiceLocation(final int type) {
        final ChoiceCityDialog choiceCityDialog = new ChoiceCityDialog(this, type);
        choiceCityDialog.setButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = choiceCityDialog.getValue();
                String[] split = value.split(",");
                if (type == UserInfoConfig.TYPE_JIGUAN) {
                    jiGuan.setText(split[0] + split[1]);
                    modifyParams.put("native_province", split[0]);
                    modifyParams.put("native_city", split[1]);
                } else if (type == UserInfoConfig.TYPE_LIVE_LOCATION) {
                    liveLocation.setText(split[0] + split[1] + split[2]);
                    modifyParams.put("province", split[0]);
                    modifyParams.put("city", split[1]);
                    modifyParams.put("district", split[2]);
                } else if (type == UserInfoConfig.TYPE_HIS_LOCATION) {
                    hisLocation.setText(split[0]);
                    modifyParams.put("ta_province", split[0]);
                }
                choiceCityDialog.dismiss();
            }
        });
        choiceCityDialog.show();
    }

    /**
     * 单选框
     *
     * @param type 1 身高 2 体重  3 血型 。。。。。
     */
    private void choiceRadioItem(final int type) {
        String[] data = null;
        radioDialog.setType(type);
        if (type == UserInfoConfig.TYPE_HEIGHT) {
            data = ProductUserPropertyUtils.getHeightValue();
        } else if (type == UserInfoConfig.TYPE_WEIGHT) {
            data = ProductUserPropertyUtils.getWeightValue();
        } else if (type == UserInfoConfig.TYPE_BLOOD_TYPE) {
            data = ProductUserPropertyUtils.getBloodTypeValue();
        } else if (type == UserInfoConfig.TYPE_EDUCATION || type == UserInfoConfig.TYPE_HIS_EDUCATION) {
            data = ProductUserPropertyUtils.getEducation();
        } else if (type == UserInfoConfig.TYPE_OCCUPATION) {
            data = ProductUserPropertyUtils.getJobs();
        } else if (type == UserInfoConfig.TYPE_INCOME || type == UserInfoConfig.TYPE_HIS_INCOME) {
            data = ProductUserPropertyUtils.getIncome();
        } else if (type == UserInfoConfig.TYPE_MARRIAGE) {
            data = ProductUserPropertyUtils.getMarriages();
        } else if (type == UserInfoConfig.TYPE_HOUSE) {
            data = ProductUserPropertyUtils.getHousing();
        } else if (type == UserInfoConfig.TYPE_YIDILIAN) {
            data = ProductUserPropertyUtils.getYidi();
        } else if (type == UserInfoConfig.TYPE_GOOD_PLACE) {
            data = ProductUserPropertyUtils.getGoodPlace();
        } else if (type == UserInfoConfig.TYPE_WITH_PARENT) {
            data = ProductUserPropertyUtils.getWithParent();
        } else if (type == UserInfoConfig.TYPE_LOVE_OTHER) {
            data = ProductUserPropertyUtils.getLoveOther();
        } else if (type == UserInfoConfig.TYPE_HAS_CHILDREN) {
            data = ProductUserPropertyUtils.getHasChildren();
        } else if (type == UserInfoConfig.TYPE_QIN_MI) {
            data = ProductUserPropertyUtils.getQinMi();
        }
        radioDialog.setData(data);
        radioDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = radioDialog.getValue();
                if (type == UserInfoConfig.TYPE_HEIGHT) {
                    height.setText(value);
                    modifyParams.put("height", value);
                } else if (type == UserInfoConfig.TYPE_WEIGHT) {
                    weight.setText(value);
                    modifyParams.put("weight", value);
                } else if (type == UserInfoConfig.TYPE_BLOOD_TYPE) {
                    bloodType.setText(value);
                    modifyParams.put("blood", value);
                } else if (type == UserInfoConfig.TYPE_EDUCATION) {
                    education.setText(value);
                    modifyParams.put("education", value);
                } else if (type == UserInfoConfig.TYPE_OCCUPATION) {
                    occupation.setText(value);
                    modifyParams.put("job", value);
                } else if (type == UserInfoConfig.TYPE_INCOME) {
                    income.setText(value);
                    modifyParams.put("income", value);
                } else if (type == UserInfoConfig.TYPE_MARRIAGE) {
                    marriage.setText(value);
                    modifyParams.put("marriage", value);
                } else if (type == UserInfoConfig.TYPE_HOUSE) {
                    house.setText(value);
                    modifyParams.put("housing", value);
                } else if (type == UserInfoConfig.TYPE_YIDILIAN) {
                    yiDiLian.setText(value);
                    modifyParams.put("yidi", value);
                } else if (type == UserInfoConfig.TYPE_HIS_EDUCATION) {
                    hisEducation.setText(value);
                    modifyParams.put("ta_education", value);
                } else if (type == UserInfoConfig.TYPE_HIS_INCOME) {
                    hisIncome.setText(value);
                    modifyParams.put("ta_income", value);
                } else if (type == UserInfoConfig.TYPE_GOOD_PLACE) {
                    modifyParams.put("charm", value);
                    goodPlace.setText(value);
                } else if (type == UserInfoConfig.TYPE_LOVE_OTHER) {
                    loveOther.setText(value);
                }/*else if (type == UserInfoConfig.TYPE_WITH_PARENT) {
                    withParent.setText(value);
                }*/ else if (type == UserInfoConfig.TYPE_HAS_CHILDREN) {
                    hasChild.setText(value);
                    modifyParams.put("have_baby", value);
                } else if (type == UserInfoConfig.TYPE_QIN_MI) {
                    qinMiXingWei.setText(value);
                    modifyParams.put("premarital_sex", value);
                }
                radioDialog.dismiss();
            }
        });
        radioDialog.show();
    }

    /**
     * 修改昵称
     */
    private void changeNickName() {
        final ChangeNickDialog changeNickDialog = new ChangeNickDialog(this);
        changeNickDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newNick = changeNickDialog.getValue();
                if (TextUtils.isEmpty(newNick) || newNick.length() > 8) {
                    PromptManager.showCenterToast("请输入合法的昵称");
                    return;
                }
                nickName.setText(newNick);
                modifyParams.put("nick", newNick);
                changeNickDialog.dismiss();
            }
        });
        changeNickDialog.show();
    }

    /**
     * 选取特征
     */
    private void choiceCharacter() {
        dispositionTextCount = 0;
        dispositionIsSuccess = false;
        currentItem = 1;
        if (allDisposition == null || allDisposition.isEmpty()) {
            allDisposition = ProductUserPropertyUtils.getDispositionData();
        }
        if (multiplyDialog == null) {
            multiplyDialog = new MultiplyDialog(this);
        }
        dispositionAdapter = new DispositionAdapter(this, allDisposition);
        multiplyDialog.setTitle("个性选择");
        multiplyDialog.setGridViewAdapter(dispositionAdapter);
        multiplyDialog.setOnItemClickListener(onItemClickListener);
        multiplyDialog.btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiplyDialog.dismiss();
                fillDispotionItem();
            }
        });
        multiplyDialog.show();
    }

    /**
     * 选取爱好
     */
    private void choiceHobby() {
        hobbyTextCount = 0;
        hobbyIsSuccess = false;
        currentItem = 2;
        if (allHobbys == null || allHobbys.isEmpty()) {
            allHobbys = ProductUserPropertyUtils.getHobbyData();
        }
        if (multiplyDialog == null) {
            multiplyDialog = new MultiplyDialog(this);
        }
        hobbyAdapter = new HobbyAdapter(this, allHobbys);
        multiplyDialog.setTitle("兴趣爱好");
        multiplyDialog.setGridViewAdapter(hobbyAdapter);
        multiplyDialog.setOnItemClickListener(onItemClickListener);
        multiplyDialog.btnSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiplyDialog.dismiss();
                fillHobbyItem();
            }
        });
        multiplyDialog.show();
    }


    /**
     * 填充个性特征信息
     */
    private void fillDispotionItem() {
        dispositionTextCount = 0;
        LinearLayout view = null;
        if (dispositions != null && !dispositions.isEmpty()) {
            llUserFeature.setVisibility(View.VISIBLE);
            llUserFeature.removeAllViews();
            for (String str : dispositions) {
                dispositionTextCount += str.length();
                if (dispositionTextCount < 8) {
                    view = createTextView(str);
                    addDispositionView(view);
                } else if (!dispositionIsSuccess) {
                    dispositionIsSuccess = true;
                    view = createImageView();
                    addDispositionView(view);
                }
            }
            modifyParams.put("feature", dispositions.toString());
        } else {
            llUserFeature.setVisibility(View.GONE);
            dispositionIsSuccess = false;
            llUserFeature.removeAllViews();
            modifyParams.put("feature", "");
        }
    }

    /**
     * 填充兴趣爱好信息
     */
    private void fillHobbyItem() {
        hobbyTextCount = 0;
        LinearLayout view = null;
        if (hobbys != null && !hobbys.isEmpty()) {
            llUserHobby.setVisibility(View.VISIBLE);
            llUserHobby.removeAllViews();
            for (String str : hobbys) {
                hobbyTextCount += str.length();
                if (hobbyTextCount < 8) {
                    view = createTextView(str);
                    addHobbyView(view);
                } else if (!hobbyIsSuccess) {
                    hobbyIsSuccess = true;
                    view = createImageView();
                    addHobbyView(view);
                }
            }
            modifyParams.put("hobby", hobbys.toString());
        } else {
            llUserHobby.setVisibility(View.GONE);
            hobbyIsSuccess = false;
            llUserHobby.removeAllViews();
            modifyParams.put("hobby", "");
        }
    }

    private void addDispositionView(LinearLayout view) {
        if (layoutParams == null) {
            getLayoutParams();
        }
        view.setLayoutParams(layoutParams);
        llUserFeature.setGravity(Gravity.CENTER_VERTICAL);
        llUserFeature.addView(view);
    }

    private void addHobbyView(LinearLayout view) {
        if (layoutParams == null) {
            getLayoutParams();
        }
        view.setLayoutParams(layoutParams);
        llUserHobby.setGravity(Gravity.CENTER_VERTICAL);
        llUserHobby.addView(view);
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Disposition disposition = null;
            Hobby hobby = null;
            if (currentItem == 1) {
                disposition = allDisposition.get(position);
            } else {
                hobby = allHobbys.get(position);
            }
            TextView textView = (TextView) view;
            if (currentItem == 1) {
                if (disposition.getType() == 1) {
                    dispositions.remove(disposition.getDescription());
                    disposition.setType(0);
                    view.setBackgroundResource(R.drawable.bg_md_item_nor);
                    textView.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    dispositions.add(disposition.getDescription());
                    disposition.setType(1);
                    view.setBackgroundResource(R.drawable.bg_md_item_pre);
                    textView.setTextColor(getResources().getColor(R.color.white));
                }
            } else {
                if (hobby.getType() == 1) {
                    hobbys.remove(hobby.getDescription());
                    hobby.setType(0);
                    view.setBackgroundResource(R.drawable.bg_md_item_nor);
                    textView.setTextColor(getResources().getColor(R.color.gray));
                } else {
                    hobbys.add(hobby.getDescription());
                    hobby.setType(1);
                    view.setBackgroundResource(R.drawable.bg_md_item_pre);
                    textView.setTextColor(getResources().getColor(R.color.white));
                }
            }
        }
    };


    private LinearLayout.LayoutParams getLayoutParams() {
        if (layoutParams == null) {
            layoutParams = new LinearLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.leftMargin = 10;
        }
        return layoutParams;
    }

    private LinearLayout createImageView() {
        LinearLayout view = (LinearLayout) View.inflate(this, R.layout.ulove_hobby_more, null);
        return view;
    }

    public LinearLayout createTextView(String text) {
        LinearLayout view = (LinearLayout) View.inflate(this, R.layout.ulove_hobby_tv, null);
        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setText(text);
        return view;
    }

    private class UpdateUserInfoHandler extends EventHandler {
        public void onEvent(UpdateUserInfoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                mUserController.loadUserInfo();
                PromptManager.showCenterToast("保存成功");
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, PerfectInformationActivity.class);
        return intent;
    }

}
