package com.Aiputt.taohuayun.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.app.ULoveApplication;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.controller.LoginRegisterController;
import com.Aiputt.taohuayun.event.LoginEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMGroupManager;

public class LoginActivity extends BaseActivity{
	
	private EditText etId;
	private EditText etPwd;
	private TextView tvRegist;
	private TextView tvLogin;
	private RegisterHandler mHandler = new RegisterHandler();
	private LoginRegisterController mController = LoginRegisterController
			.getInstance();
	private boolean fromSet;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		setTitle("登录");
		
		etId = (EditText)findViewById(R.id.etId);
		etPwd = (EditText)findViewById(R.id.etPwd);
		tvRegist = (TextView)findViewById(R.id.tvRegist);
		tvLogin = (TextView)findViewById(R.id.tvLogin);
		etId.setText(SharePCach.loadStringCach(SharePCach.USER_NO));
		etPwd.setText(SharePCach.loadStringCach(SharePCach.USER_PWD));
		mHandler.register();
		tvRegist.setOnClickListener(this);
		tvLogin.setOnClickListener(this);
		
		fromSet = getIntent().getBooleanExtra("fromSet", false);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.unregister();
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.tvRegist:
			actionRegister();
			break;
		case R.id.tvLogin:
			actionLogin();
			break;

		}
	}
	
	private void actionLogin(){
		String userNo = etId.getText().toString().trim();
		String userPwd = etPwd.getText().toString().trim();
		if (TextUtils.isEmpty(userNo)||TextUtils.isEmpty(userPwd)) {
			PromptManager.showCenterToast("请完成填写");
		}else {
			showProgressDialog(R.string.pl_wait);
			mController.actionLogin(userNo, userPwd);
		}
	}
	
	private void actionRegister(){
		if (fromSet) {
			Intent intent = new Intent(this,SexActivity.class);
			startActivity(intent);
			finish();
		}else {
			finish();
		}
	}
	
	class RegisterHandler extends EventHandler {
		public void onEvent(LoginEvent event) {
			dismissProgressDialog();
			if (event.code == 0) {
				SharePCach.saveStringCach(SharePCach.TOKEN, event.data.getToken());
				SharePCach.saveStringCach(SharePCach.USER_NO, etId.getText().toString().trim());
				SharePCach.saveStringCach(SharePCach.USER_PWD, etPwd.getText().toString().trim());
				SharePCach.saveStringCach(SharePCach.HPWD, event.data.getEasemod_password());
				login();
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}
	}
	
	
	
	/**
	 * 登录
	 * 
	 */
	public void login() {

		final long start = System.currentTimeMillis();
		// 调用sdk登陆方法登陆聊天服务器
		EMChatManager.getInstance().login(
				SharePCach.loadStringCach(SharePCach.USER_NO),
				SharePCach.loadStringCach(SharePCach.HPWD), new EMCallBack() {

					@Override
					public void onSuccess() {
						// 登陆成功，保存用户名密码
						Log.e("LoginActivity",
								"Login success");
						ULoveApplication.mApp.setUserName(
								SharePCach.loadStringCach(SharePCach.USER_NO));
						ULoveApplication.mApp.setPassword(
								SharePCach.loadStringCach(SharePCach.HPWD));

						try {
							// ** 第一次登录或者之前logout后再登录，加载所有本地群和回话
							// ** manually load all local groups and
							EMGroupManager.getInstance().loadAllGroups();
							EMChatManager.getInstance().loadAllConversations();
							// 处理好友和群组
//							initializeContacts();
						} catch (Exception e) {
							e.printStackTrace();
							// 取好友或者群聊失败，不让进入主页面
							runOnUiThread(new Runnable() {
								public void run() {
									ULoveApplication.mApp.logout(null);
									Toast.makeText(getApplicationContext(),
											R.string.login_failure_failed, Toast.LENGTH_SHORT)
											.show();
								}
							});
							return;
						}
						// 更新当前用户的nickname 此方法的作用是在ios离线推送时能够显示用户nick
						boolean updatenick = EMChatManager
								.getInstance()
								.updateCurrentUserNick(
										ULoveApplication.currentUserNick.trim());
						if (!updatenick) {
							Log.e("LoginActivity",
									"update current user nick fail");
						}
						nextPage();

						finish();
					}

					@Override
					public void onProgress(int progress, String status) {
					}

					@Override
					public void onError(final int code, final String message) {
						nextPage();
						runOnUiThread(new Runnable() {
							public void run() {
								Toast.makeText(
										getApplicationContext(),
										getString(R.string.Login_failed)
												+ message, Toast.LENGTH_SHORT)
										.show();
							}
						});
					}
				});
	}
	
	private void nextPage(){
		SharePCach.saveBooleanCach("index", true);
		SharePCach.saveBooleanCach("fromLogin", true);
		Intent intent = new Intent(LoginActivity.this,
				MainActivity.class);
		startActivity(intent);
		finish();
	}


}
