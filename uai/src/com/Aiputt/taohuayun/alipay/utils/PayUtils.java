package com.Aiputt.taohuayun.alipay.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Administrator on 2015/8/5.
 */
public class PayUtils {

    //商户PID
    private static final String PARTNER = "2088511940258713";
    //商户收款账号
    private static final String SELLER = "7442860@qq.com";
    //商户私钥，pkcs8格式
    private static final String RSA_PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALNMwLgbLkhMm1chtUB6GNYzLUxLN2fx1JKgsxB3nKPJzix08tknuniaEP+sfB9McNRUl98SXgZLsyNOaBz9e9JkguA7xNaL2eXa/uSsUvjtsshavogZKY9dlZesS7cWPGKuY01z4o/rU1p0sf8PebKb5c1vGJFdCdT7m9VHij+JAgMBAAECgYAVKiI1mT/FWQWdK2VxzvfPkQrvzpEju/ATvtRkRTpAOzoTFVr1NzNTuRv4kiu0phlLdxDWTBy9MYJb0I2JnoJXCJ7wQDbGNJAjGJmkgEKdRidmrYwZo02s/wKn7Dl20q1qXpAGs8Dwe7bbTv4IAtkUxPQwZBXqRNCp5Gi4QuW9gQJBAO0qfqVEQkrtK5kD2Na73yKfOiUFeEOFLiRgcBLKolWbivbo0lZ4Tpwx1hO9/2hViE6yVdvm8OWL/Ow234PpVoUCQQDBiduozSPlHbcOdwnrECq2AexKz1Ogc+xR1vGhZoGvz0Tr0SPkPm70WSThTk6D5Suwtc4ieOU1NeDkr3YKpN41AkBQF221ehu5GecDQZ1UxxVPp94G6pX2l19BwZ+XeP7hUinXwEBoGzDQ5rOY7yz1mwzxJqCAJrlri57aVp2c+ovRAkBqQ9doB7r9wiMQviuj9Wx+IvAq40t3fi7zgKGLjU0HyzZ8hIjAecdSQuANFYHmyZ4DmM9l/htb7Alfhze5AG1BAkEAqqyfqK9SubJCXbfq8zzNQ5HBPS3LSKcKwrKPcxb12QzYAW3oshz6iijIKoRECa0gBy0V9JuMut6aEnFyVmzd6Q==";
    //支付宝公钥
    private static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";

    private static final int SDK_PAY_FLAG = 1;

    private static final int SDK_CHECK_FLAG = 2;

    /**
     * create the order info. 创建订单信息
     *
     */
    public static String getOrderInfo(String subject, String body, String price,String orderNo,String callBackUrl) {
        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + orderNo + "\"";
//        orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + price + "\"";

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + callBackUrl
                + "\"";
//        orderInfo += "&notify_url=" + "\"" + "http://notify.msp.hk/notify.htm"
//                + "\"";

        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";

        // extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
        // orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";

        // 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
        // orderInfo += "&paymethod=\"expressGateway\"";

        return orderInfo;
    }

    /**
     * get the out_trade_no for an order. 生成商户订单号，该值在商户端应保持唯一（可自定义格式规范）
     *
     */
    public static String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
                Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);

        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content
     *            待签名订单信息
     */
    public static String sign(String content) {
        return SignUtils.sign(content, RSA_PRIVATE);
    }

    /**
     * get the sign type we use. 获取签名方式
     *
     */
    public static String getSignType() {
        return "sign_type=\"RSA\"";
    }

}
