package com.Aiputt.taohuayun.app;

import android.content.Context;
import android.util.Log;

import com.Aiputt.taohuayun.chat.DemoHXSDKHelper;
import com.Aiputt.taohuayun.chat.domain.User;
import com.Aiputt.taohuayun.event.LocationEvent;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.utils.CommonUtil;
import com.Aiputt.taohuayun.utils.DeviceIdUtil;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.easemob.EMCallBack;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.umeng.analytics.MobclickAgent;

import org.litepal.LitePalApplication;

import java.util.Map;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class ULoveApplication extends LitePalApplication {

    private static final String TAG = "ULoveApplication";

    public static ULoveApplication mApp;// 全局的application
    public LocationClient mLocationClient;
    private MyLocationListener mLocationListener;
    public BDLocation aBDLocation;

  public final String PREF_USERNAME = "username";
  public static Context applicationContext;

	/**
	 * 当前用户nickname,为了苹果推送不是userid而是昵称
	 */
	public static String currentUserNick = "";
	public static DemoHXSDKHelper hxSDKHelper = new DemoHXSDKHelper();

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        applicationContext = this;
        CommonUtil.init(this);
        PromptManager.init(this);
        initJPush();
        mLocationClient = new LocationClient(getApplicationContext());
        mLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mLocationListener);
        MobclickAgent.openActivityDurationTrack(false);
        initImageLoader(getContext());
        hxSDKHelper.onInit(applicationContext);

    }

    private void initJPush() {
		JPushInterface.setDebugMode(true); // 设置开启日志,发布时请关闭日志
		JPushInterface.init(this); // 初始化 JPush
		String deviceId = DeviceIdUtil.getDeviceId(this);
		JPushInterface.setAlias(this, deviceId, new TagAliasCallback() {
			@Override
			public void gotResult(int arg0, String arg1, Set<String> arg2) {
				Log.d("TAG", "gotResult:" + arg0 + "," + arg1);
			}
		});
	}


    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .threadPoolSize(3)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new WeakMemoryCache())
                .memoryCache(new LruMemoryCache(5 * 1024 * 1024))
                .memoryCacheSize(10 * 1024 * 1024)
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .discCacheFileCount(100) //缓存的文件数量
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();
        ImageLoader.getInstance().init(config);
    }

    public LocationClient getLocationClient() {
        return mLocationClient;
    }

    /**
     * 实现实时位置回调监听
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            BDLocation mBDLocation = null;
            //Receive Location
            if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                Log.i(TAG, "gps定位成功");
                mBDLocation = location;
            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                Log.i(TAG, "网络定位成功");
                mBDLocation = location;
            } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                Log.i(TAG, "离线定位成功");
                mBDLocation = location;
            } else if (location.getLocType() == BDLocation.TypeServerError) {
                Log.i(TAG, "服务端网络定位失败");
            } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                Log.i(TAG, "网络不通导致定位失败，请检查网络是否通畅");
            } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                Log.i(TAG, "无法获取有效定位依据导致定位失败");
            }
            LocationEvent event = new LocationEvent();
            event.mBDLocation = mBDLocation;
            aBDLocation = mBDLocation;
            EventCenter.getInstance().send(event);
        }


    }

    /**
	 * 获取内存中好友user list
	 *
	 * @return
	 */
	public Map<String, User> getContactList() {
	    return hxSDKHelper.getContactList();
	}

	/**
	 * 设置好友user list到内存中
	 *
	 * @param contactList
	 */
	public void setContactList(Map<String, User> contactList) {
	    hxSDKHelper.setContactList(contactList);
	}

	/**
	 * 获取当前登陆用户名
	 *
	 * @return
	 */
	public String getUserName() {
	    return hxSDKHelper.getHXId();
	}

	/**
	 * 获取密码
	 *
	 * @return
	 */
	public String getPassword() {
		return hxSDKHelper.getPassword();
	}

	/**
	 * 设置用户名
	 *
	 * @param
	 */
	public void setUserName(String username) {
	    hxSDKHelper.setHXId(username);
	}

	/**
	 * 设置密码 下面的实例代码 只是demo，实际的应用中需要加password 加密后存入 preference 环信sdk
	 * 内部的自动登录需要的密码，已经加密存储了
	 *
	 * @param pwd
	 */
	public void setPassword(String pwd) {
	    hxSDKHelper.setPassword(pwd);
	}

	/**
	 * 退出登录,清空数据
	 */
	public void logout(final EMCallBack emCallBack) {
		// 先调用sdk logout，在清理app中自己的数据
	    hxSDKHelper.logout(emCallBack);
	}
}
