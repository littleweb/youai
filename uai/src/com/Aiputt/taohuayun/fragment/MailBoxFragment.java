package com.Aiputt.taohuayun.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.jpush.android.data.p;

import com.Aiputt.taohuayun.R;

public class MailBoxFragment extends BaseFragment {

    private ViewPager pager;
    private MyPagerAdapter adapter;
    private TextView tvNotify;
    private LinearLayout llPrivateMail;
    private TextView tvPrivateMail;
    private TextView tvPrivateMailJiaobiao;
    private View vNotice;
    private View vMail;
    private PrivateMailFragment mfragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_mailbox, null);

        pager = (ViewPager) view.findViewById(R.id.pager);
        llPrivateMail = (LinearLayout) view.findViewById(R.id.llPrivateMail);
        tvNotify = (TextView) view.findViewById(R.id.tvNotify);
        tvPrivateMailJiaobiao = (TextView) view.findViewById(R.id.tvPrivateMailJiaobiao);
        tvPrivateMail = (TextView) view.findViewById(R.id.tvPrivateMail);
        vNotice = (View) view.findViewById(R.id.vNotice);
        vMail = (View) view.findViewById(R.id.vMail);
        vMail.setBackgroundColor(0xffe85683);
		vNotice.setBackgroundColor(0xffdddddd);

        adapter = new MyPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(1);
        tvNotify.setOnClickListener(this);
        llPrivateMail.setOnClickListener(this);
        pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				if (arg0==0) {
					vNotice.setBackgroundColor(0xffe85683);
					vMail.setBackgroundColor(0xffdddddd);
				}else if (arg0==1) {
					vMail.setBackgroundColor(0xffe85683);
					vNotice.setBackgroundColor(0xffdddddd);
				}
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        return view;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tvNotify:
                pager.setCurrentItem(0);
                break;
            case R.id.llPrivateMail:
                pager.setCurrentItem(1);
                break;
           
        }
    }
    
    public void updateData(){
    	if (mfragment!=null) {
			mfragment.updateData();
		}
    }


    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"系统通知", "私信"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new NotifyFragment();
                    break;
                case 1:
                    fragment = new PrivateMailFragment();
                    if (fragment instanceof PrivateMailFragment) {
                    	mfragment = (PrivateMailFragment)fragment;
					}
                    break;
//                case 2:
//                    fragment = new ContactFragment();
//                    break;
            }
            return fragment;
        }

    }

}
