package com.Aiputt.taohuayun.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class BaseFragment extends Fragment implements OnClickListener {
    protected Context mContext;
    protected ImageLoader mImageLoader = ImageLoader.getInstance();

    private String mPageName;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mPageName = this.getClass().getName();
        mContext = activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(mPageName);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(mPageName);
    }

    private static Dialog dialog;
    private static ProgressBar pb;

    /**
     * 隐藏loading对话框
     */
    public static void dismissProgressDialog() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public TextView txt;
    public ImageView delete_img;

    public void showProgressDialog(String msg) {
        showProgressDialog(msg, true);
    }

    public void showProgressDialog(int resID) {

        showProgressDialog(getString(resID), true);
    }

    /**
     * loading对话框 显示一个半透明的的loading对话框
     *
     * @param message 要显示的文字的资源
     */
    public void showProgressDialog(String message, boolean cancelable) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new Dialog(getActivity(), R.style.processDialog);
        View view = LayoutInflater.from(getActivity()).inflate(
                R.layout.loading_dialog_layout, null);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.addContentView(view, params);
        pb = (ProgressBar) view.findViewById(R.id.loading_dialog_progressBar);
        delete_img = (ImageView) view.findViewById(R.id.delete_img);
        txt = (TextView) view.findViewById(R.id.loading_message);
        txt.setText(message);
        txt.setTextSize(15);
        dialog.setCancelable(cancelable);
        Window window = dialog.getWindow();
        window.setWindowAnimations(R.style.my_dialog);
        delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissProgressDialog();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }
}
