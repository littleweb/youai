package com.Aiputt.taohuayun.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.activity.OtherPersonSpaceActivity;
import com.Aiputt.taohuayun.activity.PerfectInformationActivity;
import com.Aiputt.taohuayun.activity.PreviewPhotoActivity;
import com.Aiputt.taohuayun.activity.WebViewActivity;
import com.Aiputt.taohuayun.adapter.LookMeAdapter;
import com.Aiputt.taohuayun.adapter.UserDeatailAdapter;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.BuyVipEvent;
import com.Aiputt.taohuayun.event.ModifyHeadPhotoEvent;
import com.Aiputt.taohuayun.event.NotifyUserPhotoChangeEvent;
import com.Aiputt.taohuayun.event.UpdateUserInfoEvent;
import com.Aiputt.taohuayun.event.UploadHeadPhotoEvent;
import com.Aiputt.taohuayun.event.UploadNormalPhotoEvent;
import com.Aiputt.taohuayun.event.UserInfoEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.CommonUtil;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.utils.PictureUtil;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.PhotoUpDialog;
import com.Aiputt.taohuayun.widgets.RoundedImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class MineFragment extends BaseFragment implements View.OnClickListener, UserDeatailAdapter.OnItemClickListener, LookMeAdapter.OnRecentVisitItemClickListener {

    private static final int CHOICE_FROM_CAMERA = 0;
    private static final int CHOICE_FROM_GALLARY = 1;

    private UserController mUserController = UserController.getInstance();
    private MineHandler mHandler = new MineHandler();

    private User mUser;

    private String imageFile = "";

    private RoundedImageView userHeadPhoto;
    private RelativeLayout userInfoPerfect;

    private TextView tvReview;

    private TextView tvName;
    private TextView tvID;
    private TextView tvAge;
    private TextView tvLocation;
    private TextView tvDisposition;
    private TextView tvHobby;
    private LinearLayout llFeatureHobby;

    private RelativeLayout userBuyLovemoney;
    private RelativeLayout userBuyVip;
    private TextView buyLoveMoneyNow;
    private TextView buyVipNow;
    private TextView loveMoenyTitle;
    private TextView vipTitle;
    private TextView userScore;
    private TextView mVip;
    private TextView mCompleteInfo;
    private TextView whoLookMe;

    private boolean isHeadPhoto;
    private RecyclerView userPhoto;
    private UserDeatailAdapter adapter;
    private LookMeAdapter lookMeAdapter;
    private File mImageFile = null;
    private ArrayList<ImageList> mUserImg_list = new ArrayList<ImageList>();
    private RecyclerView rvLookMe;
    private RelativeLayout rlRecentVisit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgressDialog("加载中");
        mUserController.loadUserInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_mine, null);
        mHandler.register();
        initView(view);
        setListener();
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view
     */
    private void initView(View view) {
        whoLookMe = (TextView) view.findViewById(R.id.whoLookMe);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvID = (TextView) view.findViewById(R.id.tvID);
        tvAge = (TextView) view.findViewById(R.id.tvAge);
        tvLocation = (TextView) view.findViewById(R.id.tvLocation);
        tvDisposition = (TextView) view.findViewById(R.id.tvDisposition);
        tvHobby = (TextView) view.findViewById(R.id.tvHobby);
        llFeatureHobby = (LinearLayout) view.findViewById(R.id.llFeatureHobby);

        userPhoto = (RecyclerView) view.findViewById(R.id.userPhoto);
        adapter = new UserDeatailAdapter(getActivity(), true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        userPhoto.setLayoutManager(layoutManager);
        adapter.setOnItemClickListener(this);
        userPhoto.setAdapter(adapter);

        lookMeAdapter = new LookMeAdapter(getActivity());
        rvLookMe = (RecyclerView) view.findViewById(R.id.rvLookMe);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        layoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvLookMe.setLayoutManager(layoutManager2);
        rvLookMe.setAdapter(lookMeAdapter);

        rlRecentVisit = (RelativeLayout) view.findViewById(R.id.rlRecentVisit);

        userHeadPhoto = (RoundedImageView) view.findViewById(R.id.userHeadPhoto);
        userInfoPerfect = (RelativeLayout) view.findViewById(R.id.user_info_perfect);
        tvReview = (TextView) view.findViewById(R.id.tvReview);
        userScore = (TextView) view.findViewById(R.id.userScore);
        mVip = (TextView) view.findViewById(R.id.vip);
        mCompleteInfo = (TextView) view.findViewById(R.id.completeInfo);

        userBuyVip = (RelativeLayout) view.findViewById(R.id.userBuyVip);
        userBuyLovemoney = (RelativeLayout) view.findViewById(R.id.userBuyLovemoney);
        buyLoveMoneyNow = (TextView) view.findViewById(R.id.buyLoveMoneyNow);
        buyVipNow = (TextView) view.findViewById(R.id.buyVipNow);
        loveMoenyTitle = (TextView) view.findViewById(R.id.loveMoenyTitle);
        vipTitle = (TextView) view.findViewById(R.id.vipTitle);
    }

    /**
     * 设置监听
     */
    private void setListener() {
        userInfoPerfect.setOnClickListener(this);
        userHeadPhoto.setOnClickListener(this);
        userBuyVip.setOnClickListener(this);
        userBuyLovemoney.setOnClickListener(this);
        mCompleteInfo.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.IS_NEED_REFLASH) {
            showProgressDialog("加载中");
            Constants.IS_NEED_REFLASH = false;
            mUserController.loadUserInfo();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_info_perfect://完善信息
            case R.id.completeInfo:
//                showProgressDialog("加载中");
//                PayController.getInstance().doRechargeCardPay(null);
                startActivity(PerfectInformationActivity.createIntent(getActivity()));
                break;
            case R.id.userHeadPhoto://拍照
                isHeadPhoto = true;
                if (mUser.getUser_face_status() == 0) {
                    choiceUserImage();
                } else {
                    //查看大图
                    Intent intent = PreviewPhotoActivity.createIntent(getActivity(), 0, true, UserController.getInstance().getUser(), true);
                    startActivity(intent);
                }
                break;
            case R.id.userBuyLovemoney://love money
                buyVip(0);
                break;
            case R.id.userBuyVip://会员
                buyVip(1);
                break;
        }
    }

    /**
     * 购买会员
     */
    private void buyVip(int type) {
        String url = "";
        String title = "";
        if (type == 1) {
            title = "购买会员";
            url = "http://www.aiputt.com/app/android/v1.2/pay/pay_type.html?type=vip";
        } else if (type == 0) {
            title = "购买爱币";
            url = "http://www.aiputt.com/app/android/v1.2/pay/pay_type.html?type=bi";
        }
        Intent intent = WebViewActivity.createIntent(getActivity(), title, url);
        startActivity(intent);
    }

    /**
     * 拍照or图库
     */
    private void choiceUserImage() {
        final PhotoUpDialog photoUpDialog = new PhotoUpDialog(getActivity());
        photoUpDialog.setTakePhotoListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
                photoUpDialog.dismiss();
            }
        });
        photoUpDialog.setTakeGallaryListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                photoUpDialog.dismiss();
            }
        });
        photoUpDialog.show();
    }

    /**
     * 打开图库选择图片
     */
    private void openGallery() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.PICK");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setType("image/*");

        startActivityForResult(intent, CHOICE_FROM_GALLARY);
    }


    /**
     * 拍照
     */
    private void takePhoto() {
        File fileDir = new File(Constants.FILEPATH + "/img/");
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        imageFile = fileDir + Constants.imageName();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (isIntentAvailable(getActivity(), intent)) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(imageFile)));
            intent.putExtra("return-data", false);
            startActivityForResult(intent, CHOICE_FROM_CAMERA);
        } else {
            PromptManager.showShortToast("该手机无法拍照，请更换可拍照的手机");
        }
    }

    /**
     * 检测该意图是否可被解析
     *
     * @param context
     * @param intent
     * @return
     */
    public boolean isIntentAvailable(Context context, Intent intent) {
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == CHOICE_FROM_CAMERA) {
                File f = new File(imageFile);
                if (f.exists()) {
                    upLoadPhotoFile(f);
                } else {
                    PromptManager.showCenterToast("拍照出问题啦,请重试");
                }
            } else if (requestCode == CHOICE_FROM_GALLARY) {
                Uri uri = data.getData();
                String[] proj = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().managedQuery(uri, proj, null, null, null);
                if (cursor != null) {
                    int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    imageFile = cursor.getString(column_index);
                    try {
                        // 4.0以上的版本会自动关闭 (4.0--14;; 4.0.3--15)
                        if (Integer.parseInt(Build.VERSION.SDK) < 14) {
                            cursor.close();
                        }
                    } catch (Exception e) {
                    }
                    File files = new File(imageFile);
                    if (files.exists()) {
                        upLoadPhotoFile(files);
                    } else {
                        PromptManager.showCenterToast("选取图片失败,请重新选取");
                    }
                } else {
                    String path = uri.getPath();
                    File file = new File(path);
                    if (file.exists()) {
                        upLoadPhotoFile(file);
                    } else {
                        PromptManager.showCenterToast("选取图片失败,请重新选取");
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 上传照片
     */
    private void upLoadPhotoFile(File file) {
        DisplayMetrics metrics = CommonUtil.getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        int x = width > 480 ? 480 : width;
        int y = height > 800 ? 800 : height;

        if (x < 480 || y < 800) {
            PromptManager.showShortToast("您上传的图片太小，请选择更清晰的照片！");
            return;
        }


        Bitmap image = PictureUtil.getAfterDealBitmap(x, y, file.getPath());
        if (image == null) {
            PromptManager.showShortToast("照片解析失败，请重试!");
            return;
        }

        // 旋转照片
        int angle = PictureUtil.readPictureDegree(imageFile);
        // 旋转图片 动作
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        // 创建新的图片
        Bitmap mImage = Bitmap.createBitmap(image, 0, 0,
                image.getWidth(), image.getHeight(), matrix, true);
        File fileDir = new File(Constants.FILEPATH + "/img/");
        mImageFile = new File(fileDir + Constants.imageName());
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        try {
            OutputStream outputStream = new FileOutputStream(mImageFile);
            mImage.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
            showProgressDialog("上传中");
            mUserController.upLoadUserPhoto(isHeadPhoto, mImageFile);
        } catch (Exception e) {
            PromptManager.showCenterToast("上传出错,请稍候再试");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    @Override
    public void onItemClick(int position) {
        if (position == 0) {
            isHeadPhoto = false;
            choiceUserImage();
        } else {
            Intent intent = PreviewPhotoActivity.createIntent(getActivity(), position - 1, false, UserController.getInstance().getUser(), true);
            startActivity(intent);
        }
    }

    @Override
    public void onRecentVisitItemClick(int position) {
        User currentItem = lookMeAdapter.getCurrentItem(position);
        Intent intent = OtherPersonSpaceActivity.createIntent(getActivity(), currentItem.getUid());
        startActivity(intent);
    }

    private class MineHandler extends EventHandler {

        public void onEvent(UserInfoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                if (event.mUser != null) {
                    mUser = event.mUser;
                    UserController.getInstance().setUser(mUser);
                    updateUI();
                } else {
                    PromptManager.showCenterToast("加载个人信息失败,请稍候再试");
                }
            } else {
                PromptManager.showCenterToast("加载个人信息失败,请稍候再试");
            }
        }

        public void onEvent(UploadHeadPhotoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                UserController.getInstance().getUser().setUser_face_status(1);
                mUser.setUser_face_status(1);
                tvReview.setVisibility(View.VISIBLE);
                mImageLoader.displayImage(event.user_face, userHeadPhoto, ImageLoadUtils.getHeadPhotoOptions());
                UserController.getInstance().getUser().setUser_face(event.user_face);
                UserController.getInstance().getUser().setUser_face_big(event.user_face_big);
                mUser.setUser_face(event.user_face);
                mUser.setUser_face_big(event.user_face_big);
                updateAiBi(event.user_total_balance);
            } else {
                PromptManager.showCenterToast("上传失败,请重新上传");
            }
        }

        public void onEvent(UploadNormalPhotoEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                if (UserController.getInstance().getUser().getImg_list() == null) {
                    ArrayList<ImageList> al = new ArrayList<ImageList>();
                    UserController.getInstance().getUser().setImg_list(al);
                }
                UserController.getInstance().getUser().getImg_list().add(event.imageList);
                adapter.addImageList(event.imageList);
                adapter.notifyDataSetChanged();
            } else {
                PromptManager.showCenterToast("上传失败,请重新上传");
            }
        }

        public void onEvent(NotifyUserPhotoChangeEvent event) {
            ImageList removedImageList = null;
            for (int i = 0; i < mUserImg_list.size(); i++) {
                if (event.imageList.getPid().equals(mUserImg_list.get(i).getPid())) {
                    removedImageList = mUserImg_list.get(i);
                }
            }
            if (removedImageList != null) {
                mUserImg_list.remove(removedImageList);
                mUser.getImg_list().remove(removedImageList);
                adapter.replace(mUserImg_list);
                adapter.notifyDataSetChanged();
            }
        }

        public void onEvent(BuyVipEvent event) {
            showProgressDialog("加载中");
            UserController.getInstance().loadUserInfo();
        }

        public void onEvent(UpdateUserInfoEvent event) {
            if (event.code == Constants.SERVER_CONNECT_OK) {
                mUser = event.user;
                setData();
            }
        }

        public void onEvent(ModifyHeadPhotoEvent event) {
            if (event.code == Constants.SERVER_CONNECT_OK) {
                addHeadPhotoView();
            }
        }

    }

    private void updateAiBi(String user_total_balance) {
        if (TextUtils.isEmpty(user_total_balance) || user_total_balance.equals("0")) {
            loveMoenyTitle.setText("爱币");
            buyLoveMoneyNow.setText("立即购买");
        } else {
            loveMoenyTitle.setText("爱币: " + user_total_balance + "个");
            buyLoveMoneyNow.setText("继续购买");
        }
    }

    /**
     * after loadUserInfo update ui
     */
    private void updateUI() {
        setData();
        addHeadPhotoView();
        addPhotoView();
        showRecentVisit();
    }

    private void showRecentVisit() {
        if (mUser.getRecent_visit() == null || mUser.getRecent_visit().isEmpty()) {
            rlRecentVisit.setVisibility(View.GONE);
            whoLookMe.setVisibility(View.VISIBLE);
        } else {
            rlRecentVisit.setVisibility(View.VISIBLE);
            whoLookMe.setVisibility(View.GONE);
            lookMeAdapter.replace(mUser.getRecent_visit());
            lookMeAdapter.notifyDataSetChanged();
            lookMeAdapter.setOnRecentVisitItemClickListener(this);
        }
    }

    private void addHeadPhotoView() {
        if (!TextUtils.isEmpty(mUser.getUser_face())) {
            if (mUser.getUser_face_status() == 1) {
                tvReview.setVisibility(View.VISIBLE);
            }
            mImageLoader.displayImage(mUser.getUser_face(), userHeadPhoto, ImageLoadUtils.getHeadPhotoOptions());
        }
    }

    private void setData() {
        tvName.setText(mUser.getUser_nick());
        if (!TextUtils.isEmpty(mUser.getUser_age())) {
            tvAge.setText(mUser.getUser_age() + "岁");
        }
        tvLocation.setText(mUser.getUser_location());
        tvID.setText("ID:" + mUser.getUser_no());
        if (TextUtils.isEmpty(mUser.getUser_feature()) && TextUtils.isEmpty(mUser.getUser_hobby())) {
            llFeatureHobby.setVisibility(View.GONE);
            tvDisposition.setVisibility(View.GONE);
        } else {
            llFeatureHobby.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(mUser.getUser_hobby())) {
                tvHobby.setVisibility(View.GONE);
            } else {
                String hobby = mUser.getUser_hobby().split(",")[0];
                if (hobby.endsWith("]")) {
                    hobby = hobby.substring(0, hobby.length() - 1);
                }
                tvHobby.setText(hobby.substring(1));
            }
            if (TextUtils.isEmpty(mUser.getUser_feature())) {
                tvDisposition.setVisibility(View.GONE);
            } else {
                String feature = mUser.getUser_feature().split(",")[0];
                if (feature.endsWith("]")) {
                    feature = feature.substring(0, feature.length() - 1);
                }
                tvDisposition.setText(feature.substring(1));
            }
        }
        if (mUser.getIs_vip() == 1) {
            vipTitle.setText("VIP会员: " + mUser.getSurplus_day() + "天");
            buyVipNow.setText("继续续费");
        } else {
            vipTitle.setText("VIP会员");
            buyVipNow.setText("立即充值");
        }
        if (TextUtils.isEmpty(mUser.getUser_total_balance()) || mUser.getUser_total_balance().equals("0")) {
            loveMoenyTitle.setText("爱币");
            buyLoveMoneyNow.setText("立即购买");
        } else {
            loveMoenyTitle.setText("爱币: " + mUser.getUser_total_balance() + "个");
            buyLoveMoneyNow.setText("继续购买");
        }
        userScore.setText(mUser.getScore() + "");
        if (mUser.getScore() == 100) {
            mCompleteInfo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_complete_info, 0, 0);
        } else {
            mCompleteInfo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_complete_info_gray, 0, 0);
        }

        if (mUser.getIs_vip() == 1) {
            mVip.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vip, 0, 0);
        } else {
            mVip.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vip_gray, 0, 0);
        }

    }

    /**
     * 添加照片
     */
    private void addPhotoView() {
        if (mUser.getImg_list() != null) {
            mUserImg_list = mUser.getImg_list();
        }
        if (mUserImg_list != null && !mUserImg_list.isEmpty()) {
            adapter.replace(mUserImg_list);
            adapter.notifyDataSetChanged();
        }
    }

}
