package com.Aiputt.taohuayun.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.activity.MainActivity;
import com.Aiputt.taohuayun.adapter.PrivateMailAdapter;
import com.Aiputt.taohuayun.controller.MailController;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.MailResultEvent;
import com.Aiputt.taohuayun.event.PrivateMailEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.IntentUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.xlistview.XListView;

import java.util.Iterator;

public class PrivateMailFragment extends BaseFragment implements
		XListView.IXListViewListener, AdapterView.OnItemClickListener {

	private NotifyHandler mHandler = new NotifyHandler();
	private MailController mController = MailController.getInstance();
	private XListView xlvData;
	private PrivateMailAdapter mAdapter;
	private User currentUser;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mHandler.register();

	}
	
	@Override
	public void onResume() {
		mController.getPrivateMessageList();
		super.onResume();
	}
	
	public  void updateData(){
		mController.getPrivateMessageList();
	}

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
	public void onDestroy() {
        mHandler.unregister();
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = View.inflate(getActivity(), R.layout.fragment_private_mail,
				null);
		xlvData = (XListView) view.findViewById(R.id.xlvData);
		xlvData.setPullLoadEnable(false);

		xlvData.setXListViewListener(this, 0);
		xlvData.setRefreshTime();
		xlvData.setOnItemClickListener(this);
		showProgressDialog("加载中");
		mController.getPrivateMessageList();
		return view;
	}

	private class NotifyHandler extends EventHandler {

		public void onEvent(PrivateMailEvent event) {
			onLoadOver();
			dismissProgressDialog();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				if (event.data != null && event.data.size() != 0) {
					if (MainActivity.ivIcon!=null) {
						MainActivity.ivIcon.setVisibility(View.GONE);
					}
					for (Iterator iterator = event.data.iterator(); iterator
							.hasNext();) {
						User type = (User) iterator.next();
						if (type.getUnread_cnt()>0) {
							if (MainActivity.ivIcon!=null) {
								MainActivity.ivIcon.setVisibility(View.VISIBLE);
							}
							break;
						}
					}
					
					mAdapter = new PrivateMailAdapter(
							PrivateMailFragment.this.getActivity(), event.data);
					xlvData.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}
		public boolean onEvent(MailResultEvent event) {
			dismissProgressDialog();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				IntentUtils.enterHuanxin(mContext,
						String.valueOf(currentUser.getUser_no()), currentUser.getUser_nick(),
						String.valueOf(currentUser.getUid()), currentUser.getUser_face());
			} else {
				PromptManager.showCenterToast(event.msg);
			}
			return true;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		User mUser = mAdapter.getItem(position - 1);
		if (mUser != null) {
			currentUser = mUser;
			showProgressDialog(R.string.pl_wait);
			mController.clearMessageList(mUser.getDialog_id());
		}

	}

	@Override
	public void onRefresh(int id) {
		mController.getPrivateMessageList();

	}

	@Override
	public void onLoadMore(int id) {
		// TODO Auto-generated method stub

	}

	private void onLoadOver() {
		xlvData.stopRefresh();
		xlvData.stopLoadMore();
		xlvData.setRefreshTime();
	}
}
