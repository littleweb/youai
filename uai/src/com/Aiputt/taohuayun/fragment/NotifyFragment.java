package com.Aiputt.taohuayun.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.activity.NoticeWebViewActivity;
import com.Aiputt.taohuayun.adapter.NotifyMessageAdapter;
import com.Aiputt.taohuayun.controller.MailController;
import com.Aiputt.taohuayun.domain.NotifyMessage;
import com.Aiputt.taohuayun.event.NotifyEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.xlistview.XListView;

public class NotifyFragment extends BaseFragment implements
		XListView.IXListViewListener, AdapterView.OnItemClickListener {
	private static final String EXTRA_EXPERT_LIST = "data_list";
//	private NotifyHandler mHandler = new NotifyHandler();
//	private MailController mController = MailController.getInstance();
	private XListView xlvData;
	private NotifyMessageAdapter mAdapter;
	private ArrayList<NotifyMessage> mList;

	private RelativeLayout rlSystem;
	private RelativeLayout rlRengong;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// mHandler.register();
		// showProgressDialog("加载中");

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// mHandler.unregister();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = View.inflate(getActivity(), R.layout.fragment_notify, null);
		rlSystem = (RelativeLayout) view.findViewById(R.id.rlSystem);
		rlRengong = (RelativeLayout) view.findViewById(R.id.rlRengong);

		rlSystem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = NoticeWebViewActivity
						.createIntent(getActivity(), "系统通知",
								"http://www.aiputt.com/app/android/v1/mail/notice.html",true);
				startActivity(intent);

			}
		});
		rlRengong.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = NoticeWebViewActivity
						.createIntent(getActivity(), "人工客服",
								"http://www.aiputt.com/app/android/v1/mail/kefu.html",false);
				startActivity(intent);
			}
		});

		return view;
	}

//	private class NotifyHandler extends EventHandler {
//
//		public void onEvent(NotifyEvent event) {
//			dismissProgressDialog();
//			if (event.code == Constants.SERVER_CONNECT_OK) {
//				if (event.data != null && event.data.size() != 0) {
//					mList = event.data;
//					mAdapter = new NotifyMessageAdapter(getActivity(), mList);
//					xlvData.setAdapter(mAdapter);
//				}
//			} else {
//				PromptManager.showCenterToast(event.msg);
//			}
//			dismissProgressDialog();
//		}
//
//	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRefresh(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadMore(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (mList != null && !mList.isEmpty()) {
			outState.putParcelableArrayList(EXTRA_EXPERT_LIST, mList);
		}
		super.onSaveInstanceState(outState);
	}
}
