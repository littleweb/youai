package com.Aiputt.taohuayun.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.activity.OtherPersonSpaceActivity;
import com.Aiputt.taohuayun.activity.PerfectInformationActivity;
import com.Aiputt.taohuayun.adapter.SuperAdapter;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.controller.MainController;
import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.domain.UserList;
import com.Aiputt.taohuayun.event.MainDataEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.service.LocationService;
import com.Aiputt.taohuayun.service.PopInfoService;
import com.Aiputt.taohuayun.utils.HuanXinSendMessage;
import com.Aiputt.taohuayun.utils.ImageLoadUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.widgets.RoundedImageView;
import com.Aiputt.taohuayun.xlistview.XListView.IXListViewListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends BaseFragment implements IXListViewListener {
	private static final String EXTRA_EXPERT_LIST = "data_list";
	private PullToRefreshGridView xlvData;
	private MainHandler mHandler = new MainHandler();
	private MainController mController = MainController.getInstance();
	private int page = 1;
	private ArrayList<User> userLists;
	private boolean isUpdate = false;
	private MainNImageAdapter mainImageAdapter;
	private String currentUserNo;
	private User currentUser;
	private TextView tv_complete_info;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
//			userLists = (ArrayList<UserList>) savedInstanceState
//					.getSerializable(EXTRA_EXPERT_LIST);、
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = View.inflate(getActivity(), R.layout.fragment_main, null);
		xlvData = (PullToRefreshGridView) view.findViewById(R.id.xlvData);
		tv_complete_info = (TextView)view.findViewById(R.id.tv_complete_info);
		mHandler.register();


		// xlvData.setPullLoadEnable(true);
		// xlvData.setXListViewListener(this, 0);
		// xlvData.setRefreshTime();

		xlvData.setOnRefreshListener(new OnRefreshListener2<GridView>() {

			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase<GridView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh"); // Do work to
				String label = DateUtils.formatDateTime(getActivity(),
						System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME
								| DateUtils.FORMAT_SHOW_DATE
								| DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				onRefresh(0);
			}

			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase<GridView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh"); // Do work to refresh
													// the list here.
				onLoadMore(0);
			}
		});
		if (userLists != null && !userLists.isEmpty()) {
			mainImageAdapter = new MainNImageAdapter(getActivity(), userLists);
			xlvData.setAdapter(mainImageAdapter);
		} else {
			userLists = new ArrayList<User>();
			showProgressDialog(R.string.pl_wait);
			mController.actionRegister(String.valueOf(page));
		}
		tv_complete_info.setOnClickListener(this);
        UserController.getInstance().loadPopQa();
        Intent intent = new Intent(getActivity(), PopInfoService.class);
        getActivity().startService(intent);
        Intent locationIntent = new Intent(getActivity(), LocationService.class);
        getActivity().startService(locationIntent);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (SharePCach.loadBooleanCach("mainCompleteInfo")) {
			tv_complete_info.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.tv_complete_info:
			SharePCach.saveBooleanCach("mainCompleteInfo", true);
			Intent intent = new Intent(getActivity(),PerfectInformationActivity.class);
			startActivity(intent);
			break;

		default:
			break;
		}
	}

	private void onLoadOver() {
		// xlvData.stopRefresh();
		// xlvData.stopLoadMore();
		// xlvData.setRefreshTime();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mHandler.unregister();
	}


	@Override
	public void onRefresh(int id) {
		isUpdate = true;
		mController.actionRegister(String.valueOf(page));

	}

	public void onRefresh() {
		showProgressDialog(R.string.pl_wait);
		isUpdate = true;
		mController.actionRegister(String.valueOf(page));

	}

	@Override
	public void onLoadMore(int id) {
		mController.actionRegister(String.valueOf(page));

	}

	public void onSaveInstanceState(Bundle outState) {
//		if (userLists != null && !userLists.isEmpty()) {
//			outState.putSerializable(EXTRA_EXPERT_LIST, userLists);
//		}
		super.onSaveInstanceState(outState);
	};

	class MainHandler extends EventHandler {
		public void onEvent(MainDataEvent event) {
			xlvData.onRefreshComplete();
			dismissProgressDialog();
			onLoadOver();
			if (event.code == 0) {
				page++;
				ArrayList<User> data = event.data;
				if (data != null) {
					if (isUpdate) {
						userLists.clear();
						isUpdate = false;
					}
					userLists.addAll(data);
					// if (data.size() >= 18) {
					// if (isUpdate) {
					// userLists.clear();
					// isUpdate = false;
					// }
					// UserList userList1 = new UserList();
					// userList1.users = new ArrayList<User>();
					// UserList userList2 = new UserList();
					// userList2.users = new ArrayList<User>();
					// for (int i = 0; i < data.size(); i++) {
					// if (i <= 8) {
					// userList1.users.add(data.get(i));
					// } else {
					// userList2.users.add(data.get(i));
					// }
					// }
					// userLists.add(userList1);
					// userLists.add(userList2);
					// }
				}
				if (mainImageAdapter == null) {
					mainImageAdapter = new MainNImageAdapter(getActivity(),
							userLists);
					xlvData.setAdapter(mainImageAdapter);
				} else {
					mainImageAdapter.notifyDataSetChanged();
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}

		public Boolean onEvent(ResultEvent event) {
			dismissProgressDialog();
			onLoadOver();
			if (event.code == 0) {
				HuanXinSendMessage.sendTxTMessage(getActivity(), currentUserNo,
						event.data);
				PromptManager.showCenterToast("打招呼成功");
				currentUser.setIsSayHello(1);
				mainImageAdapter.notifyDataSetChanged();
			} else {
				PromptManager.showCenterToast(event.msg);
			}
			return true;
		}

	}

	public class MainImageAdapter extends SuperAdapter<UserList> {

		public MainImageAdapter(Context context, List<UserList> list) {
			super(context, list);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				convertView = View.inflate(mContext, R.layout.item_main, null);
				holder = new ViewHolder();
				holder.ivDazhaohu1 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu1);
				holder.ivDazhaohu2 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu2);
				holder.ivDazhaohu3 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu3);
				holder.ivDazhaohu4 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu4);
				holder.ivDazhaohu5 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu5);
				holder.ivDazhaohu6 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu6);
				holder.ivDazhaohu7 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu7);
				holder.ivDazhaohu8 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu8);
				holder.ivDazhaohu9 = (ImageView) convertView
						.findViewById(R.id.ivDazhaohu9);

				holder.iv1 = (RoundedImageView) convertView
						.findViewById(R.id.iv1);
				holder.iv2 = (RoundedImageView) convertView
						.findViewById(R.id.iv2);
				holder.iv3 = (RoundedImageView) convertView
						.findViewById(R.id.iv3);
				holder.iv4 = (RoundedImageView) convertView
						.findViewById(R.id.iv4);
				holder.iv5 = (RoundedImageView) convertView
						.findViewById(R.id.iv5);
				holder.iv6 = (RoundedImageView) convertView
						.findViewById(R.id.iv6);
				holder.iv7 = (RoundedImageView) convertView
						.findViewById(R.id.iv7);
				holder.iv8 = (RoundedImageView) convertView
						.findViewById(R.id.iv8);
				holder.iv9 = (RoundedImageView) convertView
						.findViewById(R.id.iv9);
				holder.tvName = (TextView) convertView
						.findViewById(R.id.tvName);
				holder.tvAge = (TextView) convertView.findViewById(R.id.tvAge);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			UserList userList = mList.get(position);
			if (userList != null && userList.users != null) {
				if (userList.users.size() >= 1) {
					final User user = userList.users.get(0);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv1);
					holder.tvName.setText(user.getUser_nick());
					holder.tvAge.setText(user.getUser_age() + "岁");
					holder.iv1.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu1
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 2) {
					final User user = userList.users.get(1);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv2);
					holder.iv2.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu2
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 3) {
					final User user = userList.users.get(2);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv3);
					holder.iv3.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu3
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 4) {
					final User user = userList.users.get(3);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv4);
					holder.iv4.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu4
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 5) {
					final User user = userList.users.get(4);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv5);
					holder.iv5.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu5
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 6) {
					final User user = userList.users.get(5);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv6);
					holder.iv6.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu6
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 7) {
					final User user = userList.users.get(6);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv7);
					holder.iv7.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu7
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 8) {
					final User user = userList.users.get(7);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv8);
					holder.iv8.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu8
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
				if (userList.users.size() >= 9) {
					final User user = userList.users.get(8);
					ImageLoader.getInstance().displayImage(user.getImg_url(),
							holder.iv9);
					holder.iv9.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = OtherPersonSpaceActivity
									.createIntent(getActivity(), user.getUid());
							mContext.startActivity(intent);

						}
					});
					holder.ivDazhaohu9
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									showProgressDialog(R.string.pl_wait);
									currentUserNo = user.getUser_no();
									mController.actionDazhaohu(String
											.valueOf(user.getUid()));

								}
							});
				}
			}
			return convertView;
		}

	}

	static class ViewHolder {
		public ImageView ivDazhaohu9;
		public ImageView ivDazhaohu8;
		public ImageView ivDazhaohu7;
		public ImageView ivDazhaohu6;
		public ImageView ivDazhaohu5;
		public ImageView ivDazhaohu4;
		public ImageView ivDazhaohu3;
		public ImageView ivDazhaohu2;
		public ImageView ivDazhaohu1;
		public RoundedImageView iv1;
		public RoundedImageView iv2;
		public RoundedImageView iv3;
		public RoundedImageView iv4;
		public RoundedImageView iv5;
		public RoundedImageView iv6;
		public RoundedImageView iv7;
		public RoundedImageView iv8;
		public RoundedImageView iv9;
		public TextView tvName;
		public TextView tvAge;
	}

	public class MainNImageAdapter extends SuperAdapter<User> {

		public MainNImageAdapter(Context context, List<User> list) {
			super(context, list);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolderN holder;
			if (convertView == null) {
				convertView = View
						.inflate(mContext, R.layout.item_main_n, null);
				holder = new ViewHolderN();
				holder.ivImage = (RoundedImageView) convertView
						.findViewById(R.id.ivImage);
				holder.ivSave = (ImageView) convertView
						.findViewById(R.id.ivSave);
				holder.tvAge = (TextView) convertView
						.findViewById(R.id.tvAge);
				holder.tvLocation = (TextView) convertView
						.findViewById(R.id.tvLocation);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolderN) convertView.getTag();
			}
			final User user = mList.get(position);
			if (user != null) {
                if(!user.getImg_url().equals(holder.ivImage.getTag())){
                    ImageLoader.getInstance().displayImage(user.getImg_url(),
                            holder.ivImage, ImageLoadUtils.getNormalOptions());
                }
                holder.ivImage.setTag(user.getImg_url());
                holder.tvAge.setText("年龄："+user.getUser_age());
                holder.tvLocation.setText(user.getUser_province());

				if (user.getIsSayHello() == 1) {
					holder.ivSave
							.setBackgroundResource(R.drawable.n_xin);
				} else {
					holder.ivSave
							.setBackgroundResource(R.drawable.n_xin_h);
				}
				holder.ivImage.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = OtherPersonSpaceActivity
								.createIntent(getActivity(), user.getUid());
						mContext.startActivity(intent);

					}
				});
				holder.ivSave.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						if (user.getIsSayHello() == 0) {
						showProgressDialog(R.string.pl_wait);
						currentUser = user;
						currentUserNo = user.getUser_no();
						mController.actionDazhaohu(String
								.valueOf(user.getUid()));
						}

					}
				});
			}
			return convertView;
		}

	}

	static class ViewHolderN {
		public RoundedImageView ivImage;
		public ImageView ivSave;
		public TextView tvAge;
		public TextView tvLocation;


	}

}
