package com.Aiputt.taohuayun.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.adapter.PrivateMailAdapter;
import com.Aiputt.taohuayun.controller.MailController;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.UnreadMailEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.IntentUtils;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.xlistview.XListView;

public class ContactFragment extends BaseFragment implements
		XListView.IXListViewListener, AdapterView.OnItemClickListener {

	private NotifyHandler mHandler = new NotifyHandler();
	private MailController mController = MailController.getInstance();
	private XListView xlvData;
	private PrivateMailAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mHandler.register();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mHandler.unregister();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = View.inflate(getActivity(), R.layout.fragment_contact_list,
				null);
		xlvData = (XListView) view.findViewById(R.id.xlvData);
		xlvData.setPullLoadEnable(false);

		xlvData.setXListViewListener(this, 0);
		xlvData.setRefreshTime();
		xlvData.setOnItemClickListener(this);
		showProgressDialog("加载中");
		mController.getUnreadMessageList();
		return view;
	}

	private class NotifyHandler extends EventHandler {

		public void onEvent(UnreadMailEvent event) {
			dismissProgressDialog();
			onLoadOver();
			if (event.code == Constants.SERVER_CONNECT_OK) {
				if (event.data != null && event.data.size() != 0) {
					mAdapter = new PrivateMailAdapter(
							ContactFragment.this.getActivity(), event.data);
					xlvData.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				}
			} else {
				PromptManager.showCenterToast(event.msg);
			}
		}

	}

	private void onLoadOver() {
		xlvData.stopRefresh();
		xlvData.stopLoadMore();
		xlvData.setRefreshTime();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		User mUser = mAdapter.getItem(position - 1);
		if (mUser != null) {
			IntentUtils.enterHuanxin(mContext,
					String.valueOf(mUser.getUser_no()), mUser.getUser_nick(),
					String.valueOf(mUser.getUid()), mUser.getUser_face());
		}

	}

	@Override
	public void onRefresh(int id) {
		mController.getUnreadMessageList();

	}

	@Override
	public void onLoadMore(int id) {
		// TODO Auto-generated method stub

	}
}