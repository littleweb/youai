package com.Aiputt.taohuayun.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.activity.OtherPersonSpaceActivity;
import com.Aiputt.taohuayun.adapter.NearByAdapter;
import com.Aiputt.taohuayun.app.ULoveApplication;
import com.Aiputt.taohuayun.controller.NearByController;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.LocationEvent;
import com.Aiputt.taohuayun.event.NearByEvent;
import com.Aiputt.taohuayun.event.RefreshNearByEvent;
import com.Aiputt.taohuayun.event.SayHiEventEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.LocationUtil;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.Aiputt.taohuayun.xlistview.XListView;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NearByFragment extends BaseFragment implements XListView.IXListViewListener, AdapterView.OnItemClickListener, NearByAdapter.OnClickSayHiListener {

    private static final String TAG = "NearByFragment";

    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener();

    private NearByHandler mHandler = new NearByHandler();
    private XListView xlvNearBy;
    private NearByAdapter mAdapter;
    private TextView emptyView;
    private List<User> userList = new ArrayList<User>();

    private int page = 1;

    private NearByController mNearByController = NearByController.getInstance();

    private Map<String, String> params = new HashMap<String, String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler.register();
        showProgressDialog("加载中");
        if (!TextUtils.isEmpty(LocationUtil.getLongitude())) {
            params = new HashMap<String, String>();
            params.put("lat", LocationUtil.getLatitude());
            params.put("lng", LocationUtil.getLongitude());
            params.put("page", page + "");
            mNearByController.getNearByList(params);
        } else {
            mLocationClient = new LocationClient(getActivity());
            mLocationClient.registerLocationListener(myListener);
            initLocation();
        }
    }

    /**
     * 开始定位
     */
    private void startLocation() {
        mLocationClient = ((ULoveApplication) getActivity().getApplicationContext()).mLocationClient;
        LocationClientOption option = LocationUtil.initLocation();
        mLocationClient.setLocOption(option);
        mLocationClient.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_nearby, null);
        xlvNearBy = (XListView) view.findViewById(R.id.xlvNearBy);
        emptyView = (TextView) view.findViewById(R.id.emptyView);
        xlvNearBy.setPullLoadEnable(false);
        mAdapter = new NearByAdapter(getActivity());
        mAdapter.setOnClickSayHiListener(this);
        xlvNearBy.setAdapter(mAdapter);
        xlvNearBy.setXListViewListener(this, 0);
        xlvNearBy.setRefreshTime();
        xlvNearBy.setOnItemClickListener(this);
        xlvNearBy.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), false, false));
        return view;
    }


    @Override
    public void onStop() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.unregister();
    }

    @Override
    public void onRefresh(int id) {
        page = 1;
        if (!TextUtils.isEmpty(LocationUtil.getLongitude())) {
            params = new HashMap<String, String>();
            params.put("lat", LocationUtil.getLatitude());
            params.put("lng", LocationUtil.getLongitude());
            params.put("page", page + "");
            mNearByController.getNearByList(params);
        } else {
            mLocationClient = new LocationClient(getActivity());
            mLocationClient.registerLocationListener(myListener);
            initLocation();
        }
    }

    @Override
    public void onLoadMore(int id) {
        page += 1;
        params.put("page", page + "");
        mNearByController.getNearByList(params);
    }

    private void onLoadOver() {
        xlvNearBy.stopRefresh();
        xlvNearBy.stopLoadMore();
        xlvNearBy.setRefreshTime();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        User user = mAdapter.getItem(position - 1);
        Intent intent = OtherPersonSpaceActivity.createIntent(getActivity(), user.getUid());
        startActivity(intent);
    }

    @Override
    public void onClickSayHi(User user) {
        showProgressDialog("请稍候");
        mNearByController.sayHi(user);
    }

    private class NearByHandler extends EventHandler {

        public void onEvent(LocationEvent event) {
//            if (event.mBDLocation != null) {
//                Log.i(TAG, event.mBDLocation.getAddrStr());
//                params.put("lat", event.mBDLocation.getLatitude() + "");
//                params.put("lng", event.mBDLocation.getLongitude() + "");
//                params.put("page", page + "");
//                mNearByController.getNearByList(params);
//            } else {
//                dismissProgressDialog();
//                xlvNearBy.setEmptyView(emptyView);
//                Toast.makeText(getActivity(), "定位失败,请稍后再试", Toast.LENGTH_SHORT).show();
//            }
        }

        public void onEvent(NearByEvent event) {
            if (event.code == Constants.SERVER_CONNECT_OK) {
                if (event.userList != null && event.userList.size() != 0) {
                    if (page == 0) {
                        mAdapter.replaceAll(event.userList);
                    } else {
                        mAdapter.addAll(event.userList);
                    }
                    mAdapter.notifyDataSetChanged();
                    if (event.userList.size() < 20) {
                        xlvNearBy.setPullLoadEnable(false);
                    } else {
                        xlvNearBy.setPullLoadEnable(true);
                    }
                } else {
                    xlvNearBy.setEmptyView(emptyView);
                }
            } else {
                xlvNearBy.setEmptyView(emptyView);
                PromptManager.showCenterToast(event.msg);
            }
            onLoadOver();
            dismissProgressDialog();
        }

        public void onEvent(SayHiEventEvent event) {
            dismissProgressDialog();
            if (event.code == Constants.SERVER_CONNECT_OK) {
                PromptManager.showCenterToast("打招呼成功");
                mAdapter.notifyDataSetChanged();
            } else {
                PromptManager.showCenterToast(event.msg);
            }
        }

        public void onEvent(RefreshNearByEvent event) {
            showProgressDialog("加载中");
            page = 1;
            if (!TextUtils.isEmpty(LocationUtil.getLongitude())) {
                params = new HashMap<String, String>();
                params.put("lat", LocationUtil.getLatitude());
                params.put("lng", LocationUtil.getLongitude());
                params.put("page", page + "");
                mNearByController.getNearByList(params);
            } else {
                mLocationClient = new LocationClient(getActivity());
                mLocationClient.registerLocationListener(myListener);
                initLocation();
            }
        }

    }

    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系，
        option.setScanSpan(0);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        mLocationClient.setLocOption(option);
        mLocationClient.start();
    }

    /**
     * 实现实时位置回调监听
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            BDLocation mBDLocation = null;
            //Receive Location
            if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                Log.i(TAG, "gps定位成功");
                mBDLocation = location;
            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                Log.i(TAG, "网络定位成功");
                mBDLocation = location;
            } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                Log.i(TAG, "离线定位成功");
                mBDLocation = location;
            } else if (location.getLocType() == BDLocation.TypeServerError) {
                Log.i(TAG, "服务端网络定位失败");
            } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                Log.i(TAG, "网络不通导致定位失败，请检查网络是否通畅");
            } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                Log.i(TAG, "无法获取有效定位依据导致定位失败");
            }
            mLocationClient.stop();
            LocationUtil.setCity(mBDLocation.getCity());
            LocationUtil.setLatitude(mBDLocation.getLatitude() + "");
            LocationUtil.setLongitude(mBDLocation.getLongitude() + "");
            params.put("lat", mBDLocation.getLatitude() + "");
            params.put("lng", mBDLocation.getLongitude() + "");
            params.put("page", page + "");
            mNearByController.getNearByList(params);
        }

    }
}
