package com.Aiputt.taohuayun.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.MailResultEvent;
import com.Aiputt.taohuayun.event.NotifyEvent;
import com.Aiputt.taohuayun.event.PrivateMailEvent;
import com.Aiputt.taohuayun.event.RengongEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.event.SendSugEvent;
import com.Aiputt.taohuayun.event.UnreadMailEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class MailController extends BaseController {

	private static MailController mNearByController = new MailController();

	private MailController() {

	}

	public static MailController getInstance() {
		return mNearByController;
	}

	public void getNotifyList() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "1");
		HttpRequest request = new HttpRequest(Constants.I_MAIL_NOTIFY,map);
		final NotifyEvent event = new NotifyEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<String>() {
					@Override
					protected void onFailure(ULoveException e) {
						event.msg = e.getServerMsg();
						notifyEvent(event);
					}

					@Override
					public void onSuccess(String data)
							throws Exception {
						event.code = Constants.SERVER_CONNECT_OK;
						event.data = data;
						notifyEvent(event);
					}
				});
	}
	public void getRengongList() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "1");
		HttpRequest request = new HttpRequest(Constants.I_MAIL_RENGONG,map);
		final RengongEvent event = new RengongEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<String>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}
			
			@Override
			public void onSuccess(String data)
					throws Exception {
				event.code = Constants.SERVER_CONNECT_OK;
				event.data = data;
				notifyEvent(event);
			}
		});
	}
	public void getPrivateMessageList() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "1");
		HttpRequest request = new HttpRequest(Constants.I_MAIL_PRIVATE_MSG,map);
		final PrivateMailEvent event = new PrivateMailEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<ArrayList<User>>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}
			
			@Override
			public void onSuccess(ArrayList<User> data)
					throws Exception {
				event.code = Constants.SERVER_CONNECT_OK;
				event.data = data;
				notifyEvent(event);
			}
		});
	}
	public void clearMessageList(String dialog_id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("dialog_id", dialog_id);
		HttpRequest request = new HttpRequest(Constants.I_CLEAR_PRIVATE_MSG,map);
		final MailResultEvent event = new MailResultEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<ArrayList<String>>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}
			
			@Override
			public void onSuccess(ArrayList<String> data)
					throws Exception {
				event.code = Constants.SERVER_CONNECT_OK;
				notifyEvent(event);
			}
		});
	}
	public void getUnreadMessageList() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "1");
		map.put("unread", "1");
		HttpRequest request = new HttpRequest(Constants.I_MAIL_PRIVATE_MSG,map);
		final UnreadMailEvent event = new UnreadMailEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<ArrayList<User>>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}

			@Override
			public void onSuccess(ArrayList<User> data)
					throws Exception {
				event.code = Constants.SERVER_CONNECT_OK;
				event.data = data;
				notifyEvent(event);
			}
		});
	}
	
	public void sendSuggest(String content) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("content", content);
		HttpRequest request = new HttpRequest(Constants.I_USER_SEND,map);
		final SendSugEvent event = new SendSugEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<String>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}

			@Override
			public void onSuccess(String data)
					throws Exception {
				event.code = Constants.SERVER_CONNECT_OK;
				event.data = data;
				notifyEvent(event);
			}
		});
	}

}
