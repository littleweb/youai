package com.Aiputt.taohuayun.controller;

import android.app.Activity;
import android.text.TextUtils;

import com.Aiputt.taohuayun.alipay.utils.PayUtils;
import com.Aiputt.taohuayun.domain.OrderNoResponse;
import com.Aiputt.taohuayun.event.AlipayEvent;
import com.Aiputt.taohuayun.event.DnaPayEvent;
import com.Aiputt.taohuayun.event.OrderNoEvent;
import com.Aiputt.taohuayun.event.RechargePayEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.PromptManager;
import com.alipay.sdk.app.PayTask;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/8/5.
 */
public class PayController extends BaseController {

    private PayController() {
    }

    private static PayController mController = new PayController();

    public static PayController getInstance() {
        return mController;
    }

    public void doAlipay(final Activity activity, OrderNoResponse orderNoResponse) {
        String orderInfo = PayUtils.getOrderInfo(orderNoResponse.getSubject(), orderNoResponse.getBody(), orderNoResponse.getPrice(), orderNoResponse.getOrdid(), orderNoResponse.getNotify_url());
        String sign = PayUtils.sign(orderInfo);
        try {
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final AlipayEvent event = new AlipayEvent();
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
                + PayUtils.getSignType();
        runWorkThread(new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(activity);
                event.result = alipay.pay(payInfo);
                notifyEvent(event);
            }
        });
    }

    /**
     * @param productId
     * @param payType              1 alipay 2 dna 3 recharge
     * @param rechargeCardNumber
     * @param rechargeCardPassword
     */
    public void getOrderNo(String productId, final String payType, final String rechargeCardNumber, final String rechargeCardPassword, final String cardcode) {
        final OrderNoEvent event = new OrderNoEvent();
        Map<String, String> params = new HashMap<String, String>();
        params.put("productid", productId);
        params.put("payment_type", payType);
        HttpRequest request = new HttpRequest(Constants.I_GET_ORDER_NO, params);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<OrderNoResponse>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(OrderNoResponse data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.payType = Integer.parseInt(payType);
                event.orderNoResponse = data;
                if (!TextUtils.isEmpty(rechargeCardNumber)) {
                    event.rechargeCardNumber = rechargeCardNumber;
                    event.rechargeCardPassword = rechargeCardPassword;
                    PromptManager.showCenterToast("cardcode = " + cardcode);
                    event.cardcode = cardcode;
                }
                notifyEvent(event);
            }
        });
    }

    /**
     * @param productId
     * @param payType
     */
    public void getOrderNo(String productId, String payType) {
        this.getOrderNo(productId, payType, "", "", "");
    }

    public void doRechargeCardPay(String productId, String rechargeCardNumber, String rechargeCardPassword) {
        final RechargePayEvent event = new RechargePayEvent();
        Map<String, String> params = new HashMap<String, String>();
        params.put("productid", productId);
        params.put("cardno", rechargeCardNumber);
        params.put("cardpass", rechargeCardPassword);
        HttpRequest request = new HttpRequest(Constants.I_PAY_CARD_PAY, params);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(String data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                notifyEvent(event);
            }
        });
    }

    public void doDnaPay(String productid, String accountno, String mobileno, String name, String idcardno) {
        final DnaPayEvent event = new DnaPayEvent();
        Map<String, String> params = new HashMap<String, String>();
        params.put("productid", productid);
        params.put("accountno", accountno);
        params.put("mobileno", mobileno);
        params.put("name", name);
        params.put("idcardno", idcardno);
        HttpRequest request = new HttpRequest(Constants.I_PAY_DNA, params);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(String data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                notifyEvent(event);
            }
        });
    }
}
