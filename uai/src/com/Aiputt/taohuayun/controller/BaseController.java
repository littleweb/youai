package com.Aiputt.taohuayun.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.notify.Event;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.resources.Constants;

/**
 * 
 * @author Administrator
 *
 */
public class BaseController {

    private static ScheduledExecutorService workThreadPool = Executors.newScheduledThreadPool(5);

    EventCenter mEventCenter;
    protected BaseController() {
        mEventCenter = EventCenter.getInstance();
    }

    protected void notifyEvent(Event event) {
        mEventCenter.send(event);
    }


    protected void runWorkThread(Runnable runnable) {
        workThreadPool.schedule(runnable, 0, TimeUnit.SECONDS);
    }

    protected void runWorkThread(Runnable runnable, int time, TimeUnit unit) {
        workThreadPool.schedule(runnable, time, unit);
    }
    
    /**
     * 很多地方要用到，放到这个里面了
     * @param hi_user_id
     */
    public void actionDazhaohu(String hi_user_id) {
		final ResultEvent event = new ResultEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("hi_user_id", hi_user_id);
		HttpRequest request = new HttpRequest(Constants.I_HI, map);
		HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}
			
			@Override
			public void onSuccess(String data) throws Exception {
				System.out.println("===打招呼返回==="+data);
				event.code = 0;
				event.data = data;
				notifyEvent(event);
			}
		});
	}

}
