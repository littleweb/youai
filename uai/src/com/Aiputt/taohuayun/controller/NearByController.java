package com.Aiputt.taohuayun.controller;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.NearByEvent;
import com.Aiputt.taohuayun.event.SayHiEventEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class NearByController extends BaseController {

    private static NearByController mNearByController = new NearByController();

    private NearByController() {

    }

    public static NearByController getInstance() {
        return mNearByController;
    }

    public void getNearByList(Map<String, String> params) {
        HttpRequest request = new HttpRequest(Constants.I_USER_NEARBY, params);
        final NearByEvent event = new NearByEvent();
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<ArrayList<User>>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(ArrayList<User> userList) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.userList = userList;
                notifyEvent(event);
            }
        });
    }

    /**
     * 打招呼
     *
     * @param user
     */
    public void sayHi(final User user) {
        final SayHiEventEvent event = new SayHiEventEvent();
        Map<String, String> map = new HashMap<String, String>();
        map.put("hi_user_id", user.getUid() + "");
        HttpRequest request = new HttpRequest(Constants.I_HI, map);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(String data) throws Exception {
                event.code = 0;
                user.setIsSayHello(1);
                notifyEvent(event);
            }
        });
    }


    /**
     * @param user
     */
    public void requestPhoto(User user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("hi_user_id", user.getUid() + "");
        HttpRequest request = new HttpRequest(Constants.I_HI, map);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
            }

            @Override
            public void onSuccess(String data) throws Exception {
            }
        });
    }
}
