package com.Aiputt.taohuayun.controller;

import android.content.ContentValues;
import android.text.TextUtils;
import android.view.View;

import com.Aiputt.taohuayun.domain.Disposition;
import com.Aiputt.taohuayun.domain.Hobby;
import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.domain.UserInfoConfig;
import com.Aiputt.taohuayun.domain.UserTipInfo;
import com.Aiputt.taohuayun.event.DeletePhotoEvent;
import com.Aiputt.taohuayun.event.LoadPopInfoEvent;
import com.Aiputt.taohuayun.event.ModifyHeadPhotoEvent;
import com.Aiputt.taohuayun.event.RandomInfoEvent;
import com.Aiputt.taohuayun.event.StopPopInfoEvent;
import com.Aiputt.taohuayun.event.UpdateUserInfoEvent;
import com.Aiputt.taohuayun.event.UploadHeadPhotoEvent;
import com.Aiputt.taohuayun.event.UploadNormalPhotoEvent;
import com.Aiputt.taohuayun.event.UserInfoEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.resources.Constants;

import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.crud.DataSupport;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class UserController extends BaseController {

    private User mUser;

    private static UserController mUserController = new UserController();

    private UserController() {
        if (mUser == null) {
            mUser = new User();
        }
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    public User getUser() {
        return mUser;
    }

    public static UserController getInstance() {
        return mUserController;
    }

    /**
     * 请求他人详细信息
     *
     * @param map
     */
    public void getUserDetail(Map<String, String> map) {
        final UserInfoEvent event = new UserInfoEvent();
        HttpRequest request = new HttpRequest(Constants.I_USER_DETAIL, map);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<User>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(User user) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.mUser = user;
                notifyEvent(event);
            }
        });
    }

    /**
     * 更新用户选择的资料相关信息 index
     *
     * @param userFlag
     */
    private void updateUserFlag(String userFlag) {
        try {
            JSONObject jsonObject = new JSONObject(userFlag);
            for (Map.Entry<Integer, String> entry : UserInfoConfig.USEINFO_CONFIG.entrySet()) {
                UserInfoConfig.saveUserSelectedInfo(entry.getValue(), jsonObject.optString(entry.getValue()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传用户头像
     *
     * @param file
     */
    public void upLoadUserPhoto(boolean isHeadPhoto, File file) {
        if (isHeadPhoto) {
            upLoadHeadPhoto(file);
        } else {
            HttpRequest request = new HttpRequest(Constants.I_USER_ADDIMG);
            request.add("img_data", file);
            final UploadNormalPhotoEvent event = new UploadNormalPhotoEvent();
            HttpUtil.doPost(request, new ULoveHttpResponseHandler<ImageList>() {
                @Override
                protected void onFailure(ULoveException e) {
                    event.msg = e.getServerMsg();
                    notifyEvent(event);
                }

                @Override
                public void onSuccess(ImageList data) throws Exception {
                    event.code = Constants.SERVER_CONNECT_OK;
                    event.imageList = data;
                    notifyEvent(event);
                }
            });
        }
    }

    /**
     * 上传头像
     *
     * @param file
     */
    private void upLoadHeadPhoto(File file) {
        HttpRequest request = new HttpRequest(Constants.I_USER_AVATAR);
        request.add("img_data", file);
        final UploadHeadPhotoEvent event = new UploadHeadPhotoEvent();
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<JSONObject>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(JSONObject data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.user_face = data.getString("user_face");
                event.user_face_big = data.getString("user_face_big");
                event.user_total_balance = data.getString("user_total_balance");
                notifyEvent(event);
            }
        });
    }

    /**
     * 加载自己的个人信息
     */
    public void loadUserInfo() {
        HttpRequest request = new HttpRequest(Constants.I_USER_DETAIL);
        final UserInfoEvent event = new UserInfoEvent();
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<User>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(User user) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.mUser = user;
                setUser(event.mUser);
                updateLocalData(user);
                notifyEvent(event);
            }
        });
    }

    private void updateLocalData(User user) {
        if (!TextUtils.isEmpty(user.getUserFlag())) {
            updateUserFlag(user.getUserFlag());
        }
        if (!TextUtils.isEmpty(user.getUser_hobby())) {
            updateUserHobby(user.getUser_hobby());
        }
        if (!TextUtils.isEmpty(user.getUser_feature())) {
            updateUserFeature(user.getUser_feature());
        }
    }

    /**
     * 更新本地用户个性特征
     *
     * @param features
     */
    private void updateUserFeature(String features) {
        if (features.equals("[]")) {
            return;
        }
        features = features.substring(1, features.length() - 1);
        String[] split = features.split(",");
        ContentValues values = new ContentValues();
        values.put("type", "0");
        Disposition.updateAll(Disposition.class, values, " type = ? ", " 1 ");
        values.put("type", "1");
        for (String s : split) {
            List<Disposition> list = Hobby.where(" description = ? ", s.trim()).find(Disposition.class);
            if (list != null && !list.isEmpty()) {
                DataSupport.update(Disposition.class, values, list.get(0).getId());
            }
        }
    }

    /**
     * 更新本地用户兴趣爱好
     *
     * @param hobbys
     */
    private void updateUserHobby(String hobbys) {
        if (hobbys.equals("[]")) {
            return;
        }
        hobbys = hobbys.substring(1, hobbys.length() - 1);
        String[] split = hobbys.split(",");
        ContentValues values = new ContentValues();
        values.put("type", "0");
        Hobby.updateAll(Hobby.class, values, " type = ? ", " 1 ");
        values.put("type", "1");
        for (String s : split) {
            List<Hobby> list = Hobby.where(" description = ? ", s.trim()).find(Hobby.class);
            if (list != null && !list.isEmpty()) {
                DataSupport.update(Hobby.class, values, list.get(0).getId());
            }
        }
    }

    /**
     * 更新用户信息
     *
     * @param modifyParams
     */
    public void updateUserInfo(Map<String, String> modifyParams) {
        final UpdateUserInfoEvent event = new UpdateUserInfoEvent();
        HttpRequest request = new HttpRequest(Constants.I_EDIT_USER_INFO, modifyParams);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<User>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.code = -1;
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(User data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                setUser(data);
                event.user = data;
                updateLocalData(data);
                notifyEvent(event);
            }
        });
    }

    /**
     * 删除照片
     *
     * @param imageList
     */
    public void deletePhoto(ImageList imageList, View view) {
        final DeletePhotoEvent event = new DeletePhotoEvent();
        event.imageList = imageList;
        event.view = view;
        Map<String, String> params = new HashMap<String, String>();
        params.put("pid", imageList.getPid());
        HttpRequest request = new HttpRequest(Constants.I_DELETE_PHOTO, params);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(String data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                notifyEvent(event);
            }
        });
    }


    public void updateLocationInfo(Map<String, String> params) {
        HttpRequest request = new HttpRequest(Constants.I_UPDATE_USER_LOCATION, params);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<Object>() {
            @Override
            protected void onFailure(ULoveException e) {
            }

            @Override
            public void onSuccess(Object data) throws Exception {
            }
        });
    }

    public void loadRandomInfo() {
        final RandomInfoEvent event = new RandomInfoEvent();
        HttpRequest request = new HttpRequest(Constants.I_USER_RANDOM);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<User>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(User data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.mUser = data;
                notifyEvent(event);
            }
        });
    }

    public void setHeadPhoto(final ImageList headPhoto) {
        final ModifyHeadPhotoEvent event = new ModifyHeadPhotoEvent();
        Map<String, String> params = new HashMap<String, String>();
        params.put("face", headPhoto.getPid());
        HttpRequest request = new HttpRequest(Constants.I_EDIT_USER_INFO, params);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<Object>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(Object data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                mUser.setUser_face(headPhoto.getImg_url_small());
                mUser.setUser_face_big(headPhoto.getImg_url_big());
                mUser.setUser_face_status(1);
                event.imageList = headPhoto;
                notifyEvent(event);
            }
        });
    }

    public void logout() {
        HttpRequest request = new HttpRequest(Constants.I_USER_LOGOUT);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<Object>() {
            @Override
            protected void onFailure(ULoveException e) {

            }

            @Override
            public void onSuccess(Object data) throws Exception {

            }
        });
    }

    /**
     * load pop info
     */
    public void loadPopQa() {
        final LoadPopInfoEvent event = new LoadPopInfoEvent();
        HttpRequest request = new HttpRequest(Constants.I_POP_QA);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<UserTipInfo>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(UserTipInfo data) throws Exception {
                if (data != null) {
                    event.code = Constants.SERVER_CONNECT_OK;
                    event.userTipInfo = data;
                    if (data.getCount() == 0) {
                        EventCenter.getInstance().send(new StopPopInfoEvent());
                    } else if (data.getUser_info() != null) {
                        notifyEvent(event);
                    }
                }
            }
        });
    }

    public void sayHi(String uid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("hi_user_id", uid);
        HttpRequest request = new HttpRequest(Constants.I_HI, map);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
            }

            @Override
            public void onSuccess(String data) throws Exception {
            }
        });
    }
}
