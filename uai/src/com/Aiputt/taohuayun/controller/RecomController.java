package com.Aiputt.taohuayun.controller;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.NearByEvent;
import com.Aiputt.taohuayun.event.RecomEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.event.SearchEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class RecomController extends BaseController {

	private static RecomController mController = new RecomController();

	private RecomController() {

	}

	public static RecomController getInstance() {
		return mController;
	}

	public void getRecomList() {
		HttpRequest request = new HttpRequest(Constants.I_RECOM);
		final RecomEvent event = new RecomEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<ArrayList<User>>() {
					@Override
					protected void onFailure(ULoveException e) {
						event.msg = e.getServerMsg();
						notifyEvent(event);
					}

					@Override
					public void onSuccess(ArrayList<User> userList)
							throws Exception {
						event.code = Constants.SERVER_CONNECT_OK;
						event.userList = userList;
						notifyEvent(event);
					}
				});
	}
	
	
}
