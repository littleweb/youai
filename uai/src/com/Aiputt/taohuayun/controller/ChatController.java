package com.Aiputt.taohuayun.controller;

import com.Aiputt.taohuayun.event.SendMessageEvent;
import com.Aiputt.taohuayun.event.UploadHeadPhotoEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

/**
 * Created by zhonglq on 2015/8/1.
 */
public class ChatController extends BaseController {

    private static ChatController mChatController = new ChatController();
    private ChatController(){}
    public static ChatController getInstance(){
        return mChatController;
    }

    public void sendMessage(String toUserId,String content){
        final SendMessageEvent event = new SendMessageEvent();
        Map<String,String> map = new HashMap<String, String>();
        map.put("to_user_id",toUserId);
        map.put("content",content);
        HttpRequest request = new HttpRequest(Constants.I_SEND_MESSAGE,map);
        HttpUtil.doPost(request,new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.code = e.getServerCode();
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(String data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                notifyEvent(event);
            }
        });

    }
    
    /**
     * 上传头像
     *
     * @param file
     */
    public void upLoadHeadPhoto(File file) {
        HttpRequest request = new HttpRequest(Constants.I_USER_AVATAR);
        request.add("img_data", file);
        final UploadHeadPhotoEvent event = new UploadHeadPhotoEvent();
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<JSONObject>() {
            @Override
            protected void onFailure(ULoveException e) {
                event.msg = e.getServerMsg();
                notifyEvent(event);
            }

            @Override
            public void onSuccess(JSONObject data) throws Exception {
                event.code = Constants.SERVER_CONNECT_OK;
                event.user_face = data.getString("user_face");
                event.user_face_big = data.getString("user_face_big");
                event.user_total_balance = data.getString("user_total_balance");
                notifyEvent(event);
            }
        });
    }

}
