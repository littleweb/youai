package com.Aiputt.taohuayun.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.MainDataEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;

public class MainController extends BaseController{
	
	
	private static MainController mController = new MainController();

	private MainController() {

	}

	public static MainController getInstance() {
		return mController;
	}

	public void actionRegister(String page) {
		final MainDataEvent event = new MainDataEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("page", page);
		HttpRequest request = new HttpRequest(Constants.I_MAIN_PAGE, map);
		HttpUtil.doPost(request, new ULoveHttpResponseHandler<ArrayList<User>>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}

			@Override
			public void onSuccess(ArrayList<User> data) throws Exception {
				event.code = 0;
				event.data = data;
				notifyEvent(event);
			}
		});
	}
	

}
