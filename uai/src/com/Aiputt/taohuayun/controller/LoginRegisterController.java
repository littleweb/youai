package com.Aiputt.taohuayun.controller;

import com.Aiputt.taohuayun.app.ULoveApplication;
import com.Aiputt.taohuayun.domain.RegisterQa;
import com.Aiputt.taohuayun.domain.RegisterResult;
import com.Aiputt.taohuayun.event.LoginEvent;
import com.Aiputt.taohuayun.event.RegisterEvent;
import com.Aiputt.taohuayun.event.RegisterQaEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.DeviceIdUtil;
import com.Aiputt.taohuayun.utils.LocationUtil;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMMessage;
import com.easemob.chat.TextMessageBody;
import com.easemob.exceptions.EaseMobException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginRegisterController extends BaseController {

	private static LoginRegisterController mController = new LoginRegisterController();

	private LoginRegisterController() {

	}

	public static LoginRegisterController getInstance() {
		return mController;
	}

	public void actionRegister(String gender, String age) {
		final RegisterEvent event = new RegisterEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("jpush_id",
				DeviceIdUtil.getDeviceId(ULoveApplication.getContext()));
		map.put("lng",
                LocationUtil.getLongitude());
		map.put("lat",
				LocationUtil.getLatitude());
		map.put("gender", gender);
		map.put("age", age);
		HttpRequest request = new HttpRequest(Constants.I_REGISTER, map);
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<RegisterResult>() {
					@Override
					protected void onFailure(ULoveException e) {
						event.msg = e.getServerMsg();
						notifyEvent(event);
					}

					@Override
					public void onSuccess(RegisterResult data) throws Exception {
						event.code = 0;
						event.data = data;
						notifyEvent(event);
					}
				});
	}

	public void actionLogin(String userNo, String pwd) {
		final LoginEvent event = new LoginEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("user_no", userNo);
		map.put("user_pass", pwd);
		map.put("jpush_id",
				DeviceIdUtil.getDeviceId(ULoveApplication.getContext()));
		HttpRequest request = new HttpRequest(Constants.I_LOGIN, map);
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<RegisterResult>() {
					@Override
					protected void onFailure(ULoveException e) {
						event.msg = e.getServerMsg();
						notifyEvent(event);
					}

					@Override
					public void onSuccess(RegisterResult data) throws Exception {
						event.code = 0;
						event.data = data;
						notifyEvent(event);
					}
				});
	}

	public void actionRegisterQa(String gender, String age) {
		final RegisterQaEvent event = new RegisterQaEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("lng", "24.3324");
		map.put("lat", "lat");
		map.put("gender", gender);
		map.put("age", age);
		HttpRequest request = new HttpRequest(Constants.I_REGISTER_QA, map);
		System.out.println("---------------------------------");
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<ArrayList<RegisterQa>>() {
					@Override
					protected void onFailure(ULoveException e) {
						event.msg = e.getServerMsg();
						notifyEvent(event);
					}

					@Override
					public void onSuccess(ArrayList<RegisterQa> data)
							throws Exception {
						event.code = 0;
						event.data = data;
						notifyEvent(event);
					}
				});
	}

	public void actionAnswerQa(String questionId, String answer, String userid,
			final String userNo) {
		final ResultEvent event = new ResultEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("user_id", userid);
		map.put("question_id", questionId);
		map.put("value", answer);
		HttpRequest request = new HttpRequest(Constants.I_QA_SUBMIT, map);
		HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}

			@Override
			public void onSuccess(String data) throws Exception {
				event.code = 0;
				sendMessageToHX(userNo, data);
				notifyEvent(event);
			}
		});
	}

	/**
	 * after send message to hx
	 */
	private void sendMessageToHX(String userid, String data) {
		EMMessage message = EMMessage.createSendMessage(EMMessage.Type.TXT);
		TextMessageBody txtBody = new TextMessageBody(data);
		message.addBody(txtBody);
		message.setReceipt(userid);
		try {
			EMChatManager.getInstance().sendMessage(message);
		} catch (EaseMobException e) {
			e.printStackTrace();
		}
	}

}
