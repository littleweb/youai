package com.Aiputt.taohuayun.controller;

import android.text.TextUtils;

import com.Aiputt.taohuayun.app.ULoveApplication;
import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.domain.SearchConfig;
import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.event.NearByEvent;
import com.Aiputt.taohuayun.event.ResultEvent;
import com.Aiputt.taohuayun.event.SearchDResultEvent;
import com.Aiputt.taohuayun.event.SearchEvent;
import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;
import com.Aiputt.taohuayun.utils.DeviceIdUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class SearchController extends BaseController {

	private static SearchController mNearByController = new SearchController();

	private SearchController() {

	}

	public static SearchController getInstance() {
		return mNearByController;
	}

	public void getSearchList(Map<String, String> params) {
		Map<String, String> map = new HashMap<String, String>();
		String province = SharePCach.loadStringCach(SearchConfig.SEARCH_POVISION_SEARCH);
		map.put("province",TextUtils.isEmpty(province)?"北京":province);
		
		String city = SharePCach.loadStringCach(SearchConfig.SEARCH_CITY_SEARCH);
		map.put("city",TextUtils.isEmpty(city)?"市区":city);
		
		String age = SharePCach.loadStringCach(SearchConfig.SEARCH_AGE_SEARCH);
		map.put("age", TextUtils.isEmpty(age)?"18-30":age);
		
		String height = SharePCach.loadStringCach(SearchConfig.SEARCH_HEIGHT_SEARCH);
		map.put("height", TextUtils.isEmpty(height)?"160-170":height);
		HttpRequest request = new HttpRequest(Constants.I_USER_SEARCH,map);
		final SearchEvent event = new SearchEvent();
		HttpUtil.doPost(request,
				new ULoveHttpResponseHandler<ArrayList<User>>() {
					@Override
					protected void onFailure(ULoveException e) {
						event.msg = e.getServerMsg();
						notifyEvent(event);
					}

					@Override
					public void onSuccess(ArrayList<User> userList)
							throws Exception {
						event.code = Constants.SERVER_CONNECT_OK;
						event.userList = userList;
						notifyEvent(event);
					}
				});
	}
	
	   /**
     * 很多地方要用到，放到这个里面了
     * @param hi_user_id
     */
    public void actionDazhaohu(String hi_user_id) {
		final SearchDResultEvent event = new SearchDResultEvent();
		Map<String, String> map = new HashMap<String, String>();
		map.put("hi_user_id", hi_user_id);
		HttpRequest request = new HttpRequest(Constants.I_HI, map);
		HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
			@Override
			protected void onFailure(ULoveException e) {
				event.msg = e.getServerMsg();
				notifyEvent(event);
			}
			
			@Override
			public void onSuccess(String data) throws Exception {
				System.out.println("===打招呼返回==="+data);
				event.code = 0;
				event.data = data;
				notifyEvent(event);
			}
		});
	}
	
	
	
}
