package com.Aiputt.taohuayun.controller;

import com.Aiputt.taohuayun.http.HttpRequest;
import com.Aiputt.taohuayun.http.HttpUtil;
import com.Aiputt.taohuayun.http.ULoveException;
import com.Aiputt.taohuayun.http.ULoveHttpResponseHandler;
import com.Aiputt.taohuayun.resources.Constants;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class InitController extends BaseController {


    private static InitController mUserController = new InitController();

    private InitController() {
    }


    public static InitController getInstance() {
        return mUserController;
    }

    /**
     * 请求他人详细信息
     *
     */
    public void initDevice() {
        HttpRequest request = new HttpRequest(Constants.I_INIT_DEVICE);
        HttpUtil.doPost(request, new ULoveHttpResponseHandler<String>() {
            @Override
            protected void onFailure(ULoveException e) {
            
            }

            @Override
            public void onSuccess(String user) throws Exception {
                
            }
        });
    }
    
}
