package com.Aiputt.taohuayun.http;


/**
 * 
 * @author Administrator
 * 
 */
public class ULoveException extends Exception {
	private ServerCode code;
	private Throwable mThrowable;
    private int serverCode;
    private String msg;


    public ULoveException(int serverCode,String msg,Throwable throwable){
        super(msg,throwable);
        this.serverCode = serverCode;
        this.msg = msg;
        this.mThrowable = throwable;
    }

    public int getServerCode(){
        return serverCode;
    }

	public ULoveException(ServerCode code, Throwable throwable) {
		super(code.toString(), throwable);
		this.code = code;
		mThrowable = throwable;
	}

	@Override
	public void printStackTrace() {
		if (mThrowable != null) {
			mThrowable.printStackTrace();
		}
	}

    public String getServerMsg(){
        return msg;
    }

	public ServerCode getCode() {
		return code;
	}
}
