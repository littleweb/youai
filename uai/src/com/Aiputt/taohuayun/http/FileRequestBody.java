package com.Aiputt.taohuayun.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.internal.Util;

import java.io.File;
import java.io.IOException;

import okio.BufferedSink;
import okio.Okio;
import okio.Source;

/**
 * 
 * @author Administrator
 *
 */
public class FileRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

    private final File file;
    private final MediaType mediaType;
    private ProgressHandler mHandler;


    public FileRequestBody(File file, MediaType mediaType) {
        this.file = file;
        this.mediaType = mediaType;
    }

    @Override
    public long contentLength() {
        return file.length();
    }

    @Override
    public MediaType contentType() {
        return this.mediaType;
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {
            source = Okio.source(file);
            long total = 0;
            long read;

            long fileLength = file.length();
            while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                total += read;
                sink.flush();
                if (mHandler != null) {
                    mHandler.notify(file.getName(), (int) total, (int) fileLength);
                }
            }
        } finally {
            Util.closeQuietly(source);
        }
    }

    /***
     * 设置进度条
     * **/
    public void setProgressListener(ProgressListener listener) {
        if (mHandler != null) {
            mHandler.mListener = listener;
        } else {
            mHandler = new ProgressHandler(listener);
        }
    }

    public static class ProgressHandler extends Handler {
        private final  static int MESSAGE_PROGRESS = 1001;
        private ProgressListener mListener;
        public ProgressHandler(ProgressListener listener) {
            super(Looper.getMainLooper());
            mListener = listener;
        }

        public void notify(String filename, int writedByte, int total) {
            Message message = obtainMessage(MESSAGE_PROGRESS);
            message.arg1 = writedByte;
            message.arg2 = total;
            message.obj = filename;
            sendMessage(message);
        }

        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            if (what == MESSAGE_PROGRESS) {
                if (this.mListener != null) {
                    String name = (String)msg.obj;
                    this.mListener.onProgress(name, msg.arg1, msg.arg2);
                }
            } else {
                super.handleMessage(msg);
            }
        }
    };}
