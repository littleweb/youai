package com.Aiputt.taohuayun.http;

/**
 * 
 * @author Administrator
 *
 */
public interface ProgressListener {
    void onProgress(String name, long bytesWritten, long totalSize);
}
