package com.Aiputt.taohuayun.http;

/**
 * 
 * @author Administrator
 * 
 */
public enum ServerCode {
	UNKNOWN(-1, "未知错误"), SUCCESS(0, "成功"), PARSE_ERROR(100, "返回值解析出错!"), NET_IO_ERROR(
			101, "网络通讯出错！");

	private int code = 0;
	private String message = "";

	private ServerCode(int errorCode, String message) {
		this.code = errorCode;
		this.message = message;
	}

	public static ServerCode valueOf(int errorCode) {
		for (ServerCode code : values()) {
			if (code.code == errorCode) {
				return code;
			}
		}
		return UNKNOWN;
	}

	public int code() {
		return code;
	}

	@Override
	public String toString() {
		return message;
	}
}
