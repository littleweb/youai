package com.Aiputt.taohuayun.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;

public abstract class ULoveHttpResponseHandler<T> extends TypeToken<T>
		implements Callback, ProgressListener {

	private static final String LOG_TAG = "ULoveHttpResponseHandler";
	
	private MainHandler<T> mHandler;
	// 通用错误编号
	protected static final int FAILURE = 1010101;
	protected static final int SUCCESS = 1010102;
	
	public static final String DEFAULT_CHARSET = "UTF-8";
	private String responseCharset = DEFAULT_CHARSET;

	protected ULoveHttpResponseHandler() {
		super();
		mHandler = new MainHandler<T>(this);
	}

	public void setCharset(final String charset) {
		this.responseCharset = charset;
	}

	public String getCharset() {
		return this.responseCharset == null ? DEFAULT_CHARSET
				: this.responseCharset;
	}


	/**
	 * Fired when the request progress, override to handle in your own code
	 * 
	 * @param bytesWritten
	 *            offset from start of file
	 * @param totalSize
	 *            total size of file
	 */
	@Override
	public void onProgress(String name, long bytesWritten, long totalSize) {
		Log.v(LOG_TAG, String.format("Progress %d from %d (%2.0f%%)",
				bytesWritten, totalSize,
				(totalSize > 0) ? (bytesWritten * 1.0 / totalSize) * 100 : -1));
	}

	@Override
	public final void onResponse(Response response) throws IOException {
		try {
			T result = (T) HttpUtil.handleResponse(response, getType());
			Message message = mHandler.obtainMessage(SUCCESS);
			message.obj = result;
			mHandler.sendMessage(message);
		} catch (ULoveException e) {
			Message message = mHandler.obtainMessage(FAILURE);
			message.obj = e;
			mHandler.sendMessage(message);
		} catch (JSONException e) {
			Message message = mHandler.obtainMessage(FAILURE);
			message.obj = new ULoveException(ServerCode.PARSE_ERROR, e);
			mHandler.sendMessage(message);
		}
	}

	@Override
	public final void onFailure(Request request, IOException e) {
		Message message = mHandler.obtainMessage(FAILURE);
		message.obj = new ULoveException(ServerCode.NET_IO_ERROR, e);
		mHandler.sendMessage(message);
	}

	protected abstract void onFailure(ULoveException e);

	public abstract void onSuccess(T data) throws Exception;

	public static class MainHandler<T> extends Handler {
		private ULoveHttpResponseHandler<T> mHandler;

		public MainHandler(ULoveHttpResponseHandler<T> responseHandler) {
			super(Looper.getMainLooper());
			mHandler = responseHandler;
		}

		@Override
		public void handleMessage(Message msg) {
			int what = msg.what;
			switch (what) {
			case FAILURE:
				ULoveException e = (ULoveException) msg.obj;
				mHandler.onFailure(e);
				break;
			case SUCCESS:
				T entity = (T) msg.obj;
				try {
					mHandler.onSuccess(entity);
				} catch (Exception e1) {
					mHandler.onFailure(new ULoveException(ServerCode.UNKNOWN,
							e1));
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}
}
