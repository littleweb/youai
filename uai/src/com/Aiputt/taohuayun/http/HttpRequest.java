package com.Aiputt.taohuayun.http;

import com.Aiputt.taohuayun.resources.Constants;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fanxi on 4/29/15.
 * 网络请求封装类
 */
public class HttpRequest extends Request.Builder {
    public static final MediaType MEDIA_PNG = MediaType.parse("image/png");
    public static final MediaType MEDIA_JPEG = MediaType.parse("image/jpeg");
    public static final MediaType MEDIA_OCTET = MediaType.parse("application/octet-stream");

    private final String mUrl ;

    private Map<String, File> mFiles = new HashMap<String, File>();

    private Map<String, String> params = null;

    public HttpRequest(String url) {
        this(url, null, false);
    }

    public HttpRequest(String url, Map<String, String> params) {
        this(url, params, false);
    }

    public HttpRequest(String url, Map<String, String> params, boolean fullUrl) {
        super();
        mUrl = fullUrl ? url : Constants.API_URL + url;
        this.params = params == null ? new HashMap<String, String>() : params;
    }

    public Request build() {
        url(mUrl);

        if (!mFiles.isEmpty()) {
            MultipartBuilder mBuilder = new MultipartBuilder();
            mBuilder.type(MultipartBuilder.FORM);

            String data = HttpUtil.encodeHttpParams(params);
            mBuilder.addFormDataPart("data", data);

            for (String key : mFiles.keySet()) {
                File file = mFiles.get(key);
                MediaType type = getFileMediaType(file);
                FileRequestBody fileBody = new FileRequestBody(file, type);
                String name = file.getName();
                fileBody.setProgressListener(mListener);
                mBuilder.addFormDataPart(key, name, fileBody);
            }

            post(mBuilder.build());
        } else {
            FormEncodingBuilder textBuilder = new FormEncodingBuilder();
            String data = HttpUtil.encodeHttpParams(params);
            textBuilder.add("data", data);

            post(textBuilder.build());
        }

        return super.build();
    }

    /**
     * 添加文件上传
     */
    public void add(String key, File file) {
        mFiles.put(key, file);
    }

    /**
     * Add new key-value pair.
     */
    public void add(String name, String value) {
        if (params == null) {
            params = new HashMap<String, String>();
        }
        params.put(name, value);
    }

    private ProgressListener mListener;

    void setProgressListener(ProgressListener progressListener) {
        this.mListener = progressListener;
    }

    /**
     * 获取文件的类型
     * 更多类型请参考如下连接
     * <url>http://tool.oschina.net/commons</url>
     */
    public static MediaType getFileMediaType(File file) {
        String filename = file.getName();
        int index = filename.lastIndexOf(".");

        String suffix = null;
        if (index > 0) {
            suffix = filename.substring(index + 1);
        }

        if ("jpg".equalsIgnoreCase(suffix)) {
            return MEDIA_JPEG;
        } else if ("png".equalsIgnoreCase(suffix)) {
            return MEDIA_PNG;
        } else {
            return MEDIA_OCTET;
        }
    }
}

