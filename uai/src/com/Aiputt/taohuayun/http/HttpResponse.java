package com.Aiputt.taohuayun.http;

/**
 * 网络请求返回
 * 
 * @author Administrator
 * 
 * @param <T>
 */
public class HttpResponse<T> {
	public ServerCode code = ServerCode.UNKNOWN;
	public T data = null;

	HttpResponse(ServerCode code, T data) {
		this.code = code;
		this.data = data;
	}
}
