package com.Aiputt.taohuayun.http;

import android.text.TextUtils;

import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.utils.AESUtils;
import com.Aiputt.taohuayun.utils.CommonUtil;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HttpUtil {

    public static final MediaType JSON = MediaType
            .parse("application/json; charset=utf-8");

    private static final OkHttpClient client = new OkHttpClient();

    private static final int TIME_OUT = 30 * 1000;

    private static final HostnameVerifier trustAllHosts = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType)
                throws CertificateException {
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType)
                throws CertificateException {
        }
    }};

    static {
        try {
            client.setConnectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
            client.setReadTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, null);
            SSLSocketFactory factory = sc.getSocketFactory();
            client.setSslSocketFactory(factory);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> void doPost(HttpRequest request,
                                  ULoveHttpResponseHandler<T> callback) {
        request.setProgressListener(callback);
        client.newCall(request.build()).enqueue(callback);
    }

    public static <T> HttpResponse<T> doPost(HttpRequest httpRequest,
                                             TypeToken<T> token) {
        return doPost(httpRequest, token.getType());
    }

    public static <T> HttpResponse<T> doPost(HttpRequest httpRequest, Type clazz) {
        HttpResponse<T> httpResponse;
        try {
            Response response = client.newCall(httpRequest.build()).execute();
            T entity = handleResponse(response, clazz);
            return new HttpResponse<T>(ServerCode.SUCCESS, entity);
        } catch (IOException e) {
            httpResponse = new HttpResponse<T>(ServerCode.NET_IO_ERROR, null);
        } catch (JSONException e) {
            httpResponse = new HttpResponse<T>(ServerCode.PARSE_ERROR, null);
        } catch (ULoveException e) {
            httpResponse = new HttpResponse<T>(e.getCode(), null);
        }

        return httpResponse;
    }

    /**
     * 解析服务端返回值，根据clazz类型解析出对应的java对象实例， decrypted 标识返回值是否加过密
     *
     * @param response okHttp的返回值
     * @param clazz    , 返回值解析的类型
     *                 *
     */
    public static <T> T handleResponse(Response response, Type clazz) throws IOException, JSONException,
            ULoveException {

        ServerCode code = ServerCode.NET_IO_ERROR;
        if (response.isSuccessful()) {
            String data = response.body().string();
            if (data != null) {
                JSONObject jsonReturn = new JSONObject(data);
                int errNo = jsonReturn.optInt("code"); // 系统默认编号
                code = ServerCode.valueOf(errNo);
                if (code == ServerCode.SUCCESS) {
                    T returnEntity = null;
                    String realData = jsonReturn.optString("data");
                    if (clazz == String.class) {
                        returnEntity = (T) realData;
                    } else if (!"[]".equals(realData) && !"{}".equals(realData)) {
                        if (clazz == JSONObject.class) {
                            returnEntity = (T) jsonReturn.getJSONObject("data");
                        } else if (clazz == JSONArray.class) {
                            returnEntity = ((T) jsonReturn.getJSONArray("data"));
                        } else {
                            returnEntity = CommonUtil.fromJson(realData, clazz);
                        }
                    }

                    return returnEntity;
                } else {
                    String errorMsg = jsonReturn.optString("msg");
                    throw new ULoveException(errNo, errorMsg, null);
                }

            } else {
                code = ServerCode.PARSE_ERROR;
            }
        }

        throw new ULoveException(code, null);
    }

    public static Map<String, String> encodeParams(Map<String, String> params) {

        String secret = AESUtils.getSignature(params);
        if (!TextUtils.isEmpty(secret)) {
            params.put("token", secret);
        }
        return params;
    }

    /**
     * 提交参数 V2.0
     *
     * @param params
     * @return
     */
    public static String encodeHttpParams(Map<String, String> params) {
        if (params == null) {
            params = new HashMap<String, String>();
        }

        params.put("device_id", CommonUtil.getUUID());
        params.put("channel_no", CommonUtil.getCustomChannelInfo());
        params.put("ver", CommonUtil.getVersionName());
        params.put("os", "android");
        String token = SharePCach.loadStringCach(SharePCach.TOKEN);
        if (!TextUtils.isEmpty(token)) {
            params.put("token", token);
        }
        JSONObject jsonObject = new JSONObject(params);
        String strParams = jsonObject.toString();
        return strParams;
    }

}
