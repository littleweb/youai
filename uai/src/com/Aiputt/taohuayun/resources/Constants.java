package com.Aiputt.taohuayun.resources;


import android.os.Environment;

import com.Aiputt.taohuayun.domain.Province;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator 放置一些常量以及静态变量
 */
public class Constants {

    public static final int SERVER_CONNECT_OK = 0;
    public static boolean IS_NEED_REFLASH = false;
    public static String mUserFace;
    public static int otherUserId;
    public static String otherUserFace;
    public static boolean mNeedSearch = false;


//    public static final String API_URL = "http://lxy.aiyingta.com";
        public static final String API_URL = "http://app.aiputt.com";
    public static final String API_HOST_VERSION = "/api/v1";

    //用户详情
    public static final String I_INIT_DEVICE = API_HOST_VERSION + "/device/active";
    public static final String I_POP_QA = API_HOST_VERSION + "/recom/popqa";
    public static final String I_USER_DETAIL = API_HOST_VERSION + "/user/detail";
    public static final String I_EDIT_USER_INFO = API_HOST_VERSION + "/user/edit";
    public static final String I_USER_SEND = API_HOST_VERSION + "/user/suggestion";
    public static final String I_USER_RANDOM = API_HOST_VERSION + "/user/random";
    public static final String I_PAY_DNA = API_HOST_VERSION + "/pay/dna";
    public static final String I_PAY_CARD_PAY = API_HOST_VERSION + "/pay/cardpay";
    //删除照片
    public static final String I_DELETE_PHOTO = API_HOST_VERSION + "/user/delimg";
    //支付获取订单号
    public static final String I_GET_ORDER_NO = API_HOST_VERSION + "/order/get";
    public static final String I_UPDATE_USER_LOCATION = API_HOST_VERSION + "/user/lbs";
    public static final String I_REGISTER = API_HOST_VERSION + "/user/register";
    public static final String I_REGISTER_QA = API_HOST_VERSION + "/recom/qa";
    public static final String I_QA_SUBMIT = API_HOST_VERSION + "/user/qasubmit";
    public static final String I_MAIN_PAGE = API_HOST_VERSION + "/recom/index";
    public static final String I_HI = API_HOST_VERSION + "/message/hi";
    public static final String I_MAIL_NOTIFY = API_HOST_VERSION + "/msg/notice";
    public static final String I_MAIL_RENGONG = API_HOST_VERSION + "/msg/getsug";
    public static final String I_MAIL_PRIVATE_MSG = API_HOST_VERSION + "/message/list";
    public static final String I_RECOM = API_HOST_VERSION + "/recom/daily";
    public static final String I_SEND_MESSAGE = API_HOST_VERSION + "/message/send";
    //搜索
    public static final String I_USER_SEARCH = API_HOST_VERSION + "/user/search";
    //附近的人
    public static final String I_USER_NEARBY = API_HOST_VERSION + "/user/near";
    //修改头像
    public static final String I_USER_AVATAR = API_HOST_VERSION + "/user/avatar";
    //上传照片
    public static final String I_USER_ADDIMG = API_HOST_VERSION + "/user/addimg";
    public static final String I_USER_LOGOUT = API_HOST_VERSION + "/device/unbinddevice";


    //序列话的省份列表
    public static List<Province> PROVINCELIST = null;

    public static String PICPATH = Environment.getExternalStorageDirectory()
            + "/taohuayun/pic/";

    public static String FILEPATH = Environment.getExternalStorageDirectory()
            + "/taohuayun/cache/";

    /**
     * 图片名称
     *
     * @return
     */
    public static String imageName() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date date = new Date();
        String time = format.format(date);
        String imageName = "IMG_" + time + ".jpg";
        return imageName;
    }

    public static final String I_LOGIN = API_HOST_VERSION + "/user/login";
    public static final String I_CLEAR_PRIVATE_MSG = API_HOST_VERSION + "/message/clean";


}
