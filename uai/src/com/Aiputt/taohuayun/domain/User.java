package com.Aiputt.taohuayun.domain;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/7/16.
 */
public class User extends DataSupport implements Serializable {
    private int uid;
    private String user_no;
    private String user_nick;
    private String user_face;//头像url 小图
    private int user_gender;//0男 1女
    private String user_age;
    private String user_birthday;
    private String user_xz;
    private String user_province;
    private String user_location;
    private String user_height;
    private String user_weight;
    private String user_income;
    private int is_vip;//0 非 1是
    private String user_description;
    private String user_blood;
    private String user_hobby;
    private String user_feature;
    private String user_education;
    private String user_job;
    private String user_marriage;
    private String user_housing;
    private String user_yidi;
    private int sys_cnt;//暂无
    private String img_url;//头像大图
    private String distance;
    private int isSayHello;//0 没有 1 打过招呼
    private ArrayList<ImageList> img_list;//图片合集
    private String user_face_big;
    private String user_flag;
    private String user_live;
    private String user_navtie;
    private int isme;//1 y 0 n
    private int user_face_status;//-1审核失败, 0:未上传头像, 1待审核
    private String img_cnt;
    private String user_total_balance;//doub count
    private String surplus_day;//vip last day
    
    private int unread_cnt;
    private String dialog_id;
    private String user_have_baby;
    private String user_premarital_sex;
    private String user_charm;

    private String ta_province;
    private String ta_height;
    private String ta_age;
    private String ta_education;
    private String ta_income;

    private int score;

    private String partner_conditions;

    private List<User> recent_visit;

    public String getUser_flag() {
        return user_flag;
    }

    public void setUser_flag(String user_flag) {
        this.user_flag = user_flag;
    }

    public List<User> getRecent_visit() {
        return recent_visit;
    }

    public void setRecent_visit(List<User> recent_visit) {
        this.recent_visit = recent_visit;
    }

    public String getPartner_conditions() {
        return partner_conditions;
    }

    public void setPartner_conditions(String partner_conditions) {
        this.partner_conditions = partner_conditions;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getUser_have_baby() {
        return user_have_baby;
    }

    public void setUser_have_baby(String user_have_baby) {
        this.user_have_baby = user_have_baby;
    }

    public String getUser_premarital_sex() {
        return user_premarital_sex;
    }

    public void setUser_premarital_sex(String user_premarital_sex) {
        this.user_premarital_sex = user_premarital_sex;
    }

    public String getUser_charm() {
        return user_charm;
    }

    public void setUser_charm(String user_charm) {
        this.user_charm = user_charm;
    }

    public String getTa_province() {
        return ta_province;
    }

    public void setTa_province(String ta_province) {
        this.ta_province = ta_province;
    }

    public String getTa_height() {
        return ta_height;
    }

    public void setTa_height(String ta_height) {
        this.ta_height = ta_height;
    }

    public String getTa_age() {
        return ta_age;
    }

    public void setTa_age(String ta_age) {
        this.ta_age = ta_age;
    }

    public String getTa_education() {
        return ta_education;
    }

    public void setTa_education(String ta_education) {
        this.ta_education = ta_education;
    }

    public String getTa_income() {
        return ta_income;
    }

    public void setTa_income(String ta_income) {
        this.ta_income = ta_income;
    }

    public int getUnread_cnt() {
		return unread_cnt;
	}

	public void setUnread_cnt(int unread_cnt) {
		this.unread_cnt = unread_cnt;
	}

	public String getDialog_id() {
		return dialog_id;
	}

	public void setDialog_id(String dialog_id) {
		this.dialog_id = dialog_id;
	}

	public String getSurplus_day() {
        return surplus_day;
    }

    public void setSurplus_day(String surplus_day) {
        this.surplus_day = surplus_day;
    }

    public int getIsme() {
        return isme;
    }

    public void setIsme(int isme) {
        this.isme = isme;
    }



    public int getUser_face_status() {
        return user_face_status;
    }

    public void setUser_face_status(int user_face_status) {
        this.user_face_status = user_face_status;
    }

    public String getUser_live() {
        return user_live;
    }

    public void setUser_live(String user_live) {
        this.user_live = user_live;
    }

    public String getUser_navtie() {
        return user_navtie;
    }

    public void setUser_navtie(String user_navtie) {
        this.user_navtie = user_navtie;
    }

    public String getUserFlag() {
        return user_flag;
    }

    public void setUserFlag(String userFlag) {
        this.user_flag = userFlag;
    }

    public String getUser_face_big() {
        return user_face_big;
    }

    public void setUser_face_big(String user_face_big) {
        this.user_face_big = user_face_big;
    }

    public ArrayList<ImageList> getImg_list() {
        return img_list;
    }

    public void setImg_list(ArrayList<ImageList> img_list) {
        this.img_list = img_list;
    }

    public int getIsSayHello() {
        return isSayHello;
    }

    public void setIsSayHello(int isSayHello) {
        this.isSayHello = isSayHello;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUser_no() {
        return user_no;
    }

    public void setUser_no(String user_no) {
        this.user_no = user_no;
    }

    public String getUser_nick() {
        return user_nick;
    }

    public void setUser_nick(String user_nick) {
        this.user_nick = user_nick;
    }

    public String getUser_face() {
        return user_face;
    }

    public void setUser_face(String user_face) {
        this.user_face = user_face;
    }

    public int getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(int user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_age() {
        return user_age;
    }

    public void setUser_age(String user_age) {
        this.user_age = user_age;
    }

    public String getUser_birthday() {
        return user_birthday;
    }

    public void setUser_birthday(String user_birthday) {
        this.user_birthday = user_birthday;
    }

    public String getUser_xz() {
        return user_xz;
    }

    public void setUser_xz(String user_xz) {
        this.user_xz = user_xz;
    }

    public String getUser_province() {
        return user_province;
    }

    public void setUser_province(String user_province) {
        this.user_province = user_province;
    }

    public String getUser_location() {
        return user_location;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public String getUser_height() {
        return user_height;
    }

    public void setUser_height(String user_height) {
        this.user_height = user_height;
    }

    public String getUser_weight() {
        return user_weight;
    }

    public void setUser_weight(String user_weight) {
        this.user_weight = user_weight;
    }

    public String getUser_income() {
        return user_income;
    }

    public void setUser_income(String user_income) {
        this.user_income = user_income;
    }

    public int getIs_vip() {
        return is_vip;
    }

    public void setIs_vip(int is_vip) {
        this.is_vip = is_vip;
    }

    public String getUser_description() {
        return user_description;
    }

    public void setUser_description(String user_description) {
        this.user_description = user_description;
    }

    public String getUser_blood() {
        return user_blood;
    }

    public void setUser_blood(String user_blood) {
        this.user_blood = user_blood;
    }

    public String getUser_hobby() {
        return user_hobby;
    }

    public void setUser_hobby(String user_hobby) {
        this.user_hobby = user_hobby;
    }

    public String getUser_feature() {
        return user_feature;
    }

    public void setUser_feature(String user_feature) {
        this.user_feature = user_feature;
    }

    public String getUser_education() {
        return user_education;
    }

    public void setUser_education(String user_education) {
        this.user_education = user_education;
    }

    public String getUser_job() {
        return user_job;
    }

    public void setUser_job(String user_job) {
        this.user_job = user_job;
    }

    public String getUser_marriage() {
        return user_marriage;
    }

    public void setUser_marriage(String user_marriage) {
        this.user_marriage = user_marriage;
    }

    public String getUser_housing() {
        return user_housing;
    }

    public void setUser_housing(String user_housing) {
        this.user_housing = user_housing;
    }

    public String getUser_yidi() {
        return user_yidi;
    }

    public void setUser_yidi(String user_yidi) {
        this.user_yidi = user_yidi;
    }

    public int getSys_cnt() {
        return sys_cnt;
    }

    public void setSys_cnt(int sys_cnt) {
        this.sys_cnt = sys_cnt;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getImg_cnt() {
        return img_cnt;
    }

    public void setImg_cnt(String img_cnt) {
        this.img_cnt = img_cnt;
    }

    public String getUser_total_balance() {
        return user_total_balance;
    }

    public void setUser_total_balance(String user_total_balance) {
        this.user_total_balance = user_total_balance;
    }

}
