package com.Aiputt.taohuayun.domain;

import org.litepal.crud.DataSupport;

public class RegisterResult extends DataSupport{

	private String token;
	private String user_no;
	private String user_pass;
	private String easemod_password;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUser_no() {
		return user_no;
	}
	public void setUser_no(String user_no) {
		this.user_no = user_no;
	}
	public String getUser_pass() {
		return user_pass;
	}
	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}
	public String getEasemod_password() {
		return easemod_password;
	}
	public void setEasemod_password(String easemod_password) {
		this.easemod_password = easemod_password;
	}
	
	
}
