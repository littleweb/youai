package com.Aiputt.taohuayun.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhonglq on 2015/9/7.
 */
public class UserTipInfo implements Serializable{

    /**
     * user_info : {"uid":"1084","user_no":"24005083","user_nick":"柯楠","user_face":"http://img.aiputt.com/100x100/1124YSANucqnRiUrkgjGAOJGZYNSU.jpg","user_gender":"1","user_age":"28","user_birthday":"1987-09-21","user_hobby":null,"user_feature":null,"user_xz":"处女座","user_province":"北京","user_city":"市区","user_district":"朝阳","user_location":"朝阳","user_height":"160","user_weight":"103","user_income":"6000-10000元","is_vip":"0","user_face_status":"2","sys_cnt":0,"dialog_cnt":0,"user_face_big":"http://img.aiputt.com/500x500/1124YSANucqnRiUrkgjGAOJGZYNSU.jpg","img_url":"http://img.aiputt.com/500x500/1124YSANucqnRiUrkgjGAOJGZYNSU.jpg","img_cnt":8,"user_live":"北京市区朝阳","isSayHello":0}
     * qa_info : {"question":"你愿意陪我看日落么？","options":["不愿意","我想想","我愿意"]}
     * count : 4
     * time : 1441636862
     * per_time : 40
     */

    private UserInfoEntity user_info;
    private QaInfoEntity qa_info;
    private int count;
    private int time;
    private int per_time;

    public void setUser_info(UserInfoEntity user_info) {
        this.user_info = user_info;
    }

    public void setQa_info(QaInfoEntity qa_info) {
        this.qa_info = qa_info;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setPer_time(int per_time) {
        this.per_time = per_time;
    }

    public UserInfoEntity getUser_info() {
        return user_info;
    }

    public QaInfoEntity getQa_info() {
        return qa_info;
    }

    public int getCount() {
        return count;
    }

    public int getTime() {
        return time;
    }

    public int getPer_time() {
        return per_time;
    }

    public static class UserInfoEntity {
        /**
         * uid : 1084
         * user_no : 24005083
         * user_nick : 柯楠
         * user_face : http://img.aiputt.com/100x100/1124YSANucqnRiUrkgjGAOJGZYNSU.jpg
         * user_gender : 1
         * user_age : 28
         * user_birthday : 1987-09-21
         * user_hobby : null
         * user_feature : null
         * user_xz : 处女座
         * user_province : 北京
         * user_city : 市区
         * user_district : 朝阳
         * user_location : 朝阳
         * user_height : 160
         * user_weight : 103
         * user_income : 6000-10000元
         * is_vip : 0
         * user_face_status : 2
         * sys_cnt : 0
         * dialog_cnt : 0
         * user_face_big : http://img.aiputt.com/500x500/1124YSANucqnRiUrkgjGAOJGZYNSU.jpg
         * img_url : http://img.aiputt.com/500x500/1124YSANucqnRiUrkgjGAOJGZYNSU.jpg
         * img_cnt : 8
         * user_live : 北京市区朝阳
         * isSayHello : 0
         */

        private String uid;
        private String user_no;
        private String user_nick;
        private String user_face;
        private String user_gender;
        private String user_age;
        private String user_birthday;
        private Object user_hobby;
        private Object user_feature;
        private String user_xz;
        private String user_province;
        private String user_city;
        private String user_district;
        private String user_location;
        private String user_height;
        private String user_weight;
        private String user_income;
        private String is_vip;
        private String user_face_status;
        private int sys_cnt;
        private int dialog_cnt;
        private String user_face_big;
        private String img_url;
        private int img_cnt;
        private String user_live;
        private int isSayHello;

        public void setUid(String uid) {
            this.uid = uid;
        }

        public void setUser_no(String user_no) {
            this.user_no = user_no;
        }

        public void setUser_nick(String user_nick) {
            this.user_nick = user_nick;
        }

        public void setUser_face(String user_face) {
            this.user_face = user_face;
        }

        public void setUser_gender(String user_gender) {
            this.user_gender = user_gender;
        }

        public void setUser_age(String user_age) {
            this.user_age = user_age;
        }

        public void setUser_birthday(String user_birthday) {
            this.user_birthday = user_birthday;
        }

        public void setUser_hobby(Object user_hobby) {
            this.user_hobby = user_hobby;
        }

        public void setUser_feature(Object user_feature) {
            this.user_feature = user_feature;
        }

        public void setUser_xz(String user_xz) {
            this.user_xz = user_xz;
        }

        public void setUser_province(String user_province) {
            this.user_province = user_province;
        }

        public void setUser_city(String user_city) {
            this.user_city = user_city;
        }

        public void setUser_district(String user_district) {
            this.user_district = user_district;
        }

        public void setUser_location(String user_location) {
            this.user_location = user_location;
        }

        public void setUser_height(String user_height) {
            this.user_height = user_height;
        }

        public void setUser_weight(String user_weight) {
            this.user_weight = user_weight;
        }

        public void setUser_income(String user_income) {
            this.user_income = user_income;
        }

        public void setIs_vip(String is_vip) {
            this.is_vip = is_vip;
        }

        public void setUser_face_status(String user_face_status) {
            this.user_face_status = user_face_status;
        }

        public void setSys_cnt(int sys_cnt) {
            this.sys_cnt = sys_cnt;
        }

        public void setDialog_cnt(int dialog_cnt) {
            this.dialog_cnt = dialog_cnt;
        }

        public void setUser_face_big(String user_face_big) {
            this.user_face_big = user_face_big;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }

        public void setImg_cnt(int img_cnt) {
            this.img_cnt = img_cnt;
        }

        public void setUser_live(String user_live) {
            this.user_live = user_live;
        }

        public void setIsSayHello(int isSayHello) {
            this.isSayHello = isSayHello;
        }

        public String getUid() {
            return uid;
        }

        public String getUser_no() {
            return user_no;
        }

        public String getUser_nick() {
            return user_nick;
        }

        public String getUser_face() {
            return user_face;
        }

        public String getUser_gender() {
            return user_gender;
        }

        public String getUser_age() {
            return user_age;
        }

        public String getUser_birthday() {
            return user_birthday;
        }

        public Object getUser_hobby() {
            return user_hobby;
        }

        public Object getUser_feature() {
            return user_feature;
        }

        public String getUser_xz() {
            return user_xz;
        }

        public String getUser_province() {
            return user_province;
        }

        public String getUser_city() {
            return user_city;
        }

        public String getUser_district() {
            return user_district;
        }

        public String getUser_location() {
            return user_location;
        }

        public String getUser_height() {
            return user_height;
        }

        public String getUser_weight() {
            return user_weight;
        }

        public String getUser_income() {
            return user_income;
        }

        public String getIs_vip() {
            return is_vip;
        }

        public String getUser_face_status() {
            return user_face_status;
        }

        public int getSys_cnt() {
            return sys_cnt;
        }

        public int getDialog_cnt() {
            return dialog_cnt;
        }

        public String getUser_face_big() {
            return user_face_big;
        }

        public String getImg_url() {
            return img_url;
        }

        public int getImg_cnt() {
            return img_cnt;
        }

        public String getUser_live() {
            return user_live;
        }

        public int getIsSayHello() {
            return isSayHello;
        }
    }

    public static class QaInfoEntity {
        /**
         * question : 你愿意陪我看日落么？
         * options : ["不愿意","我想想","我愿意"]
         */

        private String question;
        private List<String> options;

        public void setQuestion(String question) {
            this.question = question;
        }

        public void setOptions(List<String> options) {
            this.options = options;
        }

        public String getQuestion() {
            return question;
        }

        public List<String> getOptions() {
            return options;
        }
    }
}
