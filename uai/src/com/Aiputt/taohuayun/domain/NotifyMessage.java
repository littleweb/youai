package com.Aiputt.taohuayun.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class NotifyMessage implements Parcelable {

	// "type":"0","ctime":"1437645677"}]}

	public String id;
	public String uid;
	public String title;
	public String message;
	public String type;
	public String ctime;

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(title);
		dest.writeString(uid);
		dest.writeString(message);
		dest.writeString(type);
		dest.writeString(ctime);
	}

	public static final Parcelable.Creator<NotifyMessage> CREATOR = new Parcelable.Creator<NotifyMessage>() {
		public NotifyMessage createFromParcel(Parcel in) {
			return new NotifyMessage(in);
		}

		public NotifyMessage[] newArray(int size) {
			return new NotifyMessage[size];
		}
	};

	private NotifyMessage(Parcel in) {
		id = in.readString();
		title = in.readString();
		uid = in.readString();
		message = in.readString();
		type = in.readString();
		ctime = in.readString();
	}
}
