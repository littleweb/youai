package com.Aiputt.taohuayun.domain;

import java.io.Serializable;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class ImageList implements Serializable {
    private String pid;
    private String img_url_small;
    private String img_url_big;
    private String status;//0 审核中  1 审核成功

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getImg_url_small() {
        return img_url_small;
    }

    public void setImg_url_small(String img_url_small) {
        this.img_url_small = img_url_small;
    }

    public String getImg_url_big() {
        return img_url_big;
    }

    public void setImg_url_big(String img_url_big) {
        this.img_url_big = img_url_big;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
