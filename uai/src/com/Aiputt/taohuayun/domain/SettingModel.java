package com.Aiputt.taohuayun.domain;

import android.graphics.drawable.Drawable;

/**
 * Created by zhonglq on 2015/7/18.
 */
public class SettingModel {
    private int drawbleId;
    private Drawable icon;
    private String label;
    private int type;//1 rightArrow 2 text
    private String rightText;

    public int getDrawbleId() {
        return drawbleId;
    }

    public void setDrawbleId(int drawbleId) {
        this.drawbleId = drawbleId;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRightText() {
        return rightText;
    }

    public void setRightText(String rightText) {
        this.rightText = rightText;
    }
}
