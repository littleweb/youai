package com.Aiputt.taohuayun.domain;

/**
 * Created by zhonglq on 2015/7/24.
 */
public enum BaseInfo {

    NICK(1, "昵称"),
    BIRTHDAY(2, "生日"),
    AGE(3, "年龄"),
    CONSTELLATION(3, "星座"),
    LIVELOCATION(4, "居住地"),
    JIGUAN(5, "籍贯"),
    HEIGHT(6, "身高"),
    体重(7, "体重"),
    BOLLDTYPE(8, "血型");

    public int type;
    public String infoTitle;
    public String infoValue;

    private BaseInfo(int type, String infoTitle, String infoValue) {
        this.type = type;
        this.infoTitle = infoTitle;
        this.infoValue = infoValue;
    }

    private BaseInfo(int type, String infoTitle) {
        this.type = type;
        this.infoTitle = infoTitle;
    }

    private BaseInfo[] getBaseInfo() {
        return values();
    }

}
