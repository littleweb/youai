package com.Aiputt.taohuayun.domain;

import com.Aiputt.taohuayun.cache.SharePCach;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/7/29.
 */
public class UserInfoConfig {

    public static Map<Integer, String> USEINFO_CONFIG = new HashMap<Integer, String>();

    static {
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HEIGHT, UserInfoConfig.INDEX_SELECTED_HEIGHT);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_WEIGHT, UserInfoConfig.INDEX_SELECTED_WEIGHT);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_BLOOD_TYPE, UserInfoConfig.INDEX_SELECTED_BLOODTYPE);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_EDUCATION, UserInfoConfig.INDEX_SELECTED_EDUCATION);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_OCCUPATION, UserInfoConfig.INDEX_SELECTED_OCCUPATION);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_INCOME, UserInfoConfig.INDEX_SELECTED_INCOME);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_MARRIAGE, UserInfoConfig.INDEX_SELECTED_MARRIAGE);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HOUSE, UserInfoConfig.INDEX_SELECTED_HOUSE);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_YIDILIAN, UserInfoConfig.INDEX_SELECTED_YIDILIAN);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_LIVE_LOCATION, UserInfoConfig.SELECTED_LIVE_LOCATION);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_JIGUAN, UserInfoConfig.SELECTED_JIGUAN);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_BIRTHDAY, UserInfoConfig.SELECTED_BIRTHDAY);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HIS_LOCATION, UserInfoConfig.INDEX_SELECTED_HIS_LOCATION);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HIS_AGE, UserInfoConfig.INDEX_SELECTED_HIS_AGE);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HIS_HEIGHT, UserInfoConfig.INDEX_SELECTED_HIS_HEIGHT);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HIS_EDUCATION, UserInfoConfig.INDEX_SELECTED_HIS_EDUCATION);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HIS_INCOME, UserInfoConfig.INDEX_SELECTED_HIS_INCOME);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_GOOD_PLACE, UserInfoConfig.INDEX_SELECTED_GOOD_PLACE);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_WITH_PARENT, UserInfoConfig.INDEX_SELECTED_WITH_PARENT);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_LOVE_OTHER, UserInfoConfig.INDEX_SELECTED_LOVE_OTHER);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HAS_CHILDREN, UserInfoConfig.INDEX_SELECTED_HAS_CHILDREN);
        USEINFO_CONFIG.put(UserInfoConfig.TYPE_QIN_MI, UserInfoConfig.INDEX_SELECTED_QIN_MI);
    }

    public static final String SELECTED_LIVE_LOCATION = "liveLocation";
    public static final String SELECTED_JIGUAN = "jiGuan";
    public static final String SELECTED_BIRTHDAY = "birthDay";

    public static final int TYPE_HEIGHT = 1;
    public static final int TYPE_WEIGHT = 2;
    public static final int TYPE_BLOOD_TYPE = 3;
    public static final int TYPE_EDUCATION = 4;
    public static final int TYPE_OCCUPATION = 5;
    public static final int TYPE_INCOME = 6;
    public static final int TYPE_MARRIAGE = 7;
    public static final int TYPE_HOUSE = 8;
    public static final int TYPE_YIDILIAN = 9;
    public static final int TYPE_LIVE_LOCATION = 10;
    public static final int TYPE_JIGUAN = 11;
    public static final int TYPE_BIRTHDAY = 12;
    public static final int TYPE_HIS_LOCATION = 13;
    public static final int TYPE_HIS_AGE = 14;
    public static final int TYPE_HIS_HEIGHT = 15;
    public static final int TYPE_HIS_EDUCATION = 16;
    public static final int TYPE_HIS_INCOME = 17;
    public static final int TYPE_GOOD_PLACE = 18;
    public static final int TYPE_WITH_PARENT = 19;
    public static final int TYPE_LOVE_OTHER = 20;
    public static final int TYPE_HAS_CHILDREN = 21;
    public static final int TYPE_QIN_MI = 22;

    public static final String INDEX_SELECTED_HEIGHT = "selectedHeight";
    public static final String INDEX_SELECTED_WEIGHT = "selectedWeight";
    public static final String INDEX_SELECTED_BLOODTYPE = "selectedBloodType";
    public static final String INDEX_SELECTED_EDUCATION = "selectedEducation";
    public static final String INDEX_SELECTED_OCCUPATION = "selectedOccupation";
    public static final String INDEX_SELECTED_INCOME = "selectedIncome";
    public static final String INDEX_SELECTED_MARRIAGE = "selectedMarriage";
    public static final String INDEX_SELECTED_HOUSE = "selectedHouse";
    public static final String INDEX_SELECTED_YIDILIAN = "selectedYidilian";
    public static final String INDEX_SELECTED_HIS_LOCATION = "selectedHisLocation";
    public static final String INDEX_SELECTED_HIS_AGE = "selectedHisAge";
    public static final String INDEX_SELECTED_HIS_HEIGHT= "selectedHisHeight";
    public static final String INDEX_SELECTED_HIS_EDUCATION = "selectedHisEducation";
    public static final String INDEX_SELECTED_HIS_INCOME = "selectedHisIncome";
    public static final String INDEX_SELECTED_GOOD_PLACE = "selectedGoodPlace";
    public static final String INDEX_SELECTED_WITH_PARENT = "selectedWithParent";
    public static final String INDEX_SELECTED_LOVE_OTHER = "selectedLoveOther";
    public static final String INDEX_SELECTED_HAS_CHILDREN = "selectedHasChildren";
    public static final String INDEX_SELECTED_QIN_MI = "selectedQinMi";

    public static Map<String, String> USERINFO_VALUE = new HashMap<String, String>();

    /**
     * 保存用户配置信息
     *
     * @param key
     * @param value
     */
    public static void saveUserSelectedInfo(String key, String value) {
        SharePCach.saveStringCach(key, value);
        USERINFO_VALUE.put(key, value);
    }

    /**
     * 获取用户配置信息
     *
     * @param key
     * @return
     */
    public static String getUserSelectedInfo(String key) {
        return SharePCach.loadStringCach(key);
    }

}
