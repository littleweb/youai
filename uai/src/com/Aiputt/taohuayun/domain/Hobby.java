package com.Aiputt.taohuayun.domain;

import org.litepal.crud.DataSupport;

/**
 * 个性特征
 * Created by zhonglq on 2015/7/19.
 */
public class Hobby extends DataSupport{
    private int id;
    private String description;
    private int type;//0 no  1 yes

    public Hobby(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
