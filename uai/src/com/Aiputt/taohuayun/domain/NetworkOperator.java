package com.Aiputt.taohuayun.domain;

public enum NetworkOperator {
	CHINA_MOBILE("1", "移动"), CHINA_UNICOM("2", "联通"), CHINA_TELECOM("3", "电信"), OTHER(
			"4", "其他");
	private NetworkOperator(String isp, String name) {
		this.ISP = isp;
		this.name = name;
	}

	public final String ISP;
	public final String name;

	public String getName() {
		return name;
	}
}
