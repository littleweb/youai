package com.Aiputt.taohuayun.domain;

import com.Aiputt.taohuayun.cache.SharePCach;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/7/29.
 */
public class SearchConfig {

	  public static Map<Integer, String> USEINFO_CONFIG = new HashMap<Integer, String>();

	    static {
	        USEINFO_CONFIG.put(UserInfoConfig.TYPE_HEIGHT, SearchConfig.SEARCH_HEIGHT);
	        USEINFO_CONFIG.put(UserInfoConfig.TYPE_EDUCATION, SearchConfig.SEARCH_EDUCATION);
	        USEINFO_CONFIG.put(UserInfoConfig.TYPE_INCOME, SearchConfig.SEARCH_ICOME);
	    }

    public static final String SEARCH_CITY = "search_city";
    public static final String SEARCH_CITY_SEARCH = "search_city_search";
    public static final String SEARCH_POVISION_SEARCH = "search_povision_search";
    public static final String SEARCH_AGE = "search_age";
    public static final String SEARCH_AGE_SEARCH = "search_age_search";
    public static final String SEARCH_HEIGHT = "search_height";
    public static final String SEARCH_HEIGHT_SEARCH = "search_height_search";
    public static final String SEARCH_EDUCATION = "search_education";
    public static final String SEARCH_ICOME = "search_incom";


   

}
