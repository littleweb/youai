package com.Aiputt.taohuayun.cache;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.Aiputt.taohuayun.utils.CommonUtil;


/**
 * 存储各种数据到sharepreference的的类，存储方法以save开头，取数据方法以load字段开头
 *
 * @author user
 */
public class SharePCach {
    private static final String SHARENAME = "POSTCARD";
    // 保存user_no
    public static final String USER_NO = "user_no";
    public static final String USER_PWD = "user_pwd";
    public static final String TOKEN = "token";
    public static final String HPWD = "hpwd";
    
    public static final String SHOWNOTICE = "shownotice";
    public static final String SHOWMSG = "showmsg";
    public static final String TARGET = "target";
    public static final String MSGCOUNT = "msgcount";
    public static final String NOTICECOUNT = "noticecount";

    /**
     * 删除缓存中的数据
     *
     * @param key
     * @return
     */
    public static boolean removeShareCach(String key) {
        SharedPreferences mySharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        Editor editor = mySharedPreferences.edit();
        editor.remove(key);
        return editor.commit();
    }

    /**
     * 往sharepreference里面存储字符串数据
     *
     * @param key     存储的键名
     * @param content 存储的内容
     * @return
     */
    public static boolean saveStringCach(String key, String content) {
        SharedPreferences mySharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        Editor editor = mySharedPreferences.edit();
        editor.putString(key, content);
        return editor.commit();
    }

    /**
     * 从sharepreference里面取出字符串数据
     *
     * @param key 取数据键名
     * @return
     */
    public static String loadStringCach(String key) {
        SharedPreferences sharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    /**
     * 往sharepreference里面存储整型数据
     *
     * @param key     存储的键名
     * @param content 存储的内容
     * @return
     */
    public static boolean saveIntCach(String key, int content) {
        SharedPreferences mySharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        Editor editor = mySharedPreferences.edit();
        editor.putInt(key, content);
        return editor.commit();
    }

    /**
     * 从sharepreference里面取出整型数据
     *
     * @param key 取数据键名
     * @return
     */
    public static int loadIntCach(String key, int defaultValue) {
        SharedPreferences sharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        return sharedPreferences.getInt(key, defaultValue);
    }


    /**
     * 从sharepreference里面取出整型数据
     *
     * @param key 取数据键名
     * @return
     */
    public static int loadIntCach(String key) {
        SharedPreferences sharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }

    /**
     * 往sharepreference里面存储Boolean数据
     *
     * @param key     存储的键名
     * @param content 存储的内容
     * @return
     */
    public static boolean saveBooleanCach(String key, Boolean content) {
        SharedPreferences mySharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        Editor editor = mySharedPreferences.edit();
        editor.putBoolean(key, content);
        return editor.commit();
    }

    /**
     * 从sharepreference里面取出Boolean数据
     *
     * @param key 取数据键名
     * @return
     */
    public static Boolean loadBooleanCach(String key) {
        SharedPreferences sharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    /**
     * 往sharepreference里面存储float数据
     *
     * @param key     存储的键名
     * @param content 存储的内容
     * @return
     */
    public static boolean saveFloatCach(String key, Float content) {
        SharedPreferences mySharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        Editor editor = mySharedPreferences.edit();
        editor.putFloat(key, content);
        return editor.commit();
    }

    /**
     * 从sharepreference里面取出float数据
     *
     * @param key 取数据键名
     * @return
     */
    public Float loadFloatCach(String key) {
        SharedPreferences sharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        return sharedPreferences.getFloat(key, 0);
    }

    /**
     * 往sharepreference里面存储Long数据
     *
     * @param key     存储的键名
     * @param content 存储的内容
     * @return
     */
    public static boolean saveLongCach(String key, Long content) {
        SharedPreferences mySharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        Editor editor = mySharedPreferences.edit();
        editor.putLong(key, content);
        return editor.commit();
    }

    /**
     * 从sharepreference里面取出Long数据
     *
     * @param key 取数据键名
     * @return
     */
    public static Long loadLongCach(String key) {
        SharedPreferences sharedPreferences = CommonUtil.getApplication()
                .getSharedPreferences(SHARENAME, Activity.MODE_PRIVATE);
        return sharedPreferences.getLong(key, 0);
    }

    /**
     * 清除缓存数据
     */
    public static void clearCach() {
        SharedPreferences sp = CommonUtil.getApplication().getSharedPreferences(SHARENAME,
                Activity.MODE_PRIVATE);
        Editor edit = sp.edit();
        edit.clear();
        edit.commit();
    }

}
