package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by Administrator on 2015/7/27.
 */
public class UploadHeadPhotoEvent extends Event {
    public int code = -1;
    public String msg = null;
    public String user_face;
    public String user_face_big;
    public String user_total_balance;
    public boolean isHeadPhoto;

}
