package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.OrderNoResponse;
import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by zhonglq on 2015/8/8.
 */
public class OrderNoEvent extends Event {
    public int code = -1;
    public String msg = null;
    public OrderNoResponse orderNoResponse;
    public int payType = 0;
    public String rechargeCardNumber = null;
    public String rechargeCardPassword = null;
    public String cardcode = null;
}
