package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by zhonglq on 2015/8/30.
 */
public class ModifyHeadPhotoEvent extends Event {
    public ImageList imageList;
    public int code = -1;
    public String msg = null;
}
