package com.Aiputt.taohuayun.event;

import android.view.View;

import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by Administrator on 2015/8/19.
 */
public class DeletePhotoEvent extends Event {
    public int code = -1;
    public String msg = null;
    public ImageList imageList = null;
    public View view;
}
