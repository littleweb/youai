package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.notify.Event;

import java.util.ArrayList;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class NearByEvent extends Event {
    public int code = -1;
    public String msg = null;
    public ArrayList<User> userList = null;
}
