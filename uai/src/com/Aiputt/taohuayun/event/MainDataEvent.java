package com.Aiputt.taohuayun.event;

import java.util.ArrayList;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.notify.Event;

public class MainDataEvent extends Event{
    public int code = -1;
    public String msg = null;
    public ArrayList<User> data = null;
}
