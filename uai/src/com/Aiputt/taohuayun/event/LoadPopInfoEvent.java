package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.UserTipInfo;
import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by zhonglq on 2015/9/7.
 */
public class LoadPopInfoEvent extends Event {
    public int code = -1;
    public String msg = null;
    public UserTipInfo userTipInfo;
}
