package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.notify.Event;

import java.util.List;

public class UnreadMailEvent extends Event{

	 public int code = -1;
	 public String msg = null;
	 public List<User> data;
}
