package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.ImageList;
import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by Administrator on 2015/7/27.
 */
public class UploadNormalPhotoEvent extends Event {
    public int code = -1;
    public String msg = null;
    public ImageList imageList;

}
