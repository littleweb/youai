package com.Aiputt.taohuayun.event;

import com.Aiputt.taohuayun.domain.User;
import com.Aiputt.taohuayun.notify.Event;

/**
 * Created by zhonglq on 2015/7/25.
 */
public class RandomInfoEvent extends Event {
    public int code = -1;
    public String msg = null;
    public User mUser = null;
}
