package com.Aiputt.taohuayun.receiver;

import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import cn.jpush.android.api.JPushInterface;

import com.Aiputt.taohuayun.cache.SharePCach;
import com.Aiputt.taohuayun.utils.MsgUtil;

public class PushMsgReceiver extends BroadcastReceiver {
	private static final String TAG = "JPush";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		Bundle bundle = intent.getExtras();
		Log.d(TAG, "action:" + action + "," + printBundle(intent.getExtras()));
		if (action.equals(JPushInterface.ACTION_MESSAGE_RECEIVED)) {
			MsgUtil.handleMessage(context, intent);
		}

		if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
			// String title =
			// bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
			// System.out.println("用户注册成功"+title);
		} else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED
				.equalsIgnoreCase(action)) {
			// 收到通知
			String msg = bundle.getString(JPushInterface.EXTRA_TITLE);
			if (msg != null) {
				int notificationId = bundle
						.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
				Log.d(TAG, "id: " + notificationId);
				MsgUtil.cancelNotification(context, notificationId);
			} else {
				Log.d(TAG, "msg: " + msg);
			}
			// CloudHelper.syncOrder(context, true);
		} else if ("cn.jpush.android.intent.NOTIFICATION_OPENED"
				.equalsIgnoreCase(action)) {
			String extras = bundle.getString("cn.jpush.android.EXTRA");
			System.out.println("===extras====" + extras);
			try {
				JSONObject extrasJson = new JSONObject(extras);
				SharePCach.saveBooleanCach(SharePCach.SHOWMSG, true);
				SharePCach.saveBooleanCach("shownotice", true);
				SharePCach.saveStringCach("target",
						extrasJson.getString("target"));
				SharePCach.saveStringCach("uread_msg_cnt",
						extrasJson.getString("uread_msg_cnt"));
				SharePCach.saveStringCach("uread_notice_count",
						extrasJson.getString("uread_notice_count"));
			} catch (Exception e) {

			}

			// action:cn.jpush.android.intent.NOTIFICATION_OPENED,
			// key:cn.jpush.android.NOTIFICATION_CONTENT_TITLE, value:桃花运
			// key:cn.jpush.android.NOTIFICATION_ID, value:191234578
			// key:cn.jpush.android.PUSH_ID, value:3903869434
			// key:cn.jpush.android.MSG_ID, value:3903869434
			// key:cn.jpush.android.ALERT, value:你有一条新消息
			// key:cn.jpush.android.EXTRA,
			// value:{"target":"","uread_msg_cnt":10,"uread_notice_count":2}
			// key:cn.jpush.android.NOTIFICATION_TYPE, value:0

			//
			// Intent notificationIntent = new Intent(context,
			// LoadingActivity.class);
			// // notificationIntent.putExtra(MainTabHost.EXTRA_INDEX_TAG,
			// // MainTabHost.TAG_CUSTOMER);
			// notificationIntent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
			// notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// context.startActivity(notificationIntent);
		}
	}

	private static String printBundle(Bundle bundle) {
		StringBuilder sb = new StringBuilder();
		for (String key : bundle.keySet()) {
			if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
				sb.append("\nkey:" + key + ", value:" + bundle.getInt(key));
			} else if (key.equals(JPushInterface.EXTRA_CONNECTION_CHANGE)) {
				sb.append("\nkey:" + key + ", value:" + bundle.getBoolean(key));
			} else {
				sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
			}
		}
		return sb.toString();
	}
}
