package com.Aiputt.taohuayun.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.event.StopPopInfoEvent;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.utils.CommonUtil;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by zhonglq on 2015/9/8.
 */
public class PopInfoService extends Service {

    private PopInfoHandler popInfoHandler = new PopInfoHandler();

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (CommonUtil.isRunningForeground()) {
                UserController.getInstance().loadPopQa();
            }
        }
    };

    private int defaultExecuteTime = 1000 * 60;

    private Timer timer = new Timer();
    private MyTimerTask myTimerTask;

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            Message message = Message.obtain();
            mHandler.sendMessage(message);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        popInfoHandler.register();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (myTimerTask != null) {
            myTimerTask.cancel();
        }
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 0, defaultExecuteTime);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        popInfoHandler.unregister();
        super.onDestroy();
    }

    private class PopInfoHandler extends EventHandler {

        public void onEvent(StopPopInfoEvent event) {
            myTimerTask.cancel();
            timer.cancel();
            stopSelf();
        }

    }

}
