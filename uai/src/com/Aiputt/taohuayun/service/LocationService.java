package com.Aiputt.taohuayun.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.Aiputt.taohuayun.controller.UserController;
import com.Aiputt.taohuayun.event.LocationEvent;
import com.Aiputt.taohuayun.event.StopLocationEvent;
import com.Aiputt.taohuayun.notify.EventCenter;
import com.Aiputt.taohuayun.notify.EventHandler;
import com.Aiputt.taohuayun.utils.CommonUtil;
import com.Aiputt.taohuayun.utils.LocationUtil;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2015/8/25.
 */
public class LocationService extends Service {
    private static final String TAG = "LocationService";
    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener();

    private MyHandler stopHandler = new MyHandler();

    private MyTimerTask myTimerTask;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (CommonUtil.isRunningForeground()) {
                mLocationClient = new LocationClient(getApplicationContext());
                mLocationClient.registerLocationListener(myListener);
                initLocation();
            }
        }
    };

    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系，
        option.setScanSpan(0);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        mLocationClient.setLocOption(option);
        mLocationClient.start();
    }

    private int defaultExecuteTime = 1000 * 60 * 30;

    private Timer timer = new Timer();

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            Message message = Message.obtain();
            mHandler.sendMessage(message);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        stopHandler.register();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (myTimerTask != null) {
            myTimerTask.cancel();
        }
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 0, defaultExecuteTime);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        stopHandler.unregister();
        super.onDestroy();
    }

    private class MyHandler extends EventHandler {
        public void onEvent(StopLocationEvent event) {
            myTimerTask.cancel();
            timer.cancel();
            LocationService.this.stopSelf();
        }

        public void onEvent(LocationEvent event) {
            if (event.mBDLocation != null) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lat", event.mBDLocation.getLatitude() + "");
                params.put("lng", event.mBDLocation.getLongitude() + "");
                LocationUtil.setLatitude(event.mBDLocation.getLatitude() + "");
                LocationUtil.setLongitude(event.mBDLocation.getLongitude() + "");
                LocationUtil.setCity(event.mBDLocation.getCity());
                UserController.getInstance().updateLocationInfo(params);
            }
        }
    }

    /**
     * 实现实时位置回调监听
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            BDLocation mBDLocation = null;
            //Receive Location
            if (location != null) {
                if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                    Log.i(TAG, "gps定位成功");
                    mBDLocation = location;
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                    Log.i(TAG, "网络定位成功");
                    mBDLocation = location;
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                    Log.i(TAG, "离线定位成功");
                    mBDLocation = location;
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    Log.i(TAG, "服务端网络定位失败");
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    Log.i(TAG, "网络不通导致定位失败，请检查网络是否通畅");
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    Log.i(TAG, "无法获取有效定位依据导致定位失败");
                }
                mLocationClient.stop();
                LocationEvent event = new LocationEvent();
                event.mBDLocation = mBDLocation;
                if (mBDLocation != null) {
                    if(!TextUtils.isEmpty(mBDLocation.getCity())) {
                        LocationUtil.setCity(mBDLocation.getCity());
                        LocationUtil.setLatitude(mBDLocation.getLatitude() + "");
                        LocationUtil.setLongitude(mBDLocation.getLongitude() + "");
                        EventCenter.getInstance().send(event);
                    }
                }
            }
        }

    }
}
