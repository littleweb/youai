package com.Aiputt.taohuayun.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.Aiputt.taohuayun.R;
import com.Aiputt.taohuayun.domain.AgeInfo;
import com.Aiputt.taohuayun.domain.Disposition;

import java.util.List;

/**
 * Created by zhonglq on 2015/7/20.
 */
public class SelectAgeDialog {

    private Context mContext;
    private String title;
    private Dialog mDialog;
    private ListView mListView;
    public Button btnSure;
    private TextView mTitle;

    public SelectAgeDialog(Context context) {
        this.mContext = context;
        View view = View.inflate(context, R.layout.dialog_select_age, null);
        mDialog = new Dialog(context, R.style.dialog);
        mDialog.setContentView(view);
        mDialog.setCancelable(true);

        mTitle = (TextView) view.findViewById(R.id.dialog_title);
        mListView = (ListView) view.findViewById(R.id.gvItem);
        btnSure = (Button) view.findViewById(R.id.btnSure);

    }

    public void setTitle(String title){
        mTitle.setText(title);
    }

    public void setGridViewAdapter(BaseAdapter baseAdapter){
    	mListView.setAdapter(baseAdapter);
    	mListView.setSelection(6);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
    	mListView.setOnItemClickListener(listener);
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    interface OnSureClickListener{

    }

}
